package io.gravitar.pubsub

import boopickle.Default._
import boopickle.Pickler

sealed trait SubscriberMessage
case class SubscribeTo(sector: Int)     extends SubscriberMessage
case class UnsubscribeFrom(sector: Int) extends SubscriberMessage

object SubscriberMessage {

  implicit val pubSubClientToServerMessagePickler: Pickler[SubscriberMessage] =
    generatePickler[SubscriberMessage]

}
