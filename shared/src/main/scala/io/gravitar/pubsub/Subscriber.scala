package io.gravitar.pubsub

import scala.collection.mutable

trait Subscriber[Channel] {

  val subscriptions: mutable.Set[Channel] = mutable.Set()

}
