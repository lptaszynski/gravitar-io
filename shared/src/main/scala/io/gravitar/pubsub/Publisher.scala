package io.gravitar.pubsub

import scala.collection.mutable

trait Publisher[Channel, Subscriber] {

  private val channelToSubscribers: mutable.Map[Channel, mutable.Set[Subscriber]] = mutable.Map()
  private val subscriberToChannels: mutable.Map[Subscriber, mutable.Set[Channel]] = mutable.Map()

  def addChannel(channel: Channel): Unit = channelToSubscribers.put(channel, mutable.Set())

  def subscribe(subscriber: Subscriber, channel: Channel): Unit =
    if (channelExist(channel)) {
      if (!isSubscriber(subscriber)) addSubscriber(subscriber)
      channelToSubscribers(channel) += subscriber
      subscriberToChannels(subscriber) += channel
    }

  def unsubscribe(subscriber: Subscriber, channel: Channel): Unit =
    if (isSubscriber(subscriber)) {
      subscriberToChannels(subscriber) -= channel
      channelToSubscribers(channel) -= subscriber
      if (subscriberToChannels.isEmpty) subscriberToChannels -= subscriber
    }

  def unsubscribeFromAllChannels(subscriber: Subscriber): Unit =
    if (isSubscriber(subscriber)) {
      val channels = subscriberToChannels(subscriber)
      for (channel <- channels) channelToSubscribers(channel) -= subscriber
      subscriberToChannels -= subscriber
    }

  def subscribersOf(channel: Channel): Set[Subscriber] =
    channelToSubscribers(channel).toSet

  def subscriptionsOf(subscriber: Subscriber): Set[Channel] =
    if (isSubscriber(subscriber)) subscriberToChannels(subscriber).toSet else Set()

  private def channelExist(channel: Channel): Boolean = channelToSubscribers.isDefinedAt(channel)

  private def isSubscriber(subscriber: Subscriber): Boolean =
    subscriberToChannels.contains(subscriber)

  private def addSubscriber(subscriber: Subscriber): Unit =
    subscriberToChannels.put(subscriber, mutable.Set())

}
