package io.gravitar.wsprotocol

import boopickle.Default._
import boopickle.Pickler
import io.gravitar.engine._
import io.gravitar.engine.HostDirective._
import io.gravitar.engine.ImPlayer._
import io.gravitar.engine.ImClientBody._
import io.gravitar.engine.Leaderboard._

sealed trait ServerToClientMessage

case class JoinedGameAs(player: ImPlayer, body: ImClientBody) extends ServerToClientMessage

case class CannotJoinGame(reason: String) extends ServerToClientMessage

case class YouHaveLost(reason: String) extends ServerToClientMessage

case class UpdateLeaderboard(leaderboard: Leaderboard) extends ServerToClientMessage

case class UpdateSummary(summary: Summary) extends ServerToClientMessage

case class PropagateHostDirectives(hostDirectives: Vector[HostDirective])
    extends ServerToClientMessage

case class Logs(events: Vector[String]) extends ServerToClientMessage

object ServerToClientMessage {

  implicit val serverToClientMessagePickler: Pickler[ServerToClientMessage] =
    generatePickler[ServerToClientMessage]

}
