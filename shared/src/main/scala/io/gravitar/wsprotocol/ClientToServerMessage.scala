package io.gravitar.wsprotocol

import boopickle.Default._
import boopickle.Pickler
import io.gravitar.engine.PlayerDeclaration
import io.gravitar.engine.PlayerDeclaration._
import io.gravitar.pubsub.SubscriberMessage
import io.gravitar.pubsub.SubscriberMessage._

sealed trait ClientToServerMessage
case class JoinGame(playerName: String) extends ClientToServerMessage
case class Log(message: String)         extends ClientToServerMessage
case class PlayerDeclarationMessage(playerDeclaration: PlayerDeclaration)
    extends ClientToServerMessage
case class SubscriberMessageEnvelope(subscriberMessage: SubscriberMessage)
    extends ClientToServerMessage
case class GetLogs(events: Int) extends ClientToServerMessage

object ClientToServerMessage {

  implicit val clientToServerMessagePickler: Pickler[ClientToServerMessage] =
    generatePickler[ClientToServerMessage]

}
