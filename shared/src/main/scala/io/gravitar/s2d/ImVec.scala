package io.gravitar.s2d

import boopickle.Default._
import boopickle.Pickler

final case class ImVec(@specialized _x: Float, @specialized _y: Float) extends Vec {

  def this(x: Double, y: Double) = this(x.toFloat, y.toFloat)

  def x: Double = _x
  def y: Double = _y

  override type VecImpl = ImVec

  override def unary_- : ImVec = ImVec(-x, -y)

  override def +(x: Double, y: Double): ImVec = ImVec(this.x + x, this.y + y)

  override def -(x: Double, y: Double): ImVec = ImVec(this.x - x, this.y - y)

  override def /(value: Double): ImVec = ImVec(x / value, y / value)

  override def *(value: Double): ImVec = ImVec(x * value, y * value)

  override def rotated(theta: Double): ImVec = {
    val cos = math.cos(theta)
    val sin = math.sin(theta)
    ImVec(cos * x - sin * y, sin * x + cos * y)
  }

  override def normalized: ImVec = ImVec(x / length, y / length)

  override def toImmutable: ImVec = this
  override def toMutable: MVec    = new MVec(x, y)
}

object ImVec {

  implicit val imVecPickler: Pickler[ImVec] = generatePickler[ImVec]

  val zero: ImVec = ImVec(0, 0)

  val unit: ImVec = ImVec(1, 0)

  def apply(): ImVec = zero

  def apply(xy: Double): ImVec = new ImVec(xy, xy)

  def apply(x: Double, y: Double): ImVec = new ImVec(x, y)

  def randomVec(maxRadius: Double = 1): ImVec = {
    val theta  = 2 * math.Pi * math.random
    val length = maxRadius * math.random
    unit.rotated(theta) * length
  }

  def randomUnitVec: ImVec = {
    val theta = 2 * math.Pi * math.random
    unit.rotated(theta)
  }

}
