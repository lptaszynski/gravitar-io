package io.gravitar.s2d

trait Point {

  type PointImpl <: Point

  def x: Double
  def y: Double

  final def +(vec: Vec): PointImpl = this.+(vec.x, vec.y)
  def +(x: Double, y: Double): PointImpl

  final def -(vec: Vec): PointImpl = this.-(vec.x, vec.y)
  def -(x: Double, y: Double): PointImpl

  final def imVecTo(other: Point): ImVec = imVecTo(other.x, other.y)

  final def imVecTo(otherX: Double, otherY: Double): ImVec = ImVec(otherX - x, otherY - y)

  final def mVecTo(other: Point): MVec = mVecTo(other.x, other.y)

  final def mVecTo(otherX: Double, otherY: Double): MVec = new MVec(otherX - x, otherY - y)

  @inline final def distanceTo(other: Point): Double = distanceTo(other.x, other.y)

  @inline final def distanceTo(otherX: Double, otherY: Double): Double =
    math.sqrt(squareDistanceTo(otherX, otherY))

  @inline final def squareDistanceTo(other: Point): Double =
    squareDistanceTo(other.x, other.y)

  @inline final def squareDistanceTo(otherX: Double, otherY: Double): Double = {
    val dx = x - otherX
    val dy = y - otherY
    dx * dx + dy * dy
  }

  def isWithin(a: Point, b: Point, extra: Vec = ImVec()): Boolean = {
    x >= math.min(a.x, b.x) - extra.x &&
    x <= math.max(a.x, b.x) + extra.y &&
    y >= math.min(a.y, b.y) - extra.x &&
    y <= math.max(a.y, b.y) + extra.y
  }

  def isNotWithin(a: Point, b: Point, extra: Vec = ImVec()): Boolean =
    !isWithin(a, b, extra)

  def toMutable: MPoint

  def toImmutable: ImPoint

  override def equals(that: Any): Boolean =
    that match {
      case that: Point => that.x == x && that.y == y
      case _           => false
    }

  override def hashCode: Int = {
    val prime  = 31
    var result = 1
    result = prime * result + x.hashCode()
    result = prime * result + y.hashCode()
    result
  }

}
