package io.gravitar.s2d

class MVec(@specialized var x: Double, @specialized var y: Double) extends Vec {

  override type VecImpl = MVec

  def this(xy: Double) = this(xy, xy)
  def this() = this(0)

  def toZero(): Unit = setTo(0, 0)

  def setTo(vec: Vec): Unit = {
    this.x = vec.x
    this.y = vec.y
  }

  def setTo(otherX: Double, otherY: Double): Unit = {
    this.x = otherX
    this.y = otherY
  }

  override def unary_- : MVec = new MVec(-x, -y)
  def negate(): Unit = {
    this.x = -this.x
    this.y = -this.y
  }

  override final def +(x: Double, y: Double): MVec = new MVec(this.x + x, this.y + y)
  def +=(vec: Vec): Unit                           = this.+=(vec.x, vec.y)
  final def +=(x: Double, y: Double): Unit = {
    this.x += x
    this.y += y
  }

  override final def -(x: Double, y: Double): MVec = new MVec(this.x - x, this.y - y)
  def -=(vec: Vec): Unit                           = this.-=(vec.x, vec.y)
  final def -=(x: Double, y: Double): Unit = {
    this.x -= x
    this.y -= y
  }

  override final def /(value: Double): MVec = new MVec(x / value, y / value)
  final def /=(value: Double): Unit = {
    this.x /= value
    this.y /= value
  }

  override final def *(value: Double): MVec = new MVec(x * value, y * value)
  final def *=(value: Double): Unit = {
    this.x *= value
    this.y *= value
  }

  override final def rotated(theta: Double): MVec = {
    val cos = math.cos(theta)
    val sin = math.sin(theta)
    new MVec(cos * x - sin * y, sin * x + cos * y)
  }

  final def rotate(theta: Double): Unit = {
    val cos  = math.cos(theta)
    val sin  = math.sin(theta)
    val tmpX = this.x
    val tmpY = this.y
    this.x = cos * tmpX - sin * tmpY
    this.y = sin * tmpX + cos * tmpY
  }

  override def normalized: MVec = {
    val tempLength = length
    new MVec(x / tempLength, y / tempLength)
  }

  def normalize(): Unit = {
    val tempLength = length
    this.x /= tempLength
    this.y /= tempLength
  }

  def graduallyCapLength(
      limit: Double,
      vecLength: Double = length,
      exponent: Double = MVec.slowingExponent
  ): Unit = {
    if (vecLength > limit) this *= math.pow(limit / vecLength, exponent)
  }

  def capLength(limit: Double, vecLength: Double = length): Unit = {
    if (vecLength > limit) setLength(limit, vecLength)
  }

  def setLength(newLength: Double, vecLength: Double = length): Unit = {
    this *= newLength / vecLength
  }

  override def toImmutable: ImVec = ImVec(x, y)
  override def toMutable: MVec    = copy()

  def copy(x: Double = this.x, y: Double = this.y): MVec = new MVec(x, y)

  override def toString: String = s"MVec($x, $y)"

}

object MVec {
  val slowingExponent: Double = 1.0 / 8.0
}
