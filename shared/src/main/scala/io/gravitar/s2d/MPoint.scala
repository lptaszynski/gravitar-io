package io.gravitar.s2d

class MPoint(@specialized var x: Double, @specialized var y: Double) extends Point {

  override type PointImpl = MPoint

  def this(xy: Double) = this(xy, xy)
  def this() = this(0.0)

  def setTo(other: Point): Unit = {
    this.x = other.x
    this.y = other.y
  }

  def setTo(x: Double, y: Double): Unit = {
    this.x = x
    this.y = y
  }

  override final def +(x: Double, y: Double): MPoint = new MPoint(this.x + x, this.y + y)
  override final def -(x: Double, y: Double): MPoint = new MPoint(this.x - x, this.y - y)

  final def +=(vec: Vec): Unit = +=(vec.x, vec.y)

  final def +=(x: Double, y: Double): Unit = {
    this.x += x
    this.y += y
  }

  final def -=(vec: Vec): Unit = -=(vec.x, vec.y)

  final def -=(x: Double, y: Double): Unit = {
    this.x -= x
    this.y -= y
  }

  override final def toImmutable: ImPoint = ImPoint(x, y)
  override final def toMutable: MPoint    = copy()

  def copy(x: Double = this.x, y: Double = this.y): MPoint = new MPoint(x, y)

  override def toString: String = s"MPoint($x, $y)"

}
