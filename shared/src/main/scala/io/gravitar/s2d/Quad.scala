package io.gravitar.s2d

import boopickle.Default._
import boopickle.Pickler

case class Quad(@specialized posX: Double, @specialized posY: Double, @specialized width: Double) {

  val halfWidth: Double    = width / 2.0
  val quarterWidth: Double = halfWidth / 2.0

  val xLeft: Double   = posX - halfWidth
  val xRight: Double  = posX + halfWidth
  val yTop: Double    = posY + halfWidth
  val yBottom: Double = posY - halfWidth

  def leftTopQuad: Quad = {
    val subX = posX - quarterWidth
    val subY = posY + quarterWidth
    Quad(subX, subY, halfWidth)
  }

  def rightTopQuad: Quad = {
    val subX = posX + quarterWidth
    val subY = posY + quarterWidth
    Quad(subX, subY, halfWidth)
  }

  def leftBottomQuad: Quad = {
    val subX = posX - quarterWidth
    val subY = posY - quarterWidth
    Quad(subX, subY, halfWidth)
  }

  def rightBottomQuad: Quad = {
    val subX = posX + quarterWidth
    val subY = posY - quarterWidth
    Quad(subX, subY, halfWidth)
  }

  def contains(point: Point): Boolean = contains(point.x, point.y)

  def contains(x: Double, y: Double): Boolean = {
    x <= xRight && x >= xLeft && y <= yTop && y >= yBottom
  }

}

object Quad {

  def apply(point: Point, width: Double): Quad = new Quad(point.x, point.y, width)

  implicit val quadPickler: Pickler[Quad] = generatePickler[Quad]

}
