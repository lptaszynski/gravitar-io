package io.gravitar

import scala.collection.mutable
import scala.util.Random

package object s2d {

  @inline final val MinPositiveValue: Double = 1.0e-6
  @inline final val MaxNegativeValue: Double = -MinPositiveValue

  def capValue(
      value: Double,
      lowerCap: Double = Double.NegativeInfinity,
      upperCap: Double = Double.PositiveInfinity
  ): Double = {
    math.min(upperCap, math.max(lowerCap, value))
  }

  @inline final def guardAngle(angle: Double): Double = angle % (2 * math.Pi)

  @inline final def guardFromZero(x: Double): Double =
    if (x >= MinPositiveValue || x <= -MaxNegativeValue) x
    else if (x >= 0.0) MinPositiveValue
    else MaxNegativeValue

  def splitValue(
      value: Double,
      minGeneratedValue: Double,
      maxGeneratedValue: Double
  ): Vector[Double] = {
    val twiceMaxValue = 2 * maxGeneratedValue
    val rangeDiff     = maxGeneratedValue - minGeneratedValue
    var valueLeft     = value
    val buffer        = mutable.ArrayBuffer.empty[Double]

    while (valueLeft >= twiceMaxValue) {
      val randomMass = minGeneratedValue + Random.nextDouble() * rangeDiff
      buffer += randomMass
      valueLeft -= randomMass
    }

    if (valueLeft <= maxGeneratedValue) {
      buffer += valueLeft
    } else {
      val halfMassLeft = valueLeft / 2
      buffer += halfMassLeft
      buffer += halfMassLeft
    }

    buffer.toVector
  }

}
