package io.gravitar.s2d

trait Vec {
  type VecImpl <: Vec

  def x: Double
  def y: Double

  def normalized: VecImpl

  def unary_- : VecImpl

  def +(x: Double, y: Double): VecImpl

  def -(x: Double, y: Double): VecImpl

  def /(value: Double): VecImpl

  def *(value: Double): VecImpl

  def rotated(theta: Double): VecImpl

  def theta: Double = math.atan2(y, x)

  @inline final def length: Double = math.sqrt(lengthSquared)

  @inline final def lengthSquared: Double = x * x + y * y

  final def +(other: Vec): VecImpl = this.+(other.x, other.y)

  final def -(other: Vec): VecImpl = this.-(other.x, other.y)

  final def dot(other: Vec): Double = this.dot(other.x, other.y)

  final def dot(x: Double, y: Double): Double = this.x * x + this.y * y

  final def isLengthNonZero: Boolean = x != 0 || y != 0

  def toImmutable: ImVec
  def toMutable: MVec

  override def equals(that: Any): Boolean =
    that match {
      case that: Vec => that.x == x && that.y == y
      case _         => false
    }

  override def hashCode: Int = {
    val prime  = 31
    var result = 1
    result = prime * result + x.hashCode()
    result = prime * result + y.hashCode()
    result
  }

}
