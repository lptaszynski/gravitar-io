package io.gravitar.s2d

import boopickle.Default._
import boopickle.Pickler

final case class ImPoint(@specialized _x: Float, @specialized _y: Float) extends Point {

  def this(x: Double, y: Double) = this(x.toFloat, y.toFloat)

  def x: Double = _x
  def y: Double = _y

  override type PointImpl = ImPoint

  override def +(x: Double, y: Double): ImPoint = ImPoint(this.x + x, this.y + y)
  override def -(x: Double, y: Double): ImPoint = ImPoint(this.x - x, this.y - y)

  override def toImmutable: ImPoint = this
  override def toMutable: MPoint    = new MPoint(x, y)
}

object ImPoint {

  implicit val imPointPickler: Pickler[ImPoint] = generatePickler[ImPoint]

  val zero: ImPoint = ImPoint(0, 0)

  def apply(): ImPoint = zero

  def apply(xy: Double): ImPoint = new ImPoint(xy, xy)

  def apply(x: Double, y: Double): ImPoint = new ImPoint(x, y)

  def randomInsideCircle(cRadius: Double = 1, cPosition: ImPoint = ImPoint(0, 0)): ImPoint = {
    val theta  = 2 * math.Pi * math.random
    val length = cRadius * math.random
    cPosition + (ImVec(1, 0).rotated(theta) * length)
  }

}
