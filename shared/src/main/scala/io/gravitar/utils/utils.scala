package io.gravitar

import scala.annotation.tailrec

package object utils {

  def time[R](block: => R, fun: String = ""): R = {
    val t0     = System.currentTimeMillis()
    val result = block // call-by-name
    val t1     = System.currentTimeMillis()
    println(s"$fun -> elapsed time: ${t1 - t0} + ms")
    result
  }

  @tailrec
  def splitTrees[T](levels: List[List[T]], acc: List[List[T]] = Nil): List[List[T]] = {
    levels match {
      case head :: tail =>
        val newAcc = if (acc.nonEmpty) {
          for {
            accList     <- acc
            headElement <- head
          } yield accList :+ headElement
        } else {
          head.map(List(_))
        }
        splitTrees(tail, newAcc)
      case Nil => acc
    }
  }

  def formatRoundedInt(value: Int): String = {
    if (math.abs(value) < 1000) {
      value.toString
    } else {
      val div = value / 1000
      val rem = (value % 1000) / 100
      s"${formatToSpacedInt(div)}.${rem}k"
    }
  }

  def formatToSpacedInt(value: Int): String = {
    var valueWithSpaces = ""
    math.abs(value).toString.reverse.zipWithIndex.foreach {
      case (char, idx) =>
        if (idx != 0 && idx % 3 == 0) valueWithSpaces += " "
        valueWithSpaces += char
    }
    val sign = if (value < 0) "-" else ""
    sign ++ valueWithSpaces.reverse
  }

}
