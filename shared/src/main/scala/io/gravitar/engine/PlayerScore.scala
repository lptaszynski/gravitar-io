package io.gravitar.engine

import boopickle.Default._
import boopickle.Pickler

case class PlayerScore(playerName: String, score: Double)

object PlayerScore {

  implicit val playerScorePickler: Pickler[PlayerScore] = generatePickler[PlayerScore]

}
