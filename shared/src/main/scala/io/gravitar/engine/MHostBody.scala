package io.gravitar.engine

import io.gravitar.engine.effect.gravitymanipulation.MGravityManipulationEffect
import io.gravitar.engine.effect.shooting.MShootingEffect
import io.gravitar.s2d._

sealed abstract class MHostBody(
    @specialized initialMass: Double,
    @specialized initialHealth: Double,
    @specialized initialGCoefficient: Double,
    initialAntimatter: Boolean
) extends MBody(initialMass, initialHealth, initialGCoefficient, initialAntimatter) {

  implicit def gameEngine: HostGameEngine

  var isNewBody: Boolean               = true
  var healthNeedsUpdate: Boolean       = false
  var massNeedsUpdate: Boolean         = false
  var gCoefficientNeedsUpdate: Boolean = false
  var isDestroyed: Boolean             = false

  def resetFlags(): Unit = {
    isNewBody = false
    massNeedsUpdate = false
    healthNeedsUpdate = false
    gCoefficientNeedsUpdate = false
  }

  def velocity: MVec

  def gravityAcc: MVec

  def isAffectedByGravity: Boolean = true

  def vMax: Double = gameEngine.gameParameters.freeBodyVMaxPerTick

  override def mass_=(value: Double): Unit = {
    val cappedValue = capValue(value, 1)
    if (cappedValue < gameEngine.gameParameters.minMass) isDestroyed = true
    if (cappedValue != _mass) {
      _mass = cappedValue
      massNeedsUpdate = true
      radius = calculateRadius
    }
  }

  override def health_=(value: Double): Unit = {
    val cappedValue = capValue(value, 0, 1)
    if (cappedValue == 0.0) isDestroyed = true
    if (cappedValue != _health) {
      _health = cappedValue
      healthNeedsUpdate = true
    }
  }

  override def gCoefficient_=(value: Double): Unit = {
    val cappedValue = capValue(
      value,
      gameEngine.gameParameters.minGCoefficient,
      gameEngine.gameParameters.maxGCoefficient
    )
    if (cappedValue != _gCoefficient) {
      _gCoefficient = value
      gCoefficientNeedsUpdate = true
    }
  }

  def onMove(): Unit = {
    velocityUpdate(velocity)
    position += velocity
  }

  def onTick(): Unit = ()

  def velocityUpdate(v: MVec): Unit = {
    v += gravityAcc
    slowVelocity(v)
  }

  def slowVelocity(v: MVec): Unit = slowVelocityToV(v, vMax)

  protected def slowVelocityToV(v: MVec, vMax: Double): Unit = {
    var length = v.length
    if (length > gameEngine.gameParameters.vLimitPerTick) {
      v.setLength(gameEngine.gameParameters.vLimitPerTick, length)
      length = v.length
    }
    v.graduallyCapLength(vMax, vecLength = length)
  }

  def regenHealth(): Unit =
    health += gameEngine.gameParameters.healthRegenPerSecond

  def toImClientType: ImClientType

  def onDestruction(): Unit = {
    val masses = splitValue(
      mass,
      2 * gameEngine.gameParameters.minMass,
      math.max(mass / 10, 10 * gameEngine.gameParameters.minMass)
    )
    val dustBodies = masses.map(createDust)
    dustBodies.foreach(gameEngine.addBody)
  }

  protected def createDust(mass: Double): MHostDust = {
    val vec   = ImVec.randomUnitVec
    val xDiff = math.random() * radius
    val yDiff = math.random() * radius
    new MHostDust(
      id = gameEngine.nextId(),
      initialMass = mass,
      initialAntimatter = antimatter,
      position = position + (vec.x * xDiff, vec.y * yDiff),
      velocity = velocity + (vec.x * 2 * xDiff, vec.y * 2 * yDiff),
      gravityAcc = gravityAcc.copy()
    )
  }

}

class MHostAsteroid(
    @specialized override val id: Double,
    initialMass: Double,
    initialHealth: Double = 1,
    initialGCoefficient: Double = 1,
    initialAntimatter: Boolean = false,
    override val position: MPoint,
    override val velocity: MVec = new MVec(),
    override val gravityAcc: MVec = new MVec(),
    @specialized override val kind: Byte = 0
)(implicit override val gameEngine: HostGameEngine)
    extends MHostBody(initialMass, initialHealth, initialGCoefficient, initialAntimatter)
    with Asteroid {

  override def onTick(): Unit = ()

  override def toImClientType: ImClientAsteroid =
    ImClientAsteroid(
      id,
      mass.toFloat,
      health.toFloat,
      antimatter,
      position.toImmutable,
      velocity.toImmutable,
      kind
    )

  override def toString: String =
    s"MHostAsteroid($id, $mass, $health, $gCoefficient, $antimatter, $position, $velocity, $gravityAcc, $kind, $antimatter)"

}

class MHostShip(
    @specialized override val id: Double,
    @specialized initialMass: Double,
    @specialized initialHealth: Double = 1,
    @specialized initialGCoefficient: Double = 1,
    initialAntimatter: Boolean = false,
    override val position: MPoint = new MPoint(),
    override val velocity: MVec = new MVec(),
    override val gravityAcc: MVec = new MVec(),
    val engineAcc: MVec = new MVec(),
    @specialized var rotation: Double = 0,
    override val controlledByPlayer: String,
    val gravityManipulationEffect: MGravityManipulationEffect = new MGravityManipulationEffect(),
    val shootingEffect: MShootingEffect = new MShootingEffect()
)(implicit override val gameEngine: HostGameEngine)
    extends MHostBody(initialMass, initialHealth, initialGCoefficient, initialAntimatter)
    with Ship {

  override def vMax: Double               = gameEngine.gameParameters.shipVMaxPerTick
  protected def navigationPenalty: Double = gameEngine.gameParameters.navigationPenalty(mass)

  override def health_=(value: Double): Unit = {
    super.health_=(value)
    if (isDestroyed) gameEngine.getPlayer(controlledByPlayer).isDestroyed = true
  }

  override def mass_=(value: Double): Unit = {
    super.mass_=(value)
    if (isDestroyed) gameEngine.getPlayer(controlledByPlayer).isDestroyed = true
  }

  override def onMove(): Unit = {
    velocityUpdate(velocity)
    position += velocity
  }

  override def velocityUpdate(v: MVec): Unit = {
    val calculatedNavigationPenalty = navigationPenalty
    v += gravityAcc
    v += (calculatedNavigationPenalty * engineAcc.x, calculatedNavigationPenalty * engineAcc.y)

    slowVelocity(v)
  }

  override def onTick(): Unit = {
    super.onTick()
    gravityManipulationEffect.tick(this)
    shootingEffect.tick(this)
  }

  override def toImClientType: ImClientShip =
    ImClientShip(
      id,
      mass.toFloat,
      health.toFloat,
      gCoefficient.toFloat,
      position.toImmutable,
      velocity.toImmutable,
      controlledByPlayer
    )

  override def toString: String =
    s"MHostShip($id, $mass, $health, $gCoefficient, $antimatter, $position, $velocity, $gravityAcc, " +
      s"$engineAcc, $rotation, $controlledByPlayer, $gravityManipulationEffect, $shootingEffect)"

}

class MHostBomb(
    @specialized override val id: Double,
    @specialized initialMass: Double,
    @specialized initialHealth: Double = 1,
    @specialized initialGCoefficient: Double = 1,
    initialAntimatter: Boolean = false,
    override val position: MPoint,
    override val velocity: MVec = new MVec(),
    override val gravityAcc: MVec = new MVec(),
    override val firedByBodyId: Double,
    var stillInTheCannon: Boolean = true,
    var isCollidingWithMotherShip: Boolean = true
)(implicit override val gameEngine: HostGameEngine)
    extends MHostBody(initialMass, initialHealth, initialGCoefficient, initialAntimatter)
    with Bomb {

  override def health_=(value: Double): Unit = _health = 1

  override def isAffectedByGravity: Boolean = !stillInTheCannon

  override def onDestruction(): Unit = ()

  override def onTick(): Unit = {
    if (stillInTheCannon && !isCollidingWithMotherShip) stillInTheCannon = false
    isCollidingWithMotherShip = false
  }

  override def slowVelocity(v: MVec): Unit =
    slowVelocityToV(v, gameEngine.gameParameters.bombVMaxPerTick)

  override def toImClientType: ImClientBomb =
    ImClientBomb(id, mass.toFloat, position.toImmutable, velocity.toImmutable, firedByBodyId)

  override def toString: String =
    s"MHostBomb($id, $mass, $health, $gCoefficient, $antimatter, $position, $velocity, $gravityAcc, $firedByBodyId, " +
      s"$stillInTheCannon, $isCollidingWithMotherShip)"

}

class MHostDust(
    @specialized override val id: Double,
    @specialized initialMass: Double,
    @specialized initialHealth: Double = 1,
    @specialized initialGCoefficient: Double = 1,
    initialAntimatter: Boolean,
    override val position: MPoint,
    override val velocity: MVec = new MVec(),
    override val gravityAcc: MVec = new MVec()
)(implicit override val gameEngine: HostGameEngine)
    extends MHostBody(initialMass, initialHealth, initialGCoefficient, initialAntimatter)
    with Dust {

  override def health_=(value: Double): Unit = ()

  override def onDestruction(): Unit = ()

  override def toImClientType: ImClientDust =
    ImClientDust(id, mass.toFloat, antimatter, position.toImmutable, velocity.toImmutable)

  override def toString: String =
    s"MHostDust($id, $mass, $health, $gCoefficient, $antimatter, $position, $velocity, $gravityAcc)"

}
