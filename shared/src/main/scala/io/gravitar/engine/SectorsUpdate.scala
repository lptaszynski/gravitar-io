package io.gravitar.engine

case class SectorsUpdate(subscibeTo: Set[Int], unsubscribeFrom: Set[Int])
