package io.gravitar.engine

import boopickle.Pickler
import boopickle.Default._
import io.gravitar.s2d._
import io.gravitar.s2d.ImVec._
import io.gravitar.s2d.ImPoint._
import io.gravitar.engine.ImClientBody._
import io.gravitar.engine.ImPlayer._
import io.gravitar.engine.GameParameters._
import io.gravitar.engine.bhtree.ImBHSubtree
import io.gravitar.engine.bhtree.ImBHSubtree._
import io.gravitar.engine.effect.gravitymanipulation.GravityManipulationEvent
import io.gravitar.engine.effect.gravitymanipulation.GravityManipulationEvent._
import io.gravitar.engine.effect.shooting.ShootingEvent
import io.gravitar.engine.effect.shooting.ShootingEvent._

sealed trait HostDirective

sealed trait GlobalDirective extends HostDirective

case object StartOfFrame extends GlobalDirective

case object EndOfFrame extends GlobalDirective

case class SetTickN(@specialized newTickN: Int) extends GlobalDirective

case class SetGameParameters(newGameParameters: GameParameters) extends GlobalDirective

case class AddPlayer(player: ImPlayer) extends GlobalDirective

case class RemovePlayer(playerName: String) extends GlobalDirective

sealed trait SectorDirective extends HostDirective

case class ConfirmSubscription(@specialized sector: Int) extends SectorDirective

case class ConfirmUnsubscription(@specialized sector: Int) extends SectorDirective

case class BodyMovedToSector(
    body: ImClientBody,
    @specialized fromSector: Int,
    @specialized toSector: Int
) extends SectorDirective

case class BodyMovedOutFromSector(
    @specialized bodyId: Double,
    @specialized fromSector: Int,
    @specialized toSector: Int
) extends SectorDirective

case class AddBody(body: ImClientBody, @specialized sector: Int) extends SectorDirective

case class SetBodiesPosition(@specialized sector: Int, positions: Vector[Float])
    extends SectorDirective

case class RemoveBody(@specialized bodyId: Double) extends SectorDirective

case class SetBodyMass(@specialized bodyId: Double, @specialized newMass: Float)
    extends SectorDirective

case class SetBodyHealth(@specialized bodyId: Double, @specialized newHealth: Float)
    extends SectorDirective

case class SetBodyGCoefficient(@specialized bodyId: Double, @specialized newGCoefficient: Float)
    extends SectorDirective

sealed trait ConnectionDirective extends HostDirective

case class AttractiveBHSubtree(encodedMassCenters: Vector[Float]) extends ConnectionDirective

case class RepulsiveBHSubtree(encodedMassCenters: Vector[Float]) extends ConnectionDirective

object HostDirective {

  implicit val hostDirectivePickler: Pickler[HostDirective] = generatePickler[HostDirective]

}
