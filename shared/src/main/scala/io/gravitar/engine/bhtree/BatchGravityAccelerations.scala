package io.gravitar.engine.bhtree

import scala.collection.mutable

// To avoid objects creations it is encoded as repeated [bodyId, accX, accY]
case class BatchGravityAccelerations(@specialized accelerations: Vector[Double])

object BatchGravityAccelerations {

  val empty: BatchGravityAccelerations = new BatchGravityAccelerations(Vector())

  private val mergingBuffer: mutable.ArrayBuffer[Double] = mutable.ArrayBuffer()

  def merged(batchGravityAccelerations: BatchGravityAccelerations*): BatchGravityAccelerations = {
    mergingBuffer.clear()

    batchGravityAccelerations.foreach { batch =>
      mergingBuffer ++= batch.accelerations
    }

    BatchGravityAccelerations(mergingBuffer.toVector)
  }
}
