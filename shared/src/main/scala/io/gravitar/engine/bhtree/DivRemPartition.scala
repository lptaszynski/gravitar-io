package io.gravitar.engine.bhtree

import boopickle.Default._
import boopickle.Pickler

case class DivRemPartition(div: Double, rem: Double) {
  def inPartition(n: Double): Boolean = n % div == rem
  def range(exclusiveEnd: Int): Range = rem.toInt.until(exclusiveEnd).by(div.toInt)
  def toFullIndexTranslation(partialIndex: Int): Int =
    DivRemPartition.toFullIndexTranslation(div, rem, partialIndex)
}

object DivRemPartition {

  implicit val divRemPickler: Pickler[DivRemPartition] = generatePickler[DivRemPartition]

  private val single = DivRemPartition(1, 0)

  def apply(): DivRemPartition = single

  def toFullIndexTranslation(div: Double, rem: Double, partialIndex: Int): Int =
    (partialIndex * div + rem).toInt

  def partitions(div: Double): Vector[DivRemPartition] =
    (for (rem <- 0 until div.toInt) yield DivRemPartition(div, rem)).toVector
}
