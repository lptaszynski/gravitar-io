package io.gravitar.engine.bhtree

import boopickle.Default._
import boopickle.Pickler

case class ImMassCenter(
    @specialized posX: Double,
    @specialized posY: Double,
    @specialized velX: Double,
    @specialized velY: Double,
    @specialized mass: Double,
    @specialized gCoefficient: Double,
    @specialized gCoefficientDelta: Double
)

object ImMassCenter {

  implicit val imMassCenterPickler: Pickler[ImMassCenter] = generatePickler[ImMassCenter]

}
