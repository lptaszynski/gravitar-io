package io.gravitar.engine.bhtree

import io.gravitar.engine.{GameParameters, SectorMapper}
import io.gravitar.engine.bhtree._
import io.gravitar.s2d._

import scala.collection.mutable

class BHTree(
    val quad: Quad,
    val gameParameters: GameParameters,
    val sectorMapper: SectorMapper,
    val level: Int,
    val index: Int,
    val parent: Option[BHTree],
    val hostSectorMarking: Boolean
) {

  def this(
      gameParameters: GameParameters,
      sectorMapper: SectorMapper,
      hostSectorMarking: Boolean = false
  ) =
    this(
      Quad(gameParameters.center, gameParameters.size + 1000),
      gameParameters,
      sectorMapper,
      0,
      0,
      None,
      hostSectorMarking
    )

  var maybeMassCenter: Option[MassCenter] = None

  var children: Vector[BHTree] = Vector()

  val sectorsContained: mutable.Set[Int] = mutable.Set()
  val sectorMarkers: mutable.Set[Int]    = mutable.Set()

  if (level < gameParameters.sectorLevel) subdivide()

  if (hostSectorMarking && level == gameParameters.sectorLevel) {
    val quadsPath               = getQuadsPath
    val index                   = sectorMapper.sectorIndex(quadsPath)
    var optNode: Option[BHTree] = Some(this)
    while (optNode.nonEmpty) {
      val node = optNode.get
      node.sectorsContained += index
      optNode = node.parent
    }
  }

  def getQuadsPath: List[Int] = {
    if (level > 0) parent.toList.flatMap(_.getQuadsPath) :+ index else Nil
  }

  def isLeaf: Boolean = children.isEmpty

  def isSectorNode: Boolean = level == gameParameters.sectorLevel

  def insert(massCenter: MassCenter): Unit = {
    if (quad.contains(massCenter.posX, massCenter.posY)) {
      if (maybeMassCenter.isEmpty) {
        maybeMassCenter = Some(massCenter.copy)
        propagateMassCenter(massCenter)
      } else {
        maybeMassCenter.foreach { currentMassCenter =>
          if (isLeaf) {
            subdivide()
            propagateMassCenter(currentMassCenter)
          }
          currentMassCenter += massCenter
          propagateMassCenter(massCenter)
        }
      }
    }
  }

  def extractBHSubtree(sectors: Set[Int]): ImBHSubtree = {
    if (intersectNonEmpty(sectors, sectorMarkers)) {
      if (isLeaf) {
        if (intersectNonEmpty(sectors, sectorsContained)) ImBHEmptyLeaf else massCenterToLeaf
      } else {
        if (isSectorNode && intersectNonEmpty(sectors, sectorsContained)) {
          ImBHEmptyLeaf
        } else {
          val extractedChildren = children.map(_.extractBHSubtree(sectors))
          if (extractedChildren == ImBHSubtree.emptyChildren) {
            if (intersectNonEmpty(sectors, sectorsContained)) ImBHEmptyLeaf else massCenterToLeaf
          } else {
            ImBHNode(extractedChildren)
          }
        }
      }
    } else {
      ImBHEmptyLeaf
    }
  }

  def markVisitedNodes(point: Point, sector: Int): Unit =
    maybeMassCenter.foreach { thisMassCenter =>
      sectorMarkers += sector
      if (!isLeaf) {
        val distanceToMassCenter =
          guardFromZero(point.distanceTo(thisMassCenter.posX, thisMassCenter.posY))
        if ((quad.width / distanceToMassCenter) >= gameParameters.outOfSightBHTreeTheta) {
          children.foreach(_.markVisitedNodes(point, sector))
        }
      }
    }

  def applyNBodyForce(point: Point, vecAcc: MVec, excludeSelf: Boolean = true): Unit = {
    applyNBodyForceRec(point, vecAcc, excludeSelf)
    vecAcc *= gameParameters.tickGByMillis
  }

  private def applyNBodyForceRec(point: Point, vecAcc: MVec, excludeSelf: Boolean = true): Unit =
    maybeMassCenter.foreach { thisMassCenter =>
      // We skip it because that's the mass we calculate force for
      if (!isLeaf ||
          thisMassCenter.posX != point.x ||
          thisMassCenter.posY != point.y ||
          !excludeSelf) {
        if (isLeaf) {
          calculateNBodyForce(point, thisMassCenter, vecAcc)
        } else {
          val distanceToMassCenter =
            guardFromZero(point.distanceTo(thisMassCenter.posX, thisMassCenter.posY))
          if ((quad.width / distanceToMassCenter) < gameParameters.bhTreeTheta) {
            calculateNBodyForce(point, thisMassCenter, vecAcc)
          } else {
            children.foreach(_.applyNBodyForceRec(point, vecAcc, excludeSelf))
          }
        }
      }
    }

  private def calculateNBodyForce(point: Point, massCenter: MassCenter, vecAcc: MVec): Unit = {
    val distance = guardFromZero(point.distanceTo(massCenter.posX, massCenter.posY))
    val vecX     = massCenter.posX - point.x
    val vecY     = massCenter.posY - point.y
    val normVecX = vecX / distance
    val normVecY = vecY / distance
    if (massCenter.isSingleBody && distance < massCenter.radius) {
      val potential = math.sqrt(1 + 0.1 * (massCenter.radius - distance) / massCenter.radius) *
        (massCenter.mass / gameParameters.gForceDenominator(massCenter.radius))
      vecAcc += (potential * normVecX, potential * normVecY)
    } else {
      val potential = massCenter.mass / gameParameters.gForceDenominator(distance)
      vecAcc += (potential * normVecX, potential * normVecY)
    }
  }

  private def propagateMassCenter(massCenter: MassCenter): Unit =
    children.foreach(_.insert(massCenter))

  private def subdivide(): Unit = {
    val nextLevel = level + 1
    val parent    = Some(this)
    children = Vector(
      new BHTree(
        quad.leftTopQuad,
        gameParameters,
        sectorMapper,
        nextLevel,
        0,
        parent,
        hostSectorMarking
      ),
      new BHTree(
        quad.rightTopQuad,
        gameParameters,
        sectorMapper,
        nextLevel,
        1,
        parent,
        hostSectorMarking
      ),
      new BHTree(
        quad.leftBottomQuad,
        gameParameters,
        sectorMapper,
        nextLevel,
        2,
        parent,
        hostSectorMarking
      ),
      new BHTree(
        quad.rightBottomQuad,
        gameParameters,
        sectorMapper,
        nextLevel,
        3,
        parent,
        hostSectorMarking
      )
    )
  }

  protected def massCenterToLeaf: ImBHLeaf = maybeMassCenter match {
    case None => ImBHEmptyLeaf
    case Some(massCenter) =>
      ImBHLeafWithMass(massCenter.posX, massCenter.posY, massCenter.mass)
  }

  protected def intersectNonEmpty(imSet: Set[Int], mSet: mutable.Set[Int]): Boolean =
    imSet.exists(sector => mSet.contains(sector))

}
