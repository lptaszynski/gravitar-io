package io.gravitar.engine.bhtree

import collection.mutable
import boopickle.Default._
import boopickle.Pickler

sealed trait ImBHSubtree {
  def leafsWithMass: Vector[ImBHLeafWithMass]
  def toEncoded: Vector[Float] = {
    val array = mutable.ArrayBuffer[Float]()
    leafsWithMass.foreach { leafsWithMass =>
      array += leafsWithMass.positionX.toFloat
      array += leafsWithMass.positionY.toFloat
      array += leafsWithMass.mass.toFloat
    }
    array.toVector
  }
}

sealed trait ImBHLeaf extends ImBHSubtree

case class ImBHNode(children: Vector[ImBHSubtree]) extends ImBHSubtree {
  override def leafsWithMass: Vector[ImBHLeafWithMass] = children.flatMap(_.leafsWithMass)
}

case object ImBHEmptyLeaf extends ImBHLeaf {
  override val leafsWithMass: Vector[ImBHLeafWithMass] = Vector()
}

case class ImBHLeafWithMass(
    @specialized positionX: Double,
    @specialized positionY: Double,
    @specialized mass: Double
) extends ImBHLeaf {
  override def leafsWithMass: Vector[ImBHLeafWithMass] = Vector(this)
  def toMassCenter: MassCenter =
    new MassCenter(positionX, positionY, mass, radius = 0, isSingleBody = false)
}

object ImBHSubtree {

  val childrenRange: Vector[Int] = (0 to 3).toVector

  val emptyChildren: Vector[ImBHSubtree] = Vector.fill(4)(ImBHEmptyLeaf)

  implicit val imBHTreePickler: Pickler[ImBHSubtree] = generatePickler[ImBHSubtree]

}
