package io.gravitar.engine.bhtree

import io.gravitar.s2d._

class MassCenter(
    @specialized var posX: Double,
    @specialized var posY: Double,
    @specialized var mass: Double,
    @specialized var radius: Double,
    var isSingleBody: Boolean
) {

  def this(position: Point, mass: Double, radius: Double = 0, isSingleBody: Boolean = true) =
    this(position.x, position.y, mass, radius, isSingleBody = isSingleBody)

  def +=(other: MassCenter): Unit = {
    val newMass = mass + other.mass
    posX = (posX * mass + other.posX * other.mass) / newMass
    posY = (posY * mass + other.posY * other.mass) / newMass
    mass = newMass
    isSingleBody = false
    radius = 0
  }

  override def toString: String = s"MassCenter($posX, $posY, $mass, $radius, $isSingleBody)"

  def copy: MassCenter = new MassCenter(posX, posY, mass, radius, isSingleBody)
}
