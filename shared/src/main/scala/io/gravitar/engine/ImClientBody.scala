package io.gravitar.engine

import boopickle.Default._
import boopickle.Pickler
import io.gravitar.s2d._
import io.gravitar.s2d.ImPoint._
import io.gravitar.s2d.ImVec._

sealed trait ImClientBody extends Body {

  override def position: ImPoint

  def velocity: ImVec

  override def gCoefficient: Double = 1
}

case class ImClientAsteroid(
    @specialized id: Double,
    @specialized _mass: Float,
    @specialized _health: Float,
    antimatter: Boolean,
    position: ImPoint,
    velocity: ImVec,
    @specialized kind: Byte
) extends ImClientBody
    with Asteroid {
  override def mass: Double   = _mass
  override def health: Double = _health
}

case class ImClientShip(
    @specialized id: Double,
    @specialized _mass: Float,
    @specialized _health: Float,
    @specialized _gCoefficient: Float,
    position: ImPoint,
    velocity: ImVec,
    controlledByPlayer: String
) extends ImClientBody
    with Ship {
  override def mass: Double         = _mass
  override def health: Double       = _health
  override def gCoefficient: Double = _gCoefficient
  override def antimatter: Boolean  = false
}

case class ImClientBomb(
    @specialized id: Double,
    @specialized _mass: Float,
    position: ImPoint,
    velocity: ImVec,
    firedByBodyId: Double
) extends ImClientBody
    with Bomb {
  override def mass: Double        = _mass
  override def health: Double      = 1
  override def antimatter: Boolean = false
}

case class ImClientDust(
    @specialized id: Double,
    @specialized _mass: Float,
    antimatter: Boolean,
    position: ImPoint,
    velocity: ImVec
) extends ImClientBody
    with Dust {
  override def mass: Double   = _mass
  override def health: Double = 1
}

object ImClientBody {

  implicit val imClientBodyPickler: Pickler[ImClientBody] = generatePickler[ImClientBody]

}

object ImClientAsteroid {

  val numberOfKinds: Int = 4

}
