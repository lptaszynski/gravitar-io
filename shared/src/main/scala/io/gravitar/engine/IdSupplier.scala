package io.gravitar.engine

trait IdSupplier {

  type Immutable <: IdSupplier
  type Mutable <: IdSupplier

  protected def lastId: Double

  def toImmutable: Immutable
  def toMutable: Mutable

}

class MIdSupplier(@specialized protected var lastId: Double) extends IdSupplier {

  type Immutable = ImIdSupplier
  type Mutable   = MIdSupplier

  override def toImmutable: ImIdSupplier = ImIdSupplier(lastId)
  override def toMutable: MIdSupplier    = new MIdSupplier(lastId)

  def keepUpTo(id: Double): Unit = if (id > lastId) lastId = id

  def nextId(): Double = {
    lastId += 1
    lastId
  }

}

case class ImIdSupplier(lastId: Double) extends IdSupplier {

  override type Immutable = ImIdSupplier
  override type Mutable   = MIdSupplier

  override def toImmutable: ImIdSupplier = copy()
  override def toMutable: MIdSupplier    = new MIdSupplier(lastId)

}
