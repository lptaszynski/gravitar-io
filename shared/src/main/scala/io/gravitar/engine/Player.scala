package io.gravitar.engine

import boopickle.Default._
import boopickle.Pickler

sealed trait Player {
  def name: String
  def controlledShipId: Double
}

case class ImPlayer(name: String, @specialized controlledShipId: Double) extends Player {
  def toMutable = new MPlayer(name, controlledShipId)
}

class MPlayer(val name: String, @specialized val controlledShipId: Double) extends Player {

  var isNewPlayer: Boolean = true
  var isDestroyed: Boolean = false

  def toImmutable: ImPlayer = ImPlayer(name, controlledShipId)

  def resetFlags(): Unit = {
    isNewPlayer = false
  }
}

object ImPlayer {

  implicit val playerPickler: Pickler[ImPlayer] = generatePickler[ImPlayer]

}
