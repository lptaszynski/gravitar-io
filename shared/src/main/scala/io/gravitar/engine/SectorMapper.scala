package io.gravitar.engine

import io.gravitar.engine.bhtree.MassCenter
import io.gravitar.s2d._

import scala.collection.mutable

class SectorMapper(private val gameParameters: GameParameters) {

  val sectorsInDim: Int = math.pow(2, gameParameters.sectorLevel).toInt

  assume(
    (gameParameters.size % sectorsInDim) == 0,
    "Sizes are incorrect. Same size sectors can't cover entire board."
  )

  val sectorSize: Int        = gameParameters.size / sectorsInDim
  val halfSectorSize: Double = sectorSize / 2.0

  val minDim: Int       = -sectorsInDim / 2
  val absMinDim: Int    = math.abs(minDim)
  val maxDim: Int       = math.max(0, absMinDim - 1)
  val negativeDims: Int = absMinDim
  val positiveDims: Int = maxDim + 1

  val bodyIdToSector: mutable.Map[Double, Int] = mutable.Map()

  val sectorToBodyIds: Vector[mutable.SortedSet[Double]] = vectorOfSectorsSets

  val sectorToBodyIdsThatMovedTo: Vector[mutable.SortedSet[Double]]  = vectorOfSectorsSets
  val sectorToBodyIdsThatMovedOut: Vector[mutable.SortedSet[Double]] = vectorOfSectorsSets

  private def vectorOfSectorsSets: Vector[mutable.SortedSet[Double]] =
    Vector.fill(sectorsInDim * sectorsInDim)(mutable.SortedSet())

  val numberOfSectors: Int = sectorToBodyIds.size

  val sectorsRange: Range = 0 until numberOfSectors

  val sectorPath: Map[Int, List[Int]] =
    sectorsRange.map(idx => idx -> calculateQuadsPath(idx)).toMap
  val sectorIndex: Map[List[Int], Int] =
    sectorsRange.map(idx => calculateQuadsPath(idx) -> idx).toMap

  val sectorCenter: Map[Int, ImPoint] = sectorsRange
    .map(idx => idx -> ImPoint(getCenterPositionXOfSector(idx), getCenterPositionYOfSector(idx)))
    .toMap

  def sectorExists(sector: Int): Boolean =
    sector >= 0 && sector < numberOfSectors

  def getBodyIdsInSector(sector: Int): Vector[Double] =
    sectorToBodyIds(sector).toVector

  def addBody(body: Body): Unit = {
    addBodyId(body.id, body.position)
  }

  def addBodyId(bodyId: Double, point: Point): Unit = {
    val sector = getSectorOf(point)
    addBodyIdToSector(bodyId, sector)
  }

  def addBodyIdToSector(bodyId: Double, sector: Int): Unit = {
    sectorToBodyIds(sector) += bodyId
    bodyIdToSector.put(bodyId, sector)
  }

  def removeBodyId(bodyId: Double): Unit = {
    val sector = bodyIdToSector(bodyId)
    sectorToBodyIds(sector) -= bodyId
    bodyIdToSector -= bodyId
    //TODO optimize?
    sectorToBodyIdsThatMovedTo.foreach(_ -= bodyId)
    sectorToBodyIdsThatMovedOut.foreach(_ -= bodyId)
  }

  def computeSectorTransitions(bodies: Iterator[Body]): Unit = {
    bodies.foreach { body =>
      val registeredSector = bodyIdToSector(body.id)
      val actualSector     = getSectorOf(body.position)
      if (registeredSector != actualSector) {
        sectorToBodyIdsThatMovedTo(actualSector) += body.id
        sectorToBodyIdsThatMovedOut(registeredSector) += body.id
      }
    }
  }

  def addSectorTransition(bodyId: Double, fromSector: Int, toSector: Int): Unit = {
    sectorToBodyIdsThatMovedTo(toSector) += bodyId
    sectorToBodyIdsThatMovedOut(fromSector) += bodyId
  }

  def applySectorTransitions(): Unit = {
    var idx = 0
    sectorToBodyIdsThatMovedTo.foreach { bodyIds =>
      bodyIds.foreach { bodyId =>
        val registeredSector = bodyIdToSector(bodyId)
        bodyIdToSector.update(bodyId, idx)
        sectorToBodyIds(idx) += bodyId
        sectorToBodyIds(registeredSector) -= bodyId
      }
      idx += 1
    }
    sectorToBodyIdsThatMovedTo.foreach(_.clear())
    sectorToBodyIdsThatMovedOut.foreach(_.clear())
  }

  def getBodyIdsThatMovedToSector(sector: Int): Vector[Double] = {
    sectorToBodyIdsThatMovedTo(sector).toVector
  }

  def getBodyIdsThatMovedOutFromSector(sector: Int): Vector[Double] = {
    sectorToBodyIdsThatMovedOut(sector).toVector
  }

  def getSectorOf(point: Point): Int = getSectorOf(point.x, point.y)

  def getSectorOf(positionX: Double, positionY: Double): Int = {
    val coordinateX = sectorDimCoordinate(positionX)
    val coordinateY = sectorDimCoordinate(positionY)
    getSectorIndex(coordinateX, coordinateY)
  }

  def getSectorsWithinRangeOf(point: Point, range: Double): Set[Int] =
    getSectorsWithinRangeOf(point.x, point.y, range)

  def getSectorsWithinRangeOf(positionX: Double, positionY: Double, range: Double): Set[Int] = {
    val centerCoordinateX = sectorDimCoordinate(positionX)
    val centerCoordinateY = sectorDimCoordinate(positionY)

    val centerPositionX = positionOfSectorCoordinate(centerCoordinateX)
    val centerPositionY = positionOfSectorCoordinate(centerCoordinateY)

    val leftSectorsWithin =
      math.abs(((positionX - range) - (centerPositionX + halfSectorSize)) / sectorSize).toInt
    val rightSectorsWithin =
      math.abs(((positionX + range) - (centerPositionX - halfSectorSize)) / sectorSize).toInt
    val topSectorsWithin =
      math.abs(((positionY + range) - (centerPositionY - halfSectorSize)) / sectorSize).toInt
    val bottomSectorsWithin =
      math.abs(((positionY - range) - (centerPositionY + halfSectorSize)) / sectorSize).toInt

    val result = mutable.Set[Int]()

    var xCoordinate = math.max(minDim, centerCoordinateX - leftSectorsWithin)
    while (xCoordinate <= math.min(maxDim, centerCoordinateX + rightSectorsWithin)) {
      var yCoordinate = math.max(minDim, centerCoordinateY - bottomSectorsWithin)
      while (yCoordinate <= math.min(maxDim, centerCoordinateY + topSectorsWithin)) {
        result += getSectorIndex(xCoordinate, yCoordinate)
        yCoordinate += 1
      }
      xCoordinate += 1
    }

    result.toSet
  }

  def sectorDimCoordinate(posDim: Double): Int = {
    if (posDim >= 0) {
      math.min(maxDim, posDim.toInt / sectorSize)
    } else {
      math.max(minDim, (posDim.toInt / sectorSize) - 1)
    }
  }

  def getSectorIndex(coordinateX: Int, coordinateY: Int): Int = {
    val j = coordinateX + absMinDim
    val i = -coordinateY + maxDim
    i * sectorsInDim + j
  }

  def getCoordinateXOfSectorIndex(sector: Int): Int = {
    sector % sectorsInDim - absMinDim
  }

  def getCoordinateYOfSectorIndex(sector: Int): Int = {
    -sector / sectorsInDim + maxDim
  }

  def getCenterPositionXOfSector(sector: Int): Double = {
    val xCoordinate = getCoordinateXOfSectorIndex(sector)
    positionOfSectorCoordinate(xCoordinate)
  }

  def getCenterPositionYOfSector(sector: Int): Double = {
    val yCoordinate = getCoordinateYOfSectorIndex(sector)
    positionOfSectorCoordinate(yCoordinate)
  }

  protected def positionOfSectorCoordinate(dim: Int): Double =
    dim * sectorSize + halfSectorSize

  private def calculateSectorIndex(quadsPath: List[Int]): Int = {
    var sectorsInQuad     = sectorsInDim
    var halfSectorsInQuad = sectorsInQuad / 2
    var index             = 0
    quadsPath.foreach { quadIndex =>
      val rem = quadIndex % 2
      val div = quadIndex / 2
      index += div * sectorsInDim * halfSectorsInQuad + rem * halfSectorsInQuad
      sectorsInQuad /= 2
      halfSectorsInQuad /= 2
    }
    index
  }

  private def calculateQuadsPath(sectorIndex: Int): List[Int] = {
    var sectorsInQuadDim = sectorsInDim / 2
    var lvl              = 0
    var index            = sectorIndex
    val resultArray      = mutable.ArrayBuffer[Int]()
    while (lvl < gameParameters.sectorLevel) {
      val rem    = index % sectorsInDim
      val div    = index / sectorsInDim
      var result = 0
      if (rem >= sectorsInQuadDim) {
        result += 1
        index -= sectorsInQuadDim
      }
      if (div >= sectorsInQuadDim) {
        result += 2
        index -= sectorsInQuadDim * sectorsInDim
      }
      resultArray += result
      sectorsInQuadDim /= 2
      lvl += 1
    }
    resultArray.toList
  }

}
