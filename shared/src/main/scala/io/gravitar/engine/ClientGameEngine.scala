package io.gravitar.engine

import io.gravitar.engine.bhtree._
import io.gravitar.pubsub.Subscriber
import io.gravitar.s2d._

import scala.collection.mutable

trait ClientGameEngine extends GameEngine with Subscriber[Int] {

  type EMBody <: MClientBody
  type EMShip <: MClientShip

  implicit val engine: ClientGameEngine = this

  protected val bodyIdToBody: mutable.Map[Double, EMBody] = mutable.Map()

  protected var outOfSightAttractiveMassCenters: Vector[MassCenter] = Vector()
  protected var outOfSightRepulsiveMassCenters: Vector[MassCenter]  = Vector()

  protected val playerNameToPlayer: mutable.Map[String, MPlayer] = mutable.Map()

  protected val auxPoint1 = new MPoint()

  protected val auxVec1 = new MVec()

  protected val auxVec2 = new MVec()

  protected val auxVec3 = new MVec()

  def imBodyToBodyType(imBody: ImClientBody): EMBody

  def getTickN: Int = tickN

  def handleHostDirectives(hostDirectives: Vector[HostDirective]): Unit = {
    hostDirectives.foreach(handleHostDirective)
  }

  def handleHostDirective(hostDirective: HostDirective): Unit = {
    hostDirective match {
      case StartOfFrame                      => handleStartOfFrame()
      case EndOfFrame                        => handleEndOfFrame()
      case directive: SetTickN               => handleSetTickN(directive)
      case directive: SetGameParameters      => handleSetGameParameters(directive)
      case directive: ConfirmSubscription    => handleConfirmSubscription(directive)
      case directive: ConfirmUnsubscription  => handleConfirmUnsubscription(directive)
      case directive: AddPlayer              => handleAddPlayer(directive)
      case directive: RemovePlayer           => handleRemovePlayer(directive)
      case directive: BodyMovedToSector      => handleBodyMovedToSector(directive)
      case directive: BodyMovedOutFromSector => handleBodyMovedOutFromSector(directive)
      case directive: AddBody                => handleAddBody(directive)
      case directive: RemoveBody             => handleRemoveBody(directive)
      case directive: SetBodiesPosition      => handleSetBodiesPosition(directive)
      case directive: SetBodyMass            => handleSetBodyMass(directive)
      case directive: SetBodyHealth          => handleSetBodyHealth(directive)
      case directive: SetBodyGCoefficient    => handleSetBodyGCoefficient(directive)
      case directive: AttractiveBHSubtree    => handleAttractiveBHSubtree(directive)
      case directive: RepulsiveBHSubtree     => handleRepulsiveBHSubtree(directive)
    }
  }

  def handleStartOfFrame(): Unit = ()

  def handleEndOfFrame(): Unit = {
    sectorMapper.applySectorTransitions()
    removeBodiesOutOfSight()
    setTick(tickN + 1)
  }

  def handleSetTickN(setTickN: SetTickN): Unit = setTick(setTickN.newTickN)

  def handleSetGameParameters(directive: SetGameParameters): Unit =
    setGameParameters(directive.newGameParameters)

  def handleConfirmSubscription(confirmSubscription: ConfirmSubscription): Unit = {
    subscriptions += confirmSubscription.sector
  }

  def handleConfirmUnsubscription(confirmUnsubscription: ConfirmUnsubscription): Unit = {
    subscriptions -= confirmUnsubscription.sector
    val bodiesToRemove = sectorMapper.getBodyIdsInSector(confirmUnsubscription.sector)
    bodiesToRemove.foreach(removeBody)
  }

  def handleAttractiveBHSubtree(attractiveBHSubtree: AttractiveBHSubtree): Unit = {
    outOfSightAttractiveMassCenters = decodeMassCenters(attractiveBHSubtree.encodedMassCenters)
  }

  def handleRepulsiveBHSubtree(repulsiveBHSubtree: RepulsiveBHSubtree): Unit = {
    outOfSightRepulsiveMassCenters = decodeMassCenters(repulsiveBHSubtree.encodedMassCenters)
  }

  def handleAddPlayer(directive: AddPlayer): Unit = addPlayer(directive.player.toMutable)

  def handleRemovePlayer(directive: RemovePlayer): Unit = removePlayer(directive.playerName)

  def handleBodyMovedToSector(directive: BodyMovedToSector): Unit = {
    if (!bodyIdToBody.contains(directive.body.id)) {
      addBody(directive.body)
      sectorMapper.addBodyIdToSector(directive.body.id, directive.fromSector)
    }
    sectorMapper.addSectorTransition(directive.body.id, directive.fromSector, directive.toSector)
  }

  def handleBodyMovedOutFromSector(directive: BodyMovedOutFromSector): Unit = {
    sectorMapper.addSectorTransition(directive.bodyId, directive.fromSector, directive.toSector)
  }

  def handleAddBody(directive: AddBody): Unit = {
    if (!bodyIdToBody.contains(directive.body.id)) {
      addBody(directive.body)
      sectorMapper.addBodyIdToSector(directive.body.id, directive.sector)
    }
  }

  def handleRemoveBody(directive: RemoveBody): Unit = removeBody(directive.bodyId)

  def handleSetBodyMass(directive: SetBodyMass): Unit =
    setBodyMass(directive.bodyId, directive.newMass)

  def handleSetBodyHealth(directive: SetBodyHealth): Unit =
    setBodyHealth(directive.bodyId, directive.newHealth)

  def handleSetBodyGCoefficient(directive: SetBodyGCoefficient): Unit =
    setBodyGCoefficient(directive.bodyId, directive.newGCoefficient)

  def handleSetBodiesPosition(directive: SetBodiesPosition): Unit = {
    var idx = 0
    sectorMapper.getBodyIdsInSector(directive.sector).foreach { bodyId =>
      val x = directive.positions(idx)
      val y = directive.positions(idx + 1)
      setBodyPosition(bodyId, x, y)
      idx += 2
    }
  }

  def tickBodiesSmoothly(dFraction: Double): Unit = {
    bodyIdToBody.valuesIterator.foreach(_.tickVisible(dFraction))
  }

  def removeBodiesOutOfSight(): Unit = {
    sectorMapper.sectorsRange.filterNot(subscriptions.contains).foreach { sector =>
      val bodyIds = sectorMapper.getBodyIdsInSector(sector)
      bodyIds.foreach(removeBody)
    }
  }

  def addBody(body: ImClientBody): Unit = {
    bodyIdToBody.put(body.id, imBodyToBodyType(body))
  }

  def getPlayerShipOpt(playerName: String): Option[EMShip] = {
    playerNameToPlayer
      .get(playerName)
      .map(_.controlledShipId)
      .flatMap(bodyId => bodyIdToBody.get(bodyId).map(_.as[EMShip]))
  }

  def getPlayerShip(playerName: String): EMShip =
    getBody(getPlayerShipId(playerName)).as[EMShip]

  def getBody(bodyId: Double): EMBody = bodyIdToBody(bodyId)

  def getPlayerShipId(playerName: String): Double =
    playerNameToPlayer(playerName).controlledShipId

  def getPlayer(playerName: String): MPlayer =
    playerNameToPlayer(playerName)

  def addPlayer(player: MPlayer): Unit = {
    playerNameToPlayer.put(player.name, player)
  }

  def removePlayer(playerName: String): Unit = {
    playerNameToPlayer -= playerName
  }

  def removeBody(bodyId: Double): Unit = {
    sectorMapper.removeBodyId(bodyId)
    bodyIdToBody -= bodyId
  }

  protected def setBodyMass(bodyId: Double, newMass: Double): Unit =
    bodyIdToBody(bodyId).mass = newMass

  protected def setBodyHealth(bodyId: Double, newHealth: Double): Unit =
    bodyIdToBody(bodyId).health = newHealth

  protected def setBodyGCoefficient(bodyId: Double, newGCoefficient: Double): Unit =
    bodyIdToBody(bodyId).gCoefficient = newGCoefficient

  protected def setBodyPosition(bodyId: Double, newPosition: ImPoint): Unit =
    setBodyPosition(bodyId: Double, newPosition.x, newPosition.y)

  protected def setBodyPosition(bodyId: Double, x: Double, y: Double): Unit =
    bodyIdToBody(bodyId).setPositionTo(x, y)

  protected def decodeMassCenters(encodedMassCenters: Vector[Float]): Vector[MassCenter] = {
    val size   = encodedMassCenters.length / 3
    var n      = 0
    val buffer = mutable.ArrayBuffer[MassCenter]()
    while (n < size) {
      val idx  = 3 * n
      val posX = encodedMassCenters(idx)
      val posY = encodedMassCenters(idx + 1)
      val mass = encodedMassCenters(idx + 2)
      buffer += new MassCenter(posX, posY, mass, 100, true)
      n += 1
    }
    buffer.toVector
  }

}
