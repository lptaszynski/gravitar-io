package io.gravitar.engine

import io.gravitar.s2d._

trait Body {

  type ImClientType <: ImClientBody

  def id: Double
  def mass: Double
  def health: Double
  def antimatter: Boolean
  def gCoefficient: Double // < 0 means repulsive force, 1 by default
  def position: Point

  def isRepulsive: Boolean  = !isAttractive
  def isAttractive: Boolean = gCoefficient >= 0

  def calculateRadius: Double = 4 * math.pow(mass, 0.25)

  def isShip: Boolean     = isInstanceOf[Ship]
  def isAsteroid: Boolean = isInstanceOf[Asteroid]
  def isBomb: Boolean     = isInstanceOf[Bomb]
  def isDust: Boolean     = isInstanceOf[Dust]

}

trait Asteroid extends Body {

  type ImClientType = ImClientAsteroid

  def kind: Byte

}

trait Ship extends Body {

  type ImClientType = ImClientShip

  def controlledByPlayer: String

}

trait Bomb extends Body {

  type ImClientType = ImClientBomb

  def firedByBodyId: Double

}

trait Dust extends Body {

  type ImClientType = ImClientDust

}
