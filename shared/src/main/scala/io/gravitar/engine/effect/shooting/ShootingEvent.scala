package io.gravitar.engine.effect.shooting

import boopickle.Default._
import boopickle.Pickler

sealed trait ShootingEvent
case object StartShooting extends ShootingEvent
case object StopShooting  extends ShootingEvent

object ShootingEvent {

  implicit val bombShootingEventPickler: Pickler[ShootingEvent] = generatePickler[ShootingEvent]

}
