package io.gravitar.engine.effect.gravitymanipulation

import io.gravitar.engine.MHostShip

class MGravityManipulationEffect(
    var lastEvent: GravityManipulationEvent = StopGravityManipulation,
    @specialized var stack: Int = 0
) {

  def tick(mBody: MHostShip): Unit = {
    val signum = nextSignum(mBody)
    stack += signum
    mBody.gCoefficient += mBody.gameEngine.gameParameters.gCoefficientPerTick * signum
  }

  def nextSignum(mBody: MHostShip): Int = {
    val gCoefficientMaxAbsIsReached =
      mBody.gCoefficient >= mBody.gameEngine.gameParameters.maxGCoefficient ||
        mBody.gCoefficient <= mBody.gameEngine.gameParameters.minGCoefficient
    lastEvent match {
      case Attract if gCoefficientMaxAbsIsReached && stack > 0 => 0
      case Attract                                             => 1
      case Repulse if gCoefficientMaxAbsIsReached && stack < 0 => 0
      case Repulse                                             => -1
      case StopGravityManipulation                             => 0
    }
  }

  def handleEvent(event: GravityManipulationEvent): Unit = {
    lastEvent = event
  }

}
