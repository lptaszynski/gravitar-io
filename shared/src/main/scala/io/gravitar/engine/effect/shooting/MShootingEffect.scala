package io.gravitar.engine.effect.shooting

import io.gravitar.engine._
import io.gravitar.s2d._

class MShootingEffect(
    var lastEvent: ShootingEvent = StopShooting,
    var lastShotTick: Int = 0,
    var tick: Int = 0
) {

  def tick(mShip: MHostShip): Unit = {
    if (isActive && (tick - lastShotTick) > mShip.gameEngine.gameParameters.bombReloadTicks) {
      val bombId    = mShip.gameEngine.nextId()
      val bPosition = mShip.position.copy()
      val bVelocity = new MVec(1, 0)
      bVelocity.rotate(mShip.rotation)

      bVelocity *= mShip.gameEngine.gameParameters.bombVMaxPerTick
      bVelocity += mShip.velocity

      val bombMass = mShip.gameEngine.gameParameters.bombMassRatio * mShip.mass
      if (bombMass > mShip.gameEngine.gameParameters.minMass) {
        val bomb = new MHostBomb(
          id = bombId,
          initialMass = bombMass,
          initialAntimatter = mShip.antimatter,
          position = bPosition,
          velocity = bVelocity,
          firedByBodyId = mShip.id
        )(gameEngine = mShip.gameEngine)

        mShip.mass -= bombMass

        val massRatio = 2 * bomb.mass / mShip.mass

        mShip.velocity -= (massRatio * bomb.velocity.x, massRatio * bomb.velocity.y)

        mShip.gameEngine.addBody(bomb)
      }

      lastShotTick = tick
    }

    tick += 1
  }

  def isActive: Boolean = lastEvent.isInstanceOf[StartShooting.type]

  def handleEvent(event: ShootingEvent)(implicit engine: GameEngine): Unit = {
    lastEvent = event
  }

}
