package io.gravitar.engine.effect.gravitymanipulation

import boopickle.Default._
import boopickle.Pickler

sealed trait GravityManipulationEvent
case object Attract                 extends GravityManipulationEvent
case object Repulse                 extends GravityManipulationEvent
case object StopGravityManipulation extends GravityManipulationEvent

object GravityManipulationEvent {

  implicit val gravityManipulationEventPickler: Pickler[GravityManipulationEvent] =
    generatePickler[GravityManipulationEvent]

}
