package io.gravitar.engine

import boopickle.Pickler
import boopickle.Default._
import io.gravitar.s2d._
import io.gravitar.s2d.ImVec._
import io.gravitar.engine.effect.gravitymanipulation.GravityManipulationEvent
import io.gravitar.engine.effect.gravitymanipulation.GravityManipulationEvent._
import io.gravitar.engine.effect.shooting.ShootingEvent
import io.gravitar.engine.effect.shooting.ShootingEvent._

sealed trait PlayerDeclaration

case class DeclareShipEngineAcc(newEngineAcc: ImVec) extends PlayerDeclaration

case class DeclareShipRotation(@specialized newRotation: Float) extends PlayerDeclaration

case class DeclareGravityManipulationEvent(gravityManipulationEvent: GravityManipulationEvent)
    extends PlayerDeclaration

case class DeclareShootingEvent(shootingEvent: ShootingEvent) extends PlayerDeclaration

object PlayerDeclaration {

  implicit val clientDeclarationPickler: Pickler[PlayerDeclaration] =
    generatePickler[PlayerDeclaration]

}
