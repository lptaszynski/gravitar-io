package io.gravitar.engine

import boopickle.Default._
import boopickle.Pickler

case class Summary(
    tick: Int,
    shipsCount: Int,
    asteroidsCount: Int,
    dustCount: Int,
    matter: Double,
    antimatter: Double
)

object Summary {

  def apply(): Summary = new Summary(0, 0, 0, 0, 0, 0)

  implicit val summaryPickler: Pickler[Summary] = generatePickler[Summary]

}
