package io.gravitar.engine

import io.gravitar.engine.bhtree._
import io.gravitar.engine.collision._
import io.gravitar.engine.effect._
import io.gravitar.engine.effect.gravitymanipulation.GravityManipulationEvent
import io.gravitar.engine.effect.shooting.ShootingEvent
import io.gravitar.pubsub._
import io.gravitar.s2d._

import scala.collection.mutable
import scala.util.Random

trait HostGameEngine extends GameEngine with Publisher[Int, Long] {

  implicit val engine: HostGameEngine = this

  protected var idSupplier: MIdSupplier = new MIdSupplier(0)

  protected val bodyIdToBody: mutable.Map[Double, MHostBody] = mutable.Map()

  protected val playerNameToPlayer: mutable.Map[String, MPlayer] = mutable.Map()

  protected var attractiveBHTree: BHTree = null
  protected var repulsiveBHTree: BHTree  = null

  protected val auxPoint1 = new MPoint()

  protected val auxVec1 = new MVec()

  protected val auxVec2 = new MVec()

  protected val auxVec3 = new MVec()

  protected var collisionHandler: CollisionHandler = new CollisionHandler(gameParameters)

  protected val delayedOperations: mutable.ArrayBuffer[Unit => Unit] = mutable.ArrayBuffer()

  protected val declaredRotations: mutable.SortedMap[Double, Double] = mutable.SortedMap()

  protected val declaredEngineAccelerations: mutable.SortedMap[Double, ImVec] = mutable.SortedMap()

  protected val declaredShootingEvents: mutable.SortedMap[Double, ShootingEvent] =
    mutable.SortedMap()

  protected val declaredGravityManipulationEvents
      : mutable.SortedMap[Double, GravityManipulationEvent] = mutable.SortedMap()

  protected var lastSummary: Summary = Summary()

  protected def isRegenerationTick: Boolean = (tickN % gameParameters.tickFrequency) == 0

  def getCurrentLeaderboard: Leaderboard = {
    val scores = playerNameToPlayer.values
      .flatMap(player => getPlayerShipOpt(player.name))
      .map(ship => PlayerScore(ship.controlledByPlayer, ship.mass))
      .toVector
    Leaderboard(scores.sortBy(playerScore => -playerScore.score).take(10))
  }

  def getCurrentSummary: Summary = lastSummary

  def getImBody(bodyId: Double): ImClientBody =
    bodyIdToBody(bodyId).toImClientType

  def initialGlobalDirectives: Vector[GlobalDirective] = {
    val buffer     = mutable.ArrayBuffer[GlobalDirective]()
    val addPlayers = playerNameToPlayer.valuesIterator.map(p => AddPlayer(p.toImmutable))
    buffer += SetTickN(tickN)
    buffer += SetGameParameters(gameParameters)
    buffer ++= addPlayers
    buffer.toVector
  }

  def initialSectorDirectives(sector: Int): Vector[SectorDirective] = {
    val bodyIds = sectorMapper.getBodyIdsInSector(sector)
    bodyIds.map(bodyIdToBody).map(_.toImClientType).map(body => AddBody(body, sector))
  }

  def preTick(): Unit = {
    applyDeclaredRotations()
    applyDeclaredEngineAccelerations()
    applyDeclaredShootingEvents()
    applyDeclaredGravityManipulationEvents()
  }

  def tick(): Unit = {
    setTick(tickN + 1)
    tickBodies()
    computeSectorTransitions()
  }

  def postTick(): Unit = {
    sectorMapper.applySectorTransitions()

    removeDestroyedBodies()
    resetBodiesFlags()

    removeDestroyedPlayers()
    resetPlayersFlags()

    calculateSummary()
  }

  def globalDirectivesForThisTick: Vector[GlobalDirective] = {
    val buffer = mutable.ArrayBuffer[GlobalDirective]()
    buffer += StartOfFrame
    addPlayersDirectives(buffer)
    buffer.toVector
  }

  def sectorsDirectivesForThisTick: Vector[Vector[SectorDirective]] =
    sectorMapper.sectorsRange.map(sectorDirectivesForThisTick).toVector

  def sectorDirectivesForThisTick(sector: Int): Vector[SectorDirective] = {
    val buffer = mutable.ArrayBuffer[SectorDirective]()

    val bodiesMovedIn = sectorMapper
      .getBodyIdsThatMovedToSector(sector)
      .map(bodyIdToBody(_))
      .filterNot(body => body.isDestroyed || body.isNewBody)
      .map(_.toImClientType)
      .map { body =>
        val fromSector = sectorMapper.bodyIdToSector(body.id)
        BodyMovedToSector(body, fromSector, sector)
      }
    bodiesMovedIn.foreach(buffer += _)

    val bodies = sectorMapper.getBodyIdsInSector(sector).map(bodyIdToBody)

    bodies.foreach(addBodyUpdateDirectives(_, buffer))

    val positionsBuffer = mutable.ArrayBuffer[Float]()
    bodies.foreach { body =>
      positionsBuffer += body.position.x.toFloat
      positionsBuffer += body.position.y.toFloat
    }
    buffer += SetBodiesPosition(sector, positionsBuffer.toVector)

    val bodiesMovedOut = sectorMapper
      .getBodyIdsThatMovedOutFromSector(sector)
      .map { bodyId =>
        val body     = bodyIdToBody(bodyId)
        val toSector = sectorMapper.getSectorOf(body.position)
        BodyMovedOutFromSector(bodyId, sector, toSector)
      }
    bodiesMovedOut.foreach(buffer += _)

    bodies.foreach(addBodyRemoveDirectives(_, buffer))
    buffer.toVector
  }

  def connectionDirectivesForThisTick(connectionId: Long): Vector[ConnectionDirective] = {
    val sectors = subscriptionsOf(connectionId)
    val attractiveBHSubtree = AttractiveBHSubtree(
      attractiveBHTree.extractBHSubtree(sectors).toEncoded
    )
    val repulsiveBHSubtree = RepulsiveBHSubtree(repulsiveBHTree.extractBHSubtree(sectors).toEncoded)
    Vector(attractiveBHSubtree, repulsiveBHSubtree)
  }

  def removeDestroyedPlayers(): Unit = {
    playerNameToPlayer.values.filter(_.isDestroyed).foreach { player =>
      removePlayer(player.name)
    }
  }

  def removeDestroyedBodies(): Unit = {
    bodyIdToBody.values.filter(_.isDestroyed).foreach { body =>
      removeBody(body.id)
    }
  }

  def handlePlayerDeclaration(player: MPlayer, playerDeclaration: PlayerDeclaration): Unit = {
    playerDeclaration match {
      case playerDeclaration: DeclareShipRotation =>
        handleSetShipRotation(player, playerDeclaration)
      case playerDeclaration: DeclareShipEngineAcc =>
        handleSetShipEngineAcc(player, playerDeclaration)
      case playerDeclaration: DeclareShootingEvent =>
        handleUpdateShootingEvent(player, playerDeclaration)
      case playerDeclaration: DeclareGravityManipulationEvent =>
        handleUpdateGravityManipulationEvent(player, playerDeclaration)
    }
  }

  protected def handleSetShipRotation(player: MPlayer, directive: DeclareShipRotation): Unit = {
    declaredRotations.put(player.controlledShipId, directive.newRotation)
  }

  protected def handleSetShipEngineAcc(player: MPlayer, directive: DeclareShipEngineAcc): Unit = {
    declaredEngineAccelerations.put(player.controlledShipId, directive.newEngineAcc)
  }

  protected def handleUpdateShootingEvent(
      player: MPlayer,
      directive: DeclareShootingEvent
  ): Unit = {
    declaredShootingEvents.put(player.controlledShipId, directive.shootingEvent)
  }

  protected def handleUpdateGravityManipulationEvent(
      player: MPlayer,
      directive: DeclareGravityManipulationEvent
  ): Unit = {
    declaredGravityManipulationEvents.put(
      player.controlledShipId,
      directive.gravityManipulationEvent
    )
  }

  protected def applyDeclaredRotations(): Unit = {
    declaredRotations.foreach {
      case (shipId, rotation) =>
        setShipRotation(shipId, rotation)
    }
    declaredRotations.clear()
  }

  protected def applyDeclaredEngineAccelerations(): Unit = {
    declaredEngineAccelerations.foreach {
      case (shipId, engineAcc) =>
        setShipEngineAcc(shipId, engineAcc.x, engineAcc.y)
    }
    declaredEngineAccelerations.clear()
  }

  protected def applyDeclaredShootingEvents(): Unit = {
    declaredShootingEvents.foreach {
      case (shipId, shootingEvent) =>
        val ship = bodyIdToBody(shipId).as[MHostShip]
        ship.shootingEffect.handleEvent(shootingEvent)
    }
    declaredShootingEvents.clear()
  }

  protected def applyDeclaredGravityManipulationEvents(): Unit = {
    declaredGravityManipulationEvents.foreach {
      case (shipId, gravityManipulationEvent) =>
        val ship = bodyIdToBody(shipId).as[MHostShip]
        ship.gravityManipulationEffect.handleEvent(gravityManipulationEvent)
    }
    declaredGravityManipulationEvents.clear()
  }

  protected def calculateSummary(): Unit = {
    var ships      = 0
    var asteroids  = 0
    var dusts      = 0
    var antimatter = 0.0
    var matter     = 0.0
    getAllBodies.foreach { body =>
      if (body.antimatter) antimatter += body.mass else matter += body.mass
      if (body.isDust) dusts += 1
      else if (body.isAsteroid) asteroids += 1
      else if (body.isShip) ships += 1
    }
    lastSummary = Summary(tickN, ships, asteroids, dusts, matter, antimatter)
  }

  def nextId(): Double = idSupplier.nextId()

  def getTickN: Int = tickN

  def getAllShips: Iterator[MHostShip] =
    playerNameToPlayer.valuesIterator.map(
      player => bodyIdToBody(player.controlledShipId).as[MHostShip]
    )

  def getAllAsteroids: Iterator[MHostBody] = bodyIdToBody.valuesIterator.filter(_.isAsteroid)

  def getAllDusts: Iterator[MHostBody] = bodyIdToBody.valuesIterator.filter(_.isDust)

  def getAllBodies: Iterator[MHostBody] = bodyIdToBody.valuesIterator

  def getPlayerShipOpt(playerName: String): Option[MHostShip] = {
    playerNameToPlayer
      .get(playerName)
      .map(_.controlledShipId)
      .flatMap(bodyId => bodyIdToBody.get(bodyId).map(a => a.as[MHostShip]))
  }

  def getPlayerShip(playerName: String): MHostShip =
    getBody(getPlayerShipId(playerName)).as[MHostShip]

  def getBody(bodyId: Double): MHostBody =
    bodyIdToBody(bodyId)

  def getPlayerShipId(playerName: String): Double =
    playerNameToPlayer(playerName).controlledShipId

  def getPlayer(playerName: String): MPlayer =
    playerNameToPlayer(playerName)

  def addPlayer(player: MPlayer): Unit = {
    playerNameToPlayer += player.name -> player
  }

  def removePlayer(playerName: String): Unit = {
    playerNameToPlayer -= playerName
  }

  def addBody(body: MHostBody): Unit = {
    sectorMapper.addBody(body)
    bodyIdToBody.put(body.id, body)
  }

  def removeBody(bodyId: Double): Unit = {
    sectorMapper.removeBodyId(bodyId)
    bodyIdToBody -= bodyId
  }

  protected def setBodyMass(bodyId: Double, newMass: Double): Unit =
    updateBody(bodyId)(_.mass = newMass)

  protected def setBodyHealth(bodyId: Double, newHealth: Double): Unit =
    updateBody(bodyId)(_.health = newHealth)

  protected def setBodyGCoefficient(bodyId: Double, newGCoefficient: Double): Unit =
    updateBody(bodyId)(_.gCoefficient = newGCoefficient)

  protected def setBodyPosition(bodyId: Double, newPosition: ImPoint): Unit =
    updateBody(bodyId)(_.setPositionTo(newPosition.x, newPosition.y))

  protected def updateBody(bodyId: Double)(f: MHostBody => Unit): Unit =
    f(bodyIdToBody(bodyId))

  protected def updateShip(shipId: Double)(f: MHostShip => Unit): Unit =
    f(bodyIdToBody(shipId).as[MHostShip])

  override def setGameParameters(newGameParameters: GameParameters): Unit = {
    super.setGameParameters(newGameParameters)
    collisionHandler = new CollisionHandler(newGameParameters)
    sectorMapper.sectorsRange.foreach(addChannel)
  }

  protected def setBodyVelocity(bodyId: Double, newVelocity: ImVec): Unit =
    updateBody(bodyId)(_.velocity.setTo(newVelocity))

  protected def setBodyGravityAcc(bodyId: Double, newGravityAcc: ImVec): Unit =
    setBodyGravityAcc(bodyId, newGravityAcc.x, newGravityAcc.y)

  protected def setBodyGravityAcc(bodyId: Double, x: Double, y: Double): Unit =
    updateBody(bodyId)(_.gravityAcc.setTo(x, y))

  protected def setShipEngineAcc(shipId: Double, x: Double, y: Double): Unit =
    updateBody(shipId) { body =>
      auxVec1.setTo(x, y)
      if (auxVec1.lengthSquared >= 1) auxVec1.normalize()
      auxVec1 *= gameParameters.engineAccMaxPerTick
      body.as[MHostShip].engineAcc.setTo(auxVec1.x, auxVec1.y)
    }

  protected def setShipRotation(shipId: Double, newRotation: Double): Unit =
    updateBody(shipId) { body =>
      body.as[MHostShip].rotation = guardAngle(newRotation)
    }

  protected def updateGravityManipulationEvent(
      shipId: Double,
      event: GravityManipulationEvent
  ): Unit = {
    val mShip = bodyIdToBody(shipId).as[MHostShip]
    mShip.gravityManipulationEffect.handleEvent(event)
  }

  protected def updateShootingEvent(shipId: Double, event: ShootingEvent): Unit = {
    val mShip = bodyIdToBody(shipId).as[MHostShip]
    mShip.shootingEffect.handleEvent(event)
  }

  protected def addPlayersDirectives(buffer: mutable.ArrayBuffer[GlobalDirective]): Unit = {
    playerNameToPlayer.values.foreach { player =>
      addPlayerDirective(player, buffer)
    }
  }

  protected def addPlayerDirective(
      player: MPlayer,
      buffer: mutable.ArrayBuffer[GlobalDirective]
  ): Unit = {
    if (player.isNewPlayer) buffer += AddPlayer(player.toImmutable)
    if (player.isDestroyed) buffer += RemovePlayer(player.name)
  }

  protected def addBodyUpdateDirectives(
      body: MHostBody,
      buffer: mutable.ArrayBuffer[SectorDirective]
  ): Unit = {
    if (body.isNewBody) buffer += AddBody(body.toImClientType, sectorMapper.bodyIdToSector(body.id))
    if (body.massNeedsUpdate) buffer += SetBodyMass(body.id, body.mass.toFloat)
    if (body.healthNeedsUpdate) buffer += SetBodyHealth(body.id, body.health.toFloat)
    if (body.gCoefficientNeedsUpdate)
      buffer += SetBodyGCoefficient(body.id, body.gCoefficient.toFloat)
  }

  protected def addBodyRemoveDirectives(
      body: MHostBody,
      buffer: mutable.ArrayBuffer[SectorDirective]
  ): Unit = {
    if (body.isDestroyed) buffer += RemoveBody(body.id)
  }

  protected def resetPlayersFlags(): Unit = {
    playerNameToPlayer.values.foreach(_.resetFlags())
  }

  protected def resetBodiesFlags(): Unit = {
    bodyIdToBody.values.foreach(_.resetFlags())
  }

  def createGravityTrees(): Unit = {
    attractiveBHTree = new BHTree(gameParameters, sectorMapper, hostSectorMarking = true)
    repulsiveBHTree = new BHTree(gameParameters, sectorMapper, hostSectorMarking = true)

    val (attractiveBodies, repulsiveBodies) =
      getAllBodies.partition(_.isAttractive)

    val attractiveMassCenters =
      attractiveBodies.filter(_.isAffectedByGravity).map(mBodyToMassCenter)
    val repulsiveMassCenters = repulsiveBodies.filter(_.isAffectedByGravity).map(mBodyToMassCenter)

    attractiveMassCenters.foreach(attractiveBHTree.insert)
    repulsiveMassCenters.foreach(repulsiveBHTree.insert)
  }

  def calculateBatchGravityAccelerations(
      bodyIdPartition: DivRemPartition = DivRemPartition()
  ): BatchGravityAccelerations = {
    val auxDoubleBuffer: mutable.ArrayBuffer[Double] = mutable.ArrayBuffer()
    val attractiveVec                                = new MVec()
    val repulsiveVec                                 = new MVec()

    bodyIdToBody.valuesIterator
      .filter(body => bodyIdPartition.inPartition(body.id) && body.isAffectedByGravity)
      .foreach { body =>
        attractiveVec.toZero()
        repulsiveVec.toZero()
        attractiveBHTree.applyNBodyForce(body.position, attractiveVec)
        repulsiveBHTree.applyNBodyForce(body.position, repulsiveVec)
        attractiveVec -= repulsiveVec
        applyOutOfEdgeForce(body.position, attractiveVec)
        auxDoubleBuffer += body.id
        auxDoubleBuffer += attractiveVec.x
        auxDoubleBuffer += attractiveVec.y
      }
    BatchGravityAccelerations(auxDoubleBuffer.toVector)
  }

  def markSectorsInBHTrees(): Unit = {
    sectorMapper.sectorCenter.foreach {
      case (index, center) =>
        attractiveBHTree.markVisitedNodes(center, index)
        repulsiveBHTree.markVisitedNodes(center, index)
    }
  }

  def calculateBatchCollisions(
      quadsPartition: QuadsPerLevelPartition = QuadsPerLevelPartition.allInSingle
  ): BatchCollisions = {
    val collisionDetectionTree = new CollisionDetectionTree(gameParameters, quadsPartition)
    getAllBodies.foreach(collisionDetectionTree.insert)
    collisionDetectionTree.detectCollisions
  }

  protected def tickBodies(): Unit = {
    if (isRegenerationTick) getAllBodies.foreach(_.regenHealth())
    getAllBodies.foreach(_.onTick())
    getAllBodies.foreach(_.onMove())
    getAllBodies.filter(_.isDestroyed).foreach(_.onDestruction())
  }

  protected def computeSectorTransitions(): Unit =
    sectorMapper.computeSectorTransitions(getAllBodies)

  def handleBatchBodiesAccelerations(
      batchGravityAccelerations: BatchGravityAccelerations
  ): Unit = {
    val accelerations         = batchGravityAccelerations.accelerations
    val numberOfAccelerations = accelerations.length / 3
    (0 until numberOfAccelerations).foreach { n =>
      val idx    = 3 * n
      val bodyId = accelerations(idx)
      val x      = accelerations(idx + 1)
      val y      = accelerations(idx + 2)
      setBodyGravityAcc(bodyId, x, y)
    }
  }

  def handleBatchCollisions(batchCollisions: BatchCollisions): Unit =
    batchCollisions.collisions.foreach {
      case (firstBodyId, secondBodyId) =>
        val firstBody  = bodyIdToBody(firstBodyId)
        val secondBody = bodyIdToBody(secondBodyId)
        collidePair(firstBody, secondBody)
    }

  private def collidePair(firstBody: MHostBody, secondBody: MHostBody): Unit = {
    collisionHandler.handleCollision(firstBody, secondBody)
  }

  def balance(): Unit = {
    var asteroidsToAdd =
      capValue(gameParameters.asteroidsOnArena - lastSummary.asteroidsCount, 0, 2)
    while (asteroidsToAdd > 0) {
      val id = idSupplier.nextId()

      val position =
        generateMPositionFarFromBodies(() => edgeAsteroidPositionGenerator, getAllShips)

      auxVec1.setTo(position.x - gameParameters.center.x, position.y - gameParameters.center.y)
      val velocity = auxVec1.normalized

      val antimatter = lastSummary.antimatter < lastSummary.matter
      velocity.rotate(math.Pi / 2)
      velocity *= gameParameters.vLimitPerTick

      val kind = Random.nextInt(ImClientAsteroid.numberOfKinds).toByte

      val asteroid = new MHostAsteroid(
        id = id,
        initialMass = randomAsteroidMass,
        initialAntimatter = antimatter,
        position = position,
        velocity = velocity,
        kind = kind
      )

      addBody(asteroid)
      asteroidsToAdd -= 1
    }
  }

  def generatePlayerSpawnPosition: MPoint = {
    generateMPositionFarFromBodies(
      () => ImPoint.randomInsideCircle(gameParameters.arenaRadius),
      getAllBodies
    )
  }

  private def edgeAsteroidPositionGenerator: Point = {
    auxPoint1.setTo(gameParameters.center)

    val theta = 2 * math.Pi * math.random

    auxVec1.setTo(1, 0)
    auxVec1.rotate(theta)
    auxVec1 *= gameParameters.arenaRadius + 200

    auxPoint1 += auxVec1
    auxPoint1
  }

  private def generateMPositionFarFromBodies(
      positionGenerator: () => Point,
      bodies: Iterator[Body]
  ): MPoint = {
    var attempts: Int = 0
    var position      = positionGenerator()
    while (!isFarFromBodies(position, bodies) && attempts < 32) {
      position = positionGenerator()
      attempts += 1
    }
    position.toMutable
  }

  private def isFarFromBodies(position: Point, bodies: Iterator[Body]): Boolean = {
    bodies.forall { body =>
      body.position.squareDistanceTo(position) >= gameParameters.squareSpawnDistance
    }
  }

  def mBodyToMassCenter(body: MHostBody): MassCenter =
    new MassCenter(body.position, body.mass * math.abs(body.gCoefficient), body.radius)

  protected def randomAsteroidMass: Double =
    gameParameters.minInitialAsteroidMass + math.abs(1.25 * Random.nextGaussian()) * (gameParameters.averageInitialAsteroidMass - gameParameters.minInitialAsteroidMass)

}
