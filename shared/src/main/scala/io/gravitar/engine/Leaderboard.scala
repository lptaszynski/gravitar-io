package io.gravitar.engine

import boopickle.Default._
import boopickle.Pickler
import PlayerScore._

case class Leaderboard(bestScores: Vector[PlayerScore])

object Leaderboard {

  def apply(): Leaderboard = new Leaderboard(Vector())

  implicit val leaderboardPickler: Pickler[Leaderboard] = generatePickler[Leaderboard]

}
