package io.gravitar.engine

import io.gravitar.s2d._

trait GameEngine {

  implicit var gameParameters: GameParameters = GameParameters()

  var sectorMapper: SectorMapper = new SectorMapper(gameParameters)

  protected var tickN: Int = 0

  def setTick(newTickN: Int): Unit = tickN = newTickN

  def setGameParameters(newGameParameters: GameParameters): Unit = {
    gameParameters = newGameParameters
    sectorMapper = new SectorMapper(newGameParameters)
  }

  def getOutOfEdgeForce(point: Point): ImVec = {
    val vecAcc = new MVec()
    applyOutOfEdgeForce(point, vecAcc)
    vecAcc.toImmutable
  }

  def applyOutOfEdgeForce(point: Point, vecAcc: MVec): Unit = {
    val sqDistance          = point.squareDistanceTo(gameParameters.center)
    val sqDistanceOutOfEdge = sqDistance - (gameParameters.arenaRadius * gameParameters.arenaRadius)
    if (sqDistanceOutOfEdge > 0) {
      val distance          = guardFromZero(math.sqrt(sqDistance))
      val distanceOutOfEdge = distance - gameParameters.arenaRadius
      val normVecX          = (gameParameters.center.x - point.x) / distance
      val normVecY          = (gameParameters.center.y - point.y) / distance
      val potential         = math.exp(0.01 * distanceOutOfEdge - math.E)
      vecAcc += (potential * normVecX, potential * normVecY)
    }
  }

}
