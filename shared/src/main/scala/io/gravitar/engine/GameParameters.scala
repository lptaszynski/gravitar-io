package io.gravitar.engine

import boopickle.Default._
import boopickle.Pickler
import io.gravitar.s2d.ImPoint._
import io.gravitar.s2d._

case class GameParameters(
    @specialized tickFrequency: Int = 0,
    @specialized arenaRadius: Int = 0,
    @specialized sectorLevel: Int = 0,
    @specialized asteroidsOnArena: Int = 0,
    @specialized gDistanceExp: Double = 0,
    @specialized g: Double = 0,
    @specialized gMeshAmplifier: Double = 0,
    @specialized bhTreeTheta: Double = 0,
    @specialized outOfSightBHTreeTheta: Double = 0,
    @specialized spawnDistance: Double = 0,
    @specialized initialShipMass: Double = 0,
    @specialized minMass: Double = 0,
    @specialized minInitialAsteroidMass: Double = 0,
    @specialized averageInitialAsteroidMass: Double = 0,
    @specialized collisionRestitution: Double = 0,
    @specialized dustCollisionHardness: Double = 0,
    @specialized minGCoefficient: Double = 0,
    @specialized maxGCoefficient: Double = 0,
    @specialized gCoefficientPerSecond: Double = 0,
    @specialized vLimitPerSecond: Double = 0,
    @specialized shipVMaxPerSecond: Double = 0,
    @specialized freeBodyVMaxPerSecond: Double = 0,
    @specialized engineAccMaxPerSecond: Double = 0,
    @specialized bombVMaxPerSecond: Double = 0,
    @specialized bombMassRatio: Double = 0,
    @specialized bombsPerSecond: Double = 0,
    @specialized healthRegenPerSecond: Double = 0,
    @specialized energyOfMassUnit: Double = 0
) {

  val center: ImPoint = ImPoint()

  val tickMillis: Double = 1000.0 / tickFrequency

  val tickGByMillis: Double = g * tickMillis

  val size: Int = 2 * arenaRadius

  val squareSpawnDistance: Double = spawnDistance * spawnDistance

  val sectorsInDim: Int = math.pow(2, sectorLevel).toInt

  val gCoefficientPerTick: Double = gCoefficientPerSecond / tickFrequency

  val vLimitPerTick: Double = vLimitPerSecond / tickFrequency

  val shipVMaxPerTick: Double = shipVMaxPerSecond / tickFrequency

  val freeBodyVMaxPerTick: Double = freeBodyVMaxPerSecond / tickFrequency

  val engineAccMaxPerTick: Double = engineAccMaxPerSecond / tickFrequency

  val bombVMaxPerTick: Double = bombVMaxPerSecond / tickFrequency

  val bombReloadTicks: Int = (tickFrequency / bombsPerSecond).toInt

  val healthRegenPerTick: Double = healthRegenPerTick / tickFrequency

  def navigationPenalty(mass: Double): Double = {
    val exponent = math.max(mass - initialShipMass, 0) / navigationPenaltyDenominator
    capValue(math.pow(2, exponent), lowerCap = 0, upperCap = 1)
  }

  val navigationPenaltyDenominator: Double = -20 * averageInitialAsteroidMass

  def gForceDenominator(distance: Double): Double = math.pow(distance, gDistanceExp)

  def energyOfMass(mass: Double): Double = energyOfMassUnit * mass

}

object GameParameters {

  implicit val gameParametersPickler: Pickler[GameParameters] = generatePickler[GameParameters]

}
