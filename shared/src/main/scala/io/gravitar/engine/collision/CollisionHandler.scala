package io.gravitar.engine.collision

import io.gravitar.engine._
import io.gravitar.s2d._

class CollisionHandler(val gameParameters: GameParameters) {

  final private val auxVec = new MVec()

  private var aBody: MHostBody = null
  private var bBody: MHostBody = null

  private var penetration: Double = 0.0

  private var centersDistance: Double = 0.0

  private var aMassLoss: Double = 0
  private var bMassLoss: Double = 0

  private var aOverlapsB: Boolean = false

  final private val position = new MPoint()
  final private val normal   = new MVec()

  final private val aImpulse = new MVec()
  final private val bImpulse = new MVec()

  final private val aAnnihilationImpulse = new MVec()
  final private val bAnnihilationImpulse = new MVec()

  final private val aPositionFix = new MVec()
  final private val bPositionFix = new MVec()

  def handleCollision(aBody: MHostBody, bBody: MHostBody): Unit = {
    (aBody, bBody) match {
      case (aShip: MHostShip, bShip: MHostShip) =>
        calculateCollisionEffects(aBody, bBody)
        applyCollisionEffects()
      case (ship: MHostShip, asteroid: MHostAsteroid) =>
        calculateCollisionEffects(aBody, bBody, true)
        applyCollisionEffects()
      case (ship: MHostShip, bomb: MHostBomb) if ship.id == bomb.firedByBodyId =>
        if (!bomb.stillInTheCannon) {
          calculateCollisionEffects(aBody, bBody)
          applyCollisionVectorsEffects()
        } else {
          bomb.isCollidingWithMotherShip = true
        }
      case (ship: MHostShip, bomb: MHostBomb) if ship.id != bomb.firedByBodyId =>
        calculateCollisionEffects(aBody, bBody)
        if (ship.antimatter == bomb.antimatter) {
          calculateBombAnnihilationEffect()
        } else {
          calculateAnnihilationEffect()
        }
        applyCollisionEffects()
      case (ship: MHostShip, dust: MHostDust) if ship.antimatter == dust.antimatter =>
        setupBodyPair(aBody, bBody)
        if (aOverlapsB) aConsumesB()
      case (ship: MHostShip, dust: MHostDust) if ship.antimatter != dust.antimatter =>
        calculateCollisionEffects(aBody, bBody, true)
        applyCollisionEffects()
      case (aAsteroid: MHostAsteroid, bAsteroid: MHostAsteroid) =>
        calculateCollisionEffects(aBody, bBody, true)
        applyCollisionEffects()
      case (asteroid: MHostAsteroid, bomb: MHostBomb) =>
        calculateCollisionEffects(aBody, bBody)
        if (asteroid.antimatter == bomb.antimatter) {
          calculateBombAnnihilationEffect()
        } else {
          calculateAnnihilationEffect()
        }
        applyCollisionEffects()
      case (asteroid: MHostAsteroid, dust: MHostDust) if asteroid.antimatter == dust.antimatter =>
        setupBodyPair(aBody, bBody)
        if (aOverlapsB) {
          aConsumesB()
        }
      case (asteroid: MHostAsteroid, dust: MHostDust) if asteroid.antimatter != dust.antimatter =>
        calculateCollisionEffects(aBody, bBody, true)
        applyCollisionEffects()
      case (aDust: MHostDust, bDust: MHostDust) if aDust.antimatter == bDust.antimatter =>
        calculateCollisionEffects(aDust, bDust)
        applyHardnessToDustCollision()
        applyCollisionEffects()
      case (aDust: MHostDust, bDust: MHostDust) if aDust.antimatter != bDust.antimatter =>
        calculateCollisionEffects(aBody, bBody, true)
        applyCollisionEffects()
      case (dust: MHostDust, bomb: MHostBomb) =>
        calculateCollisionEffects(aBody, bBody, true)
        applyCollisionEffects()
      case (aBomb: MHostBomb, bBomb: MHostBomb) if aBomb.firedByBodyId == bBomb.firedByBodyId =>
        calculateCollisionEffects(aBody, bBody)
        applyCollisionVectorsEffects()
      case (aBomb: MHostBomb, bBomb: MHostBomb) if aBomb.firedByBodyId != bBomb.firedByBodyId =>
        calculateCollisionEffects(aBody, bBody)
        calculateAnnihilationEffect()
        applyCollisionEffects()
      case _ => handleCollision(bBody, aBody)
    }
  }

  private def aConsumesB(): Unit = {
    calculateImpulses()
    aBody.velocity += aImpulse
    aBody.mass += bBody.mass
    if (aBody.isAsteroid) aBody.health += bBody.mass / aBody.mass
    bBody.isDestroyed = true
  }

  private def calculateCollisionEffects(
      aBody: MHostBody,
      bBody: MHostBody,
      withAnnihilation: Boolean = false
  ): Unit = {
    setupBodyPair(aBody, bBody)
    calculatePositionsFixes()
    calculateImpulses()
    if (withAnnihilation && aBody.antimatter != bBody.antimatter) calculateAnnihilationEffect()
  }

  private def applyHardnessToDustCollision(): Unit = {
    aImpulse *= gameParameters.dustCollisionHardness
    bImpulse *= gameParameters.dustCollisionHardness
    aPositionFix *= gameParameters.dustCollisionHardness
    bPositionFix *= gameParameters.dustCollisionHardness
  }

  private def applyCollisionEffects(): Unit = {
    applyCollisionVectorsEffects()

    aBody.mass -= aMassLoss
    bBody.mass -= bMassLoss

    aBody.health -= impulseToVMaxRatio(aImpulse)
    bBody.health -= impulseToVMaxRatio(bImpulse)
  }

  private def applyCollisionVectorsEffects(): Unit = {
    aBody.position += aPositionFix
    bBody.position += bPositionFix

    aBody.velocity += aImpulse
    bBody.velocity += bImpulse
  }

  private def impulseToVMaxRatio(impulse: MVec): Double =
    impulse.length / gameParameters.vLimitPerTick

  private def calculateAnnihilationEffect(): Unit = {
    val massLoss = annihilationMassLoss
    aMassLoss = massLoss
    bMassLoss = massLoss

    aAnnihilationImpulse.setTo(normal)
    aAnnihilationImpulse *= math.min(
      gameParameters.freeBodyVMaxPerTick,
      gameParameters.energyOfMass(massLoss) / guardFromZero(aBody.mass - massLoss)
    )

    bAnnihilationImpulse.setTo(-normal.x, -normal.y)
    bAnnihilationImpulse *= math.min(
      gameParameters.freeBodyVMaxPerTick,
      gameParameters.energyOfMass(massLoss) / guardFromZero(bBody.mass - massLoss)
    )

    aImpulse += aAnnihilationImpulse
    bImpulse += bAnnihilationImpulse
  }

  private def calculateBombAnnihilationEffect(): Unit = {
    aMassLoss = 0
    bMassLoss = annihilationMassLoss

    aAnnihilationImpulse.setTo(normal)
    aAnnihilationImpulse *= math.min(
      gameParameters.freeBodyVMaxPerTick,
      gameParameters.energyOfMass(bMassLoss / 2) / guardFromZero(aBody.mass)
    )

    bAnnihilationImpulse.setTo(-normal.x, -normal.y)
    bAnnihilationImpulse *= math.min(
      gameParameters.freeBodyVMaxPerTick,
      gameParameters.energyOfMass(bMassLoss / 2) / guardFromZero(bBody.mass - bMassLoss)
    )

    aImpulse += aAnnihilationImpulse
    bImpulse += bAnnihilationImpulse
  }

  private def annihilationMassLoss: Double = {
    if (aBody.mass < bBody.mass) {
      aBody.mass * impulseToVMaxRatio(aImpulse)
    } else {
      bBody.mass * impulseToVMaxRatio(bImpulse)
    }
  }

  private def setupBodyPair(aBody: MHostBody, bBody: MHostBody): Unit = {
    this.aBody = aBody
    this.bBody = bBody

    val radiusSum = aBody.radius + bBody.radius

    aMassLoss = 0
    bMassLoss = 0

    auxVec.setTo(aBody.position.x, aBody.position.y)
    auxVec -= (bBody.position.x, bBody.position.y)

    centersDistance = auxVec.length

    aOverlapsB = centersDistance < aBody.radius

    penetration = radiusSum - centersDistance

    normal.setTo(auxVec)
    normal.normalize()

    auxVec *= bBody.radius / radiusSum
    auxVec += (bBody.position.x, bBody.position.y)

    position.setTo(auxVec.x, auxVec.y)
  }

  private def calculateImpulses(): Unit = {
    auxVec.setTo(aBody.velocity)
    auxVec -= bBody.velocity
    var j         = -(1 + gameParameters.collisionRestitution) * auxVec.dot(normal)
    val totalMass = 1.0 / aBody.mass + 1.0 / bBody.mass
    j /= totalMass

    aImpulse.setTo(normal)
    aImpulse *= j
    aImpulse /= aBody.mass

    bImpulse.setTo(normal)
    bImpulse *= -j
    bImpulse /= bBody.mass
  }

  private def calculatePositionsFixes(): Unit = {
    val totalMass = aBody.mass + bBody.mass
    val r1        = aBody.mass / totalMass
    val r2        = bBody.mass / totalMass

    aPositionFix.setTo(normal)
    aPositionFix *= (penetration * r1 * 0.2)

    bPositionFix.setTo(normal)
    bPositionFix *= (-penetration * r2 * 0.2)
  }

}
