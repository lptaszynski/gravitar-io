package io.gravitar.engine.collision

import scala.collection.mutable

case class BatchCollisions(@specialized collisions: Vector[(Double, Double)])

object BatchCollisions {

  val empty: BatchCollisions = new BatchCollisions(Vector())

  val mergingSet: mutable.SortedSet[(Double, Double)] = mutable.SortedSet()

  def merged(batchCollisions: BatchCollisions*): BatchCollisions = {
    mergingSet.clear()

    batchCollisions.foreach { batch =>
      mergingSet ++= batch.collisions
    }

    BatchCollisions(mergingSet.toVector)
  }
}
