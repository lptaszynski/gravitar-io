package io.gravitar.engine.collision

import io.gravitar.utils.splitTrees
import boopickle.Default._
import boopickle.Pickler

case class QuadsPerLevelPartition(value: List[Set[Int]]) {
  def inPartition(n: Int): Boolean = value.headOption.forall(_.contains(n))
}

object QuadsPerLevelPartition {

  implicit val quadsPerLevelPartitionPickler: Pickler[QuadsPerLevelPartition] =
    generatePickler[QuadsPerLevelPartition]

  val allInSingle: QuadsPerLevelPartition = divisions(List(1)).head

  def apply(): QuadsPerLevelPartition = allInSingle

  def divisions(ns: List[Int] = List(1)): Vector[QuadsPerLevelPartition] = {
    assert(ns.nonEmpty)
    ns.foreach(n => assert(n == 1 || n == 2 || n == 4))

    val grouped = ns
      .map(
        n =>
          (0 until 4)
            .map(q => q -> q % n)
            .groupBy(_._2)
            .values
            .toList
            .map(_.map(_._1).toSet)
      )

    splitTrees(grouped).toVector.map(QuadsPerLevelPartition(_))
  }
}
