package io.gravitar.engine.collision

import io.gravitar.engine.{MBody, GameParameters}
import io.gravitar.s2d._

import scala.collection.mutable

class CollisionDetectionTree(
    val quad: Quad,
    val gameParameters: GameParameters,
    @specialized val quadsOfInterest: List[Set[Int]],
    @specialized val minTreeLevels: Int, // to avoid duplication due to parallel computation
    @specialized val level: Int,
    val nodeToCheck: Boolean
) {

  import CollisionDetectionTree._

  def this(gameParameters: GameParameters, quadsOfInterestPerLevel: QuadsPerLevelPartition) = {
    this(
      Quad(gameParameters.center, gameParameters.size),
      gameParameters,
      quadsOfInterestPerLevel.value ++
        List.fill(CollisionDetectionTree.MaxTreeLevels - quadsOfInterestPerLevel.value.length)(
          CollisionDetectionTree.AllChildren
        ),
      quadsOfInterestPerLevel.value.length,
      0,
      true
    )
  }

  /*
  When children list is empty it's a leaf.
  When not empty, sub-quads are indexed in the following way:
  0 | 1
  -----
  2 | 3
   */
  private val children: mutable.ArrayBuffer[CollisionDetectionTree] = mutable.ArrayBuffer.empty

  private val bodies: mutable.ArrayBuffer[MBody] = mutable.ArrayBuffer.empty

  def insert(body: MBody): Unit = {
    if (needsSubdivision) {
      subdivide()
    }
    if (isLeaf) {
      bodies += body
    } else {
      val node = childBodyFits(body)
      if (node.bodyFitsEntirely(body)) {
        if (node.nodeToCheck) node.insert(body)
      } else {
        bodies += body
      }
    }
  }

  def detectCollisions: BatchCollisions = {
    val collisionAggregator                       = mutable.ArrayBuffer.empty[(Double, Double)]
    val ancestorsQueue: mutable.ArrayStack[MBody] = mutable.ArrayStack()
    detectCollisionsRec(ancestorsQueue, collisionAggregator)
    BatchCollisions(collisionAggregator.toVector)
  }

  private def detectCollisionsRec(
      ancestorsBodies: mutable.ArrayStack[MBody],
      collisionAggregator: mutable.ArrayBuffer[(Double, Double)]
  ): Unit = {
    for {
      firstBody  <- bodies
      secondBody <- ancestorsBodies
      if areColliding(firstBody, secondBody)
    } {
      collisionAggregator += firstBody.id -> secondBody.id
    }

    for {
      firstBody  <- bodies
      secondBody <- bodies
      if firstBody.id < secondBody.id // We want to generate only one collision
      if areColliding(firstBody, secondBody)
    } {
      collisionAggregator += firstBody.id -> secondBody.id
    }

    if (!isLeaf) {
      bodies.foreach(ancestorsBodies.push)

      children.foreach { node =>
        if (node.nodeToCheck) node.detectCollisionsRec(ancestorsBodies, collisionAggregator)
      }
      bodies.foreach(_ => ancestorsBodies.pop())
    }
  }

  private def areColliding(firstBody: MBody, secondBody: MBody): Boolean =
    circlesColliding(firstBody, secondBody)

  private def aabbColliding(firstBody: MBody, secondBody: MBody): Boolean =
    firstBody.position.x + firstBody.radius + secondBody.radius > secondBody.position.x &&
      firstBody.position.x < secondBody.position.x + firstBody.radius + secondBody.radius &&
      firstBody.position.y + firstBody.radius + secondBody.radius > secondBody.position.y &&
      firstBody.position.y < secondBody.position.y + firstBody.radius + secondBody.radius

  private def circlesColliding(firstBody: MBody, secondBody: MBody): Boolean = {
    val radii = firstBody.radius + secondBody.radius
    firstBody.position.squareDistanceTo(secondBody.position) < radii * radii
  }

  private def childBodyFits(body: MBody): CollisionDetectionTree = {
    val childIndex = if (body.position.x >= quad.posX) {
      if (body.position.y >= quad.posY) 1 else 3
    } else {
      if (body.position.y >= quad.posY) 0 else 2
    }
    children(childIndex)
  }

  private def bodyFitsEntirely(body: MBody): Boolean =
    (body.position.x - body.radius >= quad.xLeft) &&
      (body.position.x + body.radius <= quad.xRight) &&
      (body.position.y + body.radius <= quad.yTop) &&
      (body.position.y - body.radius >= quad.yBottom)

  private def isLeaf: Boolean = children.isEmpty

  private def needsSubdivision: Boolean =
    isLeaf &&
      ((bodies.length >= MaxBodiesInLeaf && level < CollisionDetectionTree.MaxTreeLevels) ||
        level < minTreeLevels)

  private def subdivide(): Unit = {
    val nextLevel = level + 1

    val quadsForThisLevel  = quadsOfInterest.head
    val quadsForNextLevels = quadsOfInterest.tail

    children += new CollisionDetectionTree(
      quad.leftTopQuad,
      gameParameters,
      quadsForNextLevels,
      minTreeLevels,
      nextLevel,
      quadsForThisLevel.contains(0)
    )
    children += new CollisionDetectionTree(
      quad.rightTopQuad,
      gameParameters,
      quadsForNextLevels,
      minTreeLevels,
      nextLevel,
      quadsForThisLevel.contains(1)
    )
    children += new CollisionDetectionTree(
      quad.leftBottomQuad,
      gameParameters,
      quadsForNextLevels,
      minTreeLevels,
      nextLevel,
      quadsForThisLevel.contains(2)
    )
    children += new CollisionDetectionTree(
      quad.rightBottomQuad,
      gameParameters,
      quadsForNextLevels,
      minTreeLevels,
      nextLevel,
      quadsForThisLevel.contains(3)
    )

    val currentNodeBodies = bodies.toList
    bodies.clear()
    currentNodeBodies.foreach(insert)
  }

  override def toString: String = {
    s"Tree(level=$level check:$nodeToCheck bodiesCount=${bodies.length} quadsOfInterest=$quadsOfInterest)\n" +
      s" ${children.map(_.toString).mkString}"
  }
}

object CollisionDetectionTree {
  val AllChildren: Set[Int] = Set(0, 1, 2, 3)
  val MaxTreeLevels: Int    = 6
  val MaxBodiesInLeaf: Int  = 4 // Only when MaxTreeLevel is not reached
}
