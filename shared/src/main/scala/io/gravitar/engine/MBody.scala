package io.gravitar.engine

import io.gravitar.s2d._

abstract class MBody(
    @specialized protected var _mass: Double,
    @specialized protected var _health: Double,
    @specialized protected var _gCoefficient: Double,
    var _antimatter: Boolean
) extends Body {

  var radius: Double = calculateRadius

  override def position: MPoint
  def setPositionTo(x: Double, y: Double): Unit = {
    position.setTo(x, y)
  }

  override def mass: Double = _mass
  def mass_=(value: Double): Unit = {
    if (_mass != value) {
      _mass = value
      radius = calculateRadius
    }
  }

  def health: Double = _health
  def health_=(value: Double): Unit = {
    if (value != _health) {
      _health = value
    }
  }

  def gCoefficient: Double = _gCoefficient
  def gCoefficient_=(value: Double): Unit = {
    _gCoefficient = value
  }

  def antimatter: Boolean = _antimatter
  def antimatter_=(value: Boolean): Unit = {
    _antimatter = value
  }

  def as[C <: MBody]: C = asInstanceOf[C]

}
