package io.gravitar.engine

import io.gravitar.s2d._

abstract class MClientBody(
    @specialized initialMass: Double,
    @specialized initialHealth: Double,
    @specialized initialGCoefficient: Double,
    initialAntimatter: Boolean
) extends MBody(initialMass, initialHealth, initialGCoefficient, initialAntimatter) {

  def gameEngine: ClientGameEngine

  def velocity: MVec

  override def setPositionTo(x: Double, y: Double): Unit = {
    prevPosition.setTo(position)
    position.setTo(x, y)
    updateSmoothedVectors()
  }

  override def gCoefficient_=(value: Double): Unit = {
    if (math.abs(visibleGCoefficient - value) > MClientBody.predictionGCoefficientTolerance)
      visibleGCoefficient = value
    prevGCoefficient = gCoefficient
    predictedGCoefficient = capValue(
      value + (value - prevGCoefficient),
      gameEngine.gameParameters.minGCoefficient,
      gameEngine.gameParameters.maxGCoefficient
    )
    deltaGCoefficient = predictedGCoefficient - visibleGCoefficient
    _gCoefficient = value
    lastGCoefficientUpdateAt = gameEngine.getTickN
  }

  val visiblePosition: MPoint                 = position.copy()
  protected var prevPosition: MPoint          = position.copy()
  protected val smoothedVelocity: MVec        = velocity.copy()
  protected val predictedVelocity: MVec       = velocity.copy()
  protected val predictedGoalPosition: MPoint = position + velocity

  var visibleGCoefficient: Double             = 1
  protected var prevGCoefficient: Double      = 1
  protected var predictedGCoefficient: Double = 1
  protected var deltaGCoefficient: Double     = 0
  protected var lastGCoefficientUpdateAt: Int = 0

  def tickVisible(dFraction: Double): Unit = {
    visiblePosition.x += dFraction * smoothedVelocity.x
    visiblePosition.y += dFraction * smoothedVelocity.y
    if (lastGCoefficientUpdateAt > gameEngine.getTickN - 2) {
      visibleGCoefficient += dFraction * deltaGCoefficient
    } else {
      visibleGCoefficient = _gCoefficient
      deltaGCoefficient = 0
    }
  }

  def updateSmoothedVectors(): Unit = {
    if (position.squareDistanceTo(visiblePosition.x, visiblePosition.y) > MClientBody.predictionSquareDistanceToleration) {
      visiblePosition.x = position.x
      visiblePosition.y = position.y
    }
    predictedVelocity.setTo(position.x - prevPosition.x, position.y - prevPosition.y)
    predictedGoalPosition.setTo(position.x + predictedVelocity.x, position.y + predictedVelocity.y)
    smoothedVelocity.setTo(
      predictedGoalPosition.x - visiblePosition.x,
      predictedGoalPosition.y - visiblePosition.y
    )
  }

}

class MClientAsteroid(
    override val gameEngine: ClientGameEngine,
    @specialized override val id: Double,
    @specialized initialMass: Double,
    @specialized initialHealth: Double,
    @specialized initialGCoefficient: Double,
    initialAntimatter: Boolean,
    override val position: MPoint,
    override val velocity: MVec,
    @specialized override val kind: Byte
) extends MClientBody(initialMass, initialHealth, initialGCoefficient, initialAntimatter)
    with Asteroid

class MClientShip(
    override val gameEngine: ClientGameEngine,
    @specialized override val id: Double,
    @specialized initialMass: Double,
    @specialized initialHealth: Double,
    @specialized initialGCoefficient: Double,
    initialAntimatter: Boolean,
    override val position: MPoint,
    override val velocity: MVec,
    override val controlledByPlayer: String
) extends MClientBody(initialMass, initialHealth, initialGCoefficient, initialAntimatter)
    with Ship

class MClientBomb(
    override val gameEngine: ClientGameEngine,
    @specialized override val id: Double,
    @specialized initialMass: Double,
    @specialized initialHealth: Double,
    @specialized initialGCoefficient: Double,
    initialAntimatter: Boolean,
    override val position: MPoint,
    override val velocity: MVec,
    override val firedByBodyId: Double
) extends MClientBody(initialMass, initialHealth, initialGCoefficient, initialAntimatter)
    with Bomb

class MClientDust(
    override val gameEngine: ClientGameEngine,
    @specialized override val id: Double,
    @specialized initialMass: Double,
    @specialized initialHealth: Double,
    @specialized initialGCoefficient: Double,
    initialAntimatter: Boolean,
    override val position: MPoint,
    override val velocity: MVec,
    override val antimatter: Boolean
) extends MClientBody(initialMass, initialHealth, initialGCoefficient, initialAntimatter)
    with Dust

object MClientBody {

  val predictionDistanceToleration: Double = 20
  val predictionSquareDistanceToleration
      : Double = predictionDistanceToleration * predictionDistanceToleration

  val predictionGCoefficientTolerance: Double = 0.5

  def apply(imClientBody: ImClientBody)(implicit gameEngine: ClientGameEngine): MClientBody =
    imClientBody match {
      case imAsteroid: ImClientAsteroid =>
        new MClientAsteroid(
          gameEngine,
          imAsteroid.id,
          imAsteroid.mass,
          imAsteroid.health,
          imAsteroid.gCoefficient,
          imAsteroid.antimatter,
          imAsteroid.position.toMutable,
          imAsteroid.velocity.toMutable,
          imAsteroid.kind
        )

      case imShip: ImClientShip =>
        new MClientShip(
          gameEngine,
          imShip.id,
          imShip.mass,
          imShip.health,
          imShip.gCoefficient,
          imShip.antimatter,
          imShip.position.toMutable,
          imShip.velocity.toMutable,
          imShip.controlledByPlayer
        )

      case imBomb: ImClientBomb =>
        new MClientBomb(
          gameEngine,
          imBomb.id,
          imBomb.mass,
          imBomb.health,
          imBomb.gCoefficient,
          imBomb.antimatter,
          imBomb.position.toMutable,
          imBomb.velocity.toMutable,
          imBomb.firedByBodyId
        )

      case imDust: ImClientDust =>
        new MClientDust(
          gameEngine,
          imDust.id,
          imDust.mass,
          imDust.health,
          imDust.gCoefficient,
          imDust.antimatter,
          imDust.position.toMutable,
          imDust.velocity.toMutable,
          imDust.antimatter
        )
    }

}
