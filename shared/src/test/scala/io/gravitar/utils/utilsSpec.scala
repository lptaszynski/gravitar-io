package io.gravitar.utils

import org.scalatest._

class utilsSpec extends FlatSpec with Matchers {

  "ClientUtils" should "space integers properly" in {
    formatToSpacedInt(-100) shouldBe "-100"
    formatToSpacedInt(-1000) shouldBe "-1 000"
    formatToSpacedInt(0) shouldBe "0"
    formatToSpacedInt(10) shouldBe "10"
    formatToSpacedInt(100) shouldBe "100"
    formatToSpacedInt(1000) shouldBe "1 000"
    formatToSpacedInt(10000) shouldBe "10 000"
    formatToSpacedInt(100000) shouldBe "100 000"
    formatToSpacedInt(1000000) shouldBe "1 000 000"

  }

}
