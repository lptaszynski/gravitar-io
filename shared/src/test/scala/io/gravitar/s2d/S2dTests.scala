package io.gravitar.s2d

import org.scalatest._
import org.scalactic._

class S2dTests extends FlatSpec with Matchers {

  implicit val doubleEquality: Equality[Double] =
    TolerantNumerics.tolerantDoubleEquality(1E-10)

  "Vec and Point apply methods" should "have consistent behaviour" in {
    ImVec() shouldBe ImVec(0, 0)
    ImPoint() shouldBe ImPoint(0, 0)
    ImVec(1) shouldBe ImVec(1, 1)
    ImPoint(1) shouldBe ImPoint(1, 1)
  }

  "Vec and Point properties" should "be correct" in {
    ImVec(0, 0).length shouldBe 0D
    ImVec(-3, -4).length shouldBe 5D
  }

  "Point" should "have correct elementary operations" in {
    ImVec(0, 1) * 4 shouldBe ImVec(0, 4)
    ImVec(4, 16) / 4 shouldBe ImVec(1, 4)
    ImVec(1, 2) + ImVec(3, 5) shouldBe ImVec(4, 7)
    ImVec(1, 2) - ImVec(3, 5) shouldBe ImVec(-2, -3)
    (ImVec(3, -3) - ImVec(1, 0)) shouldBe ImVec(2, -3)
    (ImPoint(1, 2) imVecTo ImPoint(3, 5)) shouldBe ImVec(2, 3)
    ImPoint(-1, -1) + ImVec(2, 3) shouldBe ImPoint(1, 2)
    assert(ImVec(10, 1).rotated(5 * math.Pi) === ImVec(-10, -0.9999999999999939))
    assert(ImPoint(3, 3).isWithin(ImPoint(2, 2), ImPoint(4, 4)))
    assert(ImPoint(3, 3).isNotWithin(ImPoint(0, 0), ImPoint(2, 2)))
  }

  "Quad leafs" should "have valid positions and widths" in {
    val mid = ImPoint(-100, -100)
    val width = 50
    val quad = Quad(mid, width)
    val ltQuad = quad.leftTopQuad
    val rtQuad = quad.rightTopQuad
    val lbQuad = quad.leftBottomQuad
    val rbQuad = quad.rightBottomQuad

    ltQuad.width shouldBe width / 2.0
    rtQuad.width shouldBe width / 2.0
    lbQuad.width shouldBe width / 2.0
    rbQuad.width shouldBe width / 2.0

    ltQuad.posX shouldBe -112.5
    ltQuad.posY shouldBe -87.5
    ImPoint(ltQuad.posX, ltQuad.posY).distanceTo(rtQuad.posX, rtQuad.posY) shouldBe width / 2.0
    ImPoint(lbQuad.posX, lbQuad.posY).distanceTo(rbQuad.posX, rbQuad.posY) shouldBe width / 2.0
    ImPoint(ltQuad.posX, ltQuad.posY).distanceTo(lbQuad.posX, lbQuad.posY) shouldBe width / 2.0
    ImPoint(rbQuad.posX, rbQuad.posY).distanceTo(rtQuad.posX, rtQuad.posY) shouldBe width / 2.0
  }

  "splitValue" should "work according to arguments" in {
    splitValue(100, 200, 500) shouldBe Vector(100.0)
    splitValue(100, 0, 80) shouldBe Vector(50.0, 50.0)
    val randomSplit = splitValue(100, 10, 20)
    randomSplit.sum === 100.0
    randomSplit.foreach(value => assert(value >= 10))
    randomSplit.foreach(value => assert(value <= 20))
  }
}
