package io.gravitar.engine

import io.gravitar.engine.bhtree.{BatchGravityAccelerations, DivRemPartition}
import io.gravitar.engine.collision.{BatchCollisions, QuadsPerLevelPartition}
import io.gravitar.s2d._
import org.scalatest._

class EngineTests extends FlatSpec with Matchers {

  val fixtureRadius = 1000
  val fixtureVMax   = 15.0

  def randomBody(id: Int) = ImClientAsteroid(
    id,
    math.random().toFloat * 1000,
    1,
    false,
    ImPoint.randomInsideCircle(fixtureRadius),
    ImVec.randomVec(math.random() * fixtureVMax),
    0
  )

  val fixtureGameParameters = GameParameters(
    tickFrequency = 10,
    arenaRadius = fixtureRadius,
    sectorLevel = 0,
    g = 1.5,
    shipVMaxPerSecond = fixtureVMax,
    bhTreeTheta = 0.5,
    collisionRestitution = 0.9
  )

  val fixtureBodies = (0 to 100).map(randomBody).toList

  def mutableFixtureBodiesFor(implicit gameEngine: HostGameEngine): List[MHostAsteroid] =
    fixtureBodies.map { asteroid =>
      new MHostAsteroid(
        id = asteroid.id,
        initialMass = asteroid.mass,
        initialHealth = asteroid.health,
        initialAntimatter = asteroid.antimatter,
        position = asteroid.position.toMutable,
        velocity = asteroid.velocity.toMutable,
        kind = asteroid.kind
      )
    }

  def applyFixturesTo(implicit gameEngine: HostGameEngine): Unit = {
    gameEngine.setTick(0)
    gameEngine.setGameParameters(fixtureGameParameters)
    mutableFixtureBodiesFor.foreach(gameEngine.addBody)
  }

  "Two computed batch gravity accelerations" should "give same results" in {
    val engine1 = new HostGameEngine {}
    val engine2 = new HostGameEngine {}

    applyFixturesTo(engine1)
    engine1.createGravityTrees()
    val batchGravity1 = engine1.calculateBatchGravityAccelerations()

    applyFixturesTo(engine2)
    engine2.createGravityTrees()
    val batchGravity2 = engine2.calculateBatchGravityAccelerations()

    batchGravity1 shouldBe batchGravity2

    engine1.handleBatchBodiesAccelerations(batchGravity1)
    engine1.tick()

    engine2.handleBatchBodiesAccelerations(batchGravity2)
    engine2.tick()

    engine1.initialSectorDirectives(0).toSet shouldBe engine2.initialSectorDirectives(0).toSet
  }

  "Two computed batch collisions" should "give same results" in {
    val engine1 = new HostGameEngine {}
    val engine2 = new HostGameEngine {}

    applyFixturesTo(engine1)
    val batchCollisions1 = engine1.calculateBatchCollisions()

    applyFixturesTo(engine2)
    val batchCollisions2 = engine2.calculateBatchCollisions()

    batchCollisions1 shouldBe batchCollisions2

    engine1.handleBatchCollisions(batchCollisions1)

    engine2.handleBatchCollisions(batchCollisions2)

    engine1.initialSectorDirectives(0).toSet shouldBe engine2.initialSectorDirectives(0).toSet
  }

  "Computed batch collisions" should "be ordered and without duplicates" in {
    val engine = new HostGameEngine {}

    applyFixturesTo(engine)
    val batchCollisions = BatchCollisions.merged(engine.calculateBatchCollisions())

    val firstBodyIds = batchCollisions.collisions.zipWithIndex.filter(_._2 % 2 == 1)

    firstBodyIds shouldBe firstBodyIds.sorted

    val switchedCollisions = batchCollisions.collisions.map(c => c._2 -> c._1)

    switchedCollisions.intersect(batchCollisions.collisions) shouldBe empty

  }

  "Partitioned and single threaded batch gravity accelerations" should "be identical after merge" in {
    val engine = new HostGameEngine {}
    val partitions = DivRemPartition.partitions(4)

    applyFixturesTo(engine)

    engine.createGravityTrees()
    val partitionedResults = partitions.map(engine.calculateBatchGravityAccelerations)
    val mergedBatchGravity = BatchGravityAccelerations.merged(partitionedResults:_*)

    val singleBatchCollisions = BatchGravityAccelerations.merged(
      engine.calculateBatchGravityAccelerations())

    mergedBatchGravity.accelerations.length shouldBe singleBatchCollisions.accelerations.length

    val numberOfTriples = mergedBatchGravity.accelerations.length / 3
    def toTriples(accelerations: Vector[Double]): Vector[(Double, Double, Double)] = {
      (0 until numberOfTriples).toVector.map { n =>
        val idx = 3 * n
        (accelerations(idx), accelerations(idx + 1), accelerations(idx + 2))
      }
    }

    toTriples(mergedBatchGravity.accelerations).sorted shouldBe toTriples(singleBatchCollisions.accelerations).sorted
  }

  "Partitioned and single threaded batch collisions" should "be identical after merge" in {
    val engine = new HostGameEngine {}
    val partitions: Vector[QuadsPerLevelPartition] = QuadsPerLevelPartition.divisions(List(4))

    applyFixturesTo(engine)

    val partitionedResults = partitions.map(engine.calculateBatchCollisions)

    val mergedBatchCollisions = BatchCollisions.merged(partitionedResults:_*)

    val singleBatchCollisions = BatchCollisions.merged(
      engine.calculateBatchCollisions())

    mergedBatchCollisions shouldBe singleBatchCollisions
  }

}
