package io.gravitar.engine

import org.scalactic._
import org.scalatest._

class SectorMapperTests extends FlatSpec with Matchers {

  "Sector mapper" should "define correct amount of sectors" in {
    val gameParameters = GameParameters(arenaRadius = 1000, sectorLevel = 1)
    val sectorMapper = new SectorMapper(gameParameters)
    sectorMapper.minDim shouldBe -1
    sectorMapper.maxDim shouldBe 0

    sectorMapper.sectorToBodyIds.size shouldBe 4
  }

  "Sector mapper" should "map positions correctly to corresponding sectors - suite 1" in {
    val gameParameters = GameParameters(arenaRadius = 1000, sectorLevel = 1)
    val sectorMapper = new SectorMapper(gameParameters)

    sectorMapper.getSectorOf(-0.5, 0.5) shouldBe 0

    sectorMapper.getSectorOf(0, 0) shouldBe 1
    sectorMapper.getSectorOf(0.5, 0.5) shouldBe 1
    sectorMapper.getSectorOf(sectorMapper.sectorSize - 0.5, sectorMapper.sectorSize - 0.5) shouldBe 1
    sectorMapper.getSectorOf(sectorMapper.sectorSize + 0.5, sectorMapper.sectorSize + 0.5) shouldBe 1

    sectorMapper.getSectorOf(-0.5, -0.5) shouldBe 2
    sectorMapper.getSectorOf(-sectorMapper.sectorSize + 0.5, -sectorMapper.sectorSize + 0.5) shouldBe 2
    sectorMapper.getSectorOf(-sectorMapper.sectorSize - 0.5, -sectorMapper.sectorSize - 0.5) shouldBe 2

    sectorMapper.getSectorOf(0.5, -0.5) shouldBe 3

  }

  "Sector mapper" should "map positions correctly to corresponding sectors - suite 2" in {
    val gameParameters = GameParameters(arenaRadius = 1000, sectorLevel = 2)
    val sectorMapper = new SectorMapper(gameParameters)

    sectorMapper.sectorDimCoordinate(0.5) shouldBe 0
    sectorMapper.sectorDimCoordinate(-0.5) shouldBe -1
    sectorMapper.sectorDimCoordinate(-0.5 - sectorMapper.sectorSize) shouldBe -2
    sectorMapper.sectorDimCoordinate(0.5 + sectorMapper.sectorSize) shouldBe 1

    sectorMapper.sectorsRange.foreach { sector =>
      val xCoordinate = sectorMapper.getCoordinateXOfSectorIndex(sector)
      val yCoordinate = sectorMapper.getCoordinateYOfSectorIndex(sector)
      sectorMapper.getSectorIndex(xCoordinate, yCoordinate) shouldBe sector
    }
  }

  "Sector mapper" should "map sequence of quads position to index correctly" in {
    val g0 = new SectorMapper(GameParameters(sectorLevel = 0))
    g0.sectorIndex(List()) shouldBe 0
    g0.sectorPath(0) shouldBe List()

    val g1 = new SectorMapper(GameParameters(sectorLevel = 1))
    (0 until 3).foreach { n =>
      g1.sectorIndex(List(n)) shouldBe n
      g1.sectorPath(n) shouldBe List(n)
    }

    val g2 = new SectorMapper(GameParameters(sectorLevel = 2))
    g2.sectorIndex(List(0, 0)) shouldBe 0
    g2.sectorPath(0) shouldBe List(0, 0)

    g2.sectorIndex(List(1, 0)) shouldBe 2
    g2.sectorPath(2) shouldBe List(1, 0)

    g2.sectorIndex(List(1, 2)) shouldBe 6
    g2.sectorPath(6) shouldBe List(1, 2)

    g2.sectorIndex(List(3, 3)) shouldBe 15
    g2.sectorPath(15) shouldBe List(3, 3)

    val g3 = new SectorMapper(GameParameters(sectorLevel = 3))
    (0 until g3.sectorsInDim * g3.sectorsInDim).foreach { index =>
      val quadsPath = g3.sectorPath(index)
      g3.sectorIndex(quadsPath) shouldBe index
    }

  }

}
