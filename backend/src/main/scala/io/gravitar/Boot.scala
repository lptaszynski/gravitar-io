package io.gravitar

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer

import scala.util.{Failure, Success}

object Boot extends App {

  implicit val system = ActorSystem("frontend")

  import system.dispatcher

  implicit val materializer = ActorMaterializer()

  val config    = system.settings.config
  val interface = config.getString("app.interface")
  val portEnv   = Option(System.getenv("PORT")).getOrElse(System.getenv("HTTP_PLATFORM_PORT"))
  val port      = Option(portEnv).map(_.toInt).getOrElse(config.getInt("app.port"))

  val service = new Webservice

  val binding = Http().bindAndHandle(service.route, interface, port)
  binding.onComplete {
    case Success(binding) =>
      val localAddress = binding.localAddress
      println(s"Server is listening on ${localAddress.getHostName}:${localAddress.getPort}")
    case Failure(e) =>
      println(s"Binding failed with ${e.getMessage}")
      system.terminate()
  }

}
