package io.gravitar.wsprotocol

import akka.util.ByteString
import boopickle.Default._
import io.gravitar.wsprotocol.ClientToServerMessage._
import io.gravitar.wsprotocol.ServerToClientMessage._

object WSPicklers {

  def pickle(serverToClientMessage: ServerToClientMessage): ByteString =
    ByteString.fromByteBuffer(Pickle.intoBytes[ServerToClientMessage](serverToClientMessage))

  def unpickle(data: ByteString): ClientToServerMessage =
    Unpickle[ClientToServerMessage].fromBytes(data.asByteBuffer)

}
