package io.gravitar

import akka.actor._
import akka.stream.OverflowStrategy
import akka.stream.scaladsl._
import akka.util.ByteString
import io.gravitar.engine._
import io.gravitar.wsprotocol._

trait Game {

  def playerFlow(connectionId: Long): Flow[ClientToServerMessage, ByteString, Any]

  def injectMessage(message: ServerToClientMessage): Unit

  def tick: Unit

}

object Game {

  def create(implicit system: ActorSystem): Game = {

    val gameParameters = GameParametersLoader.getGameParameters

    val gameServerActor =
      system.actorOf(Props(new GameServerEngine(gameParameters)), name = "gameServer")

    // Wraps the gameServerActor in a sink. When the stream to this sink will be completed
    // it sends the `ClientLeft(connectionId)` message to the gameServerActor.
    // FIXME: here some rate-limiting should be applied to prevent single users flooding the server
    def gameInSink(connectionId: Long) =
      Sink.actorRef[GameServerEvent](gameServerActor, ClientLeft(connectionId))

    new Game {

      def playerFlow(connectionId: Long): Flow[ClientToServerMessage, ByteString, Any] = {
        val in =
          Flow[ClientToServerMessage]
            .map(ClientEvent(connectionId, _))
            .to(gameInSink(connectionId))

        // The counter-part which is a source that will create a target ActorRef per
        // materialization where the gameServerActor will send its messages to.
        // This source will only buffer one element and will fail if the client doesn't read
        // messages fast enough.
        val out =
          Source
            .actorRef[ByteString](1024, OverflowStrategy.fail)
            .mapMaterializedValue(gameServerActor ! ClientJoined(connectionId, _))

        Flow.fromSinkAndSource(in, out)
      }

      def injectMessage(message: ServerToClientMessage): Unit =
        gameServerActor ! message // non-streams interface

      def tick: Unit = gameServerActor ! GameServerTick

    }
  }

}
