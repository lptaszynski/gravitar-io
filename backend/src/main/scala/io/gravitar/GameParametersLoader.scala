package io.gravitar

import com.typesafe.config.ConfigFactory
import io.gravitar.engine.GameParameters

object GameParametersLoader {

  private def gameConfig = ConfigFactory.load("game")

  lazy val getGameParameters = GameParameters(
    tickFrequency = gameConfig.getInt("game.tickFrequency"),
    arenaRadius = gameConfig.getInt("game.arenaRadius"),
    sectorLevel = gameConfig.getInt("game.sectorLevel"),
    asteroidsOnArena = gameConfig.getInt("game.asteroidsOnArena"),
    gDistanceExp = gameConfig.getDouble("game.gDistanceExp"),
    g = gameConfig.getDouble("game.g"),
    gMeshAmplifier = gameConfig.getDouble("game.gMeshAmplifier"),
    bhTreeTheta = gameConfig.getDouble("game.bhTreeTheta"),
    outOfSightBHTreeTheta = gameConfig.getDouble("game.outOfSightBHTreeTheta"),
    spawnDistance = gameConfig.getDouble("game.spawnDistance"),
    initialShipMass = gameConfig.getDouble("game.initialShipMass"),
    minMass = gameConfig.getDouble("game.minMass"),
    minInitialAsteroidMass = gameConfig.getDouble("game.minInitialAsteroidMass"),
    averageInitialAsteroidMass = gameConfig.getDouble("game.averageInitialAsteroidMass"),
    collisionRestitution = gameConfig.getDouble("game.collisionRestitution"),
    dustCollisionHardness = gameConfig.getDouble("game.dustCollisionHardness"),
    minGCoefficient = gameConfig.getDouble("game.minGCoefficient"),
    maxGCoefficient = gameConfig.getDouble("game.maxGCoefficient"),
    gCoefficientPerSecond = gameConfig.getDouble("game.gCoefficientPerSecond"),
    vLimitPerSecond = gameConfig.getDouble("game.vLimitPerSecond"),
    shipVMaxPerSecond = gameConfig.getDouble("game.shipVMaxPerSecond"),
    freeBodyVMaxPerSecond = gameConfig.getDouble("game.freeBodyVMaxPerSecond"),
    engineAccMaxPerSecond = gameConfig.getDouble("game.engineAccMaxPerSecond"),
    bombVMaxPerSecond = gameConfig.getDouble("game.bombVMaxPerSecond"),
    bombMassRatio = gameConfig.getDouble("game.bombMassRatio"),
    bombsPerSecond = gameConfig.getDouble("game.bombsPerSecond"),
    healthRegenPerSecond = gameConfig.getDouble("game.healthRegenPerSecond"),
    energyOfMassUnit = gameConfig.getDouble("game.energyOfMassUnit")
  )

}
