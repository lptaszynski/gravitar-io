package io.gravitar

import akka.actor.ActorRef
import io.gravitar.wsprotocol.ClientToServerMessage

sealed trait GameServerEvent
case class ClientJoined(connectionId: Long, subscriber: ActorRef)          extends GameServerEvent
case class ClientLeft(connectionId: Long)                                  extends GameServerEvent
case class ClientEvent(connectionId: Long, message: ClientToServerMessage) extends GameServerEvent
case object GameServerTick                                                 extends GameServerEvent
