package io.gravitar

import java.util.concurrent.atomic.AtomicLong

import akka.actor.ActorSystem
import akka.http.scaladsl.model.ws.{BinaryMessage, Message}
import akka.http.scaladsl.model.{HttpCharsets, HttpEntity, HttpResponse, MediaTypes}
import akka.http.scaladsl.server.Directives
import akka.stream.Materializer
import akka.stream.scaladsl.Flow
import akka.stream.stage._
import akka.util.ByteString
import io.gravitar.templates.MyStyles
import io.gravitar.wsprotocol._

import scala.concurrent.duration._
import scalacss.Defaults._

class Webservice(implicit fm: Materializer, system: ActorSystem) extends Directives {

  val theGame: Game = Game.create(system)

  val atomicConnectionId: AtomicLong = new AtomicLong()

  import system.dispatcher

  system.scheduler
    .scheduleOnce((1000 / 5).milliseconds) {
      theGame.tick
    }

  lazy val webjarsPrefix  = "lib"
  lazy val resourcePrefix = "resources"

  def mystyles = path("styles" / "mystyles.css") {
    complete {
      HttpResponse(
        entity =
          HttpEntity(MediaTypes.`text/css`.withCharset(HttpCharsets.`UTF-8`), MyStyles.render)
      )
    }
  }

  def loadResources = pathPrefix(resourcePrefix ~ Slash) {
    getFromResourceDirectory("")
  }

  def webjars = pathPrefix(webjarsPrefix ~ Slash) {
    getFromResourceDirectory(webjarsPrefix)
  }

  def route =
    get {
      pathSingleSlash {
        getFromResource("index.html")
      } ~
        // Scala-JS puts them in the root of the resource directory per default,
        // so that's where we pick them up
        path("play") {
          handleWebSocketMessages(
            websocketGameFlow(connectionId = atomicConnectionId.incrementAndGet())
          )
        }
    } ~
      webjars ~
      loadResources ~
      mystyles

  def websocketGameFlow(connectionId: Long): Flow[Message, Message, Any] =
    Flow[Message]
      .collect {
        case BinaryMessage.Strict(msg) =>
          WSPicklers.unpickle(msg) // unpack incoming WS text messages...
        // This will lose (ignore) messages not received in one chunk (which is
        // unlikely because chat messages are small) but absolutely possible
        // FIXME: We need to handle TextMessage.Streamed as well.
      }
      .via(theGame.playerFlow(connectionId)) // ... and route them through the player flow ...
      .map { msg: ByteString =>
        BinaryMessage.Strict(msg) // ... pack outgoing messages into WS JSON messages ...
      }
      .via(reportErrorsFlow) // ... then log any processing errors on stdin

  def reportErrorsFlow[T]: Flow[T, T, Any] =
    Flow[T]
      .transform(
        () =>
          new PushStage[T, T] {
            def onPush(elem: T, ctx: Context[T]): SyncDirective = ctx.push(elem)

            override def onUpstreamFailure(
                cause: Throwable,
                ctx: Context[T]
            ): TerminationDirective = {
              println(s"WS stream failed with $cause")
              super.onUpstreamFailure(cause, ctx)
            }
          }
      )
}
