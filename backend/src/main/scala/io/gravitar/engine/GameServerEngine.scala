package io.gravitar.engine

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.Locale

import akka.actor.{Actor, ActorRef, Status, Terminated}
import akka.util.ByteString
import io.gravitar._
import io.gravitar.engine.bhtree.{BatchGravityAccelerations, DivRemPartition}
import io.gravitar.engine.collision.{BatchCollisions, QuadsPerLevelPartition}
import io.gravitar.engine.effect.gravitymanipulation.MGravityManipulationEffect
import io.gravitar.engine.effect.shooting.MShootingEffect
import io.gravitar.pubsub.{SubscribeTo, SubscriberMessage, UnsubscribeFrom}
import io.gravitar.s2d.MVec
import io.gravitar.wsprotocol._

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.Random

class GameServerEngine(initialGameParameters: GameParameters) extends Actor with HostGameEngine {

  setGameParameters(initialGameParameters)

  val connectionIdToActorRef   = mutable.Map.empty[Long, ActorRef]
  val connectionIdToPlayerName = mutable.Map.empty[Long, String]

  var lastLeaderboard = Leaderboard()

  var lastTick: Long = System.currentTimeMillis()

  val gravityPartitions: Vector[DivRemPartition] = DivRemPartition.partitions(2)
  val collisionsDivisions: Vector[QuadsPerLevelPartition] =
    QuadsPerLevelPartition.divisions(List(2))

  val logs: mutable.ArrayBuffer[String] = new mutable.ArrayBuffer[String]()

  def log(message: String): Unit = {
    val timestamp = DateTimeFormatter
      .ofPattern("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
      .format(LocalDateTime.now())
    val event = s"$timestamp - $message"
    println(event)
    logs += event
  }

  def receive: Receive = {
    case ClientJoined(connectionId, subscriber) =>
      context.watch(subscriber)
      connectionIdToActorRef.put(connectionId, subscriber)
      val initialState = initialGlobalDirectives
      sendTo(subscriber, PropagateHostDirectives(initialState))
      sendTo(subscriber, UpdateLeaderboard(lastLeaderboard))
    case ClientLeft(connectionId) =>
      connectionIdToActorRef.get(connectionId).foreach { ref =>
        playerNameOf(connectionId).foreach { playerName =>
          getPlayer(playerName).isDestroyed = true
          getPlayerShip(playerName).isDestroyed = true
          connectionIdToPlayerName -= connectionId
        }
        // report downstream of completion, otherwise, there's a risk of leaking the
        // downstream when the TCP connection is only half-closed
        ref ! Status.Success(Unit)
        connectionIdToActorRef -= connectionId
        unsubscribeFromAllChannels(connectionId)
      }
    case msg: ClientEvent                       => handleClientEvent(msg)
    case injectedMessage: ServerToClientMessage => dispatch(injectedMessage)
    case GameServerTick =>
      context.system.scheduler.scheduleOnce((gameParameters.tickMillis - 25).milliseconds) {
        self ! GameServerTick
      }
      val now = System.currentTimeMillis()

      lastTick = now

      val batchGravityAccelerationsFuture = Future {
        createGravityTrees()
        markSectorsInBHTrees()
      }.flatMap { _ =>
          Future.sequence(
            gravityPartitions
              .map(partition => Future { calculateBatchGravityAccelerations(partition) })
          )
        }
        .map(results => BatchGravityAccelerations.merged(results: _*))

      val batchCollisionsFuture = Future
        .sequence(
          collisionsDivisions.map(partition => Future { calculateBatchCollisions(partition) })
        )
        .map(results => BatchCollisions.merged(results: _*))

      val zippedFuture = batchGravityAccelerationsFuture.zip(batchCollisionsFuture)

      Await.result(
        zippedFuture.map {
          case (batchGravityAccelerations, batchCollisions) =>
            handleBatchBodiesAccelerations(batchGravityAccelerations)
            handleBatchCollisions(batchCollisions)

            preTick()

            tick()

            balance()

            val globalDirectives = globalDirectivesForThisTick

            val sectorDirectives = sectorsDirectivesForThisTick

            connectionIdToActorRef.keys.foreach { connectionId =>
              val connectionDirectives = Vector(
                globalDirectives,
                subscriptionsOf(connectionId).toVector.sorted.flatMap(sectorDirectives),
                connectionDirectivesForThisTick(connectionId)
              ).flatten
              sendTo(connectionId, PropagateHostDirectives(connectionDirectives))
            }

            sendTo(connectionIdToActorRef.keys.toSet, PropagateHostDirectives(Vector(EndOfFrame)))

            eventuallyDispatchLeaderboard()

            eventuallyDispatchSummary()

            postTick()

        },
        atMost = 1.second
      )

    case Terminated(sub) =>
      // clean up dead subscribers, but should have been removed when `ParticipantLeft`
      connectionIdToActorRef.find(_._2 == sub).foreach {
        case (connectionId, _) =>
          connectionIdToPlayerName.get(connectionId).foreach { playerName =>
            getPlayer(playerName).isDestroyed = true
            getPlayerShip(playerName).isDestroyed = true
          }
          connectionIdToActorRef -= connectionId
          unsubscribeFromAllChannels(connectionId)
      }
  }

  def eventuallyDispatchLeaderboard(): Unit = {
    val newLeaderboard = getCurrentLeaderboard
    if (lastLeaderboard != newLeaderboard) {
      lastLeaderboard = newLeaderboard
      dispatch(UpdateLeaderboard(getCurrentLeaderboard))
    }
  }

  def eventuallyDispatchSummary(): Unit = {
    dispatch(UpdateSummary(getCurrentSummary))
  }

  def handleClientEvent(clientEvent: ClientEvent): Unit = {
    val maybePlayer = playerNameOf(clientEvent.connectionId).map(getPlayer)
    maybePlayer match {
      case None =>
        clientEvent.message match {
          case JoinGame(requestedPlayerName) =>
            validatePlayerName(requestedPlayerName) match {
              case Right(receivedPlayerName) =>
                val ship = new MHostShip(
                  id = engine.nextId(),
                  initialMass = gameParameters.initialShipMass,
                  position = engine.generatePlayerSpawnPosition,
                  controlledByPlayer = receivedPlayerName
                )
                val player = new MPlayer(ship.controlledByPlayer, ship.id)
                connectionIdToPlayerName.put(clientEvent.connectionId, receivedPlayerName)
                log(s"Player joined: $receivedPlayerName")
                addPlayer(player)
                addBody(ship)
                sendTo(
                  clientEvent.connectionId,
                  JoinedGameAs(player.toImmutable, ship.toImClientType)
                )
              case Left(reason) =>
                log(s"Refused player name: $requestedPlayerName reason: $reason")
                sendTo(clientEvent.connectionId, CannotJoinGame(reason))
            }
          case Log(message) =>
            log(s"Connection ${clientEvent.connectionId} logs: $message")
          case GetLogs(events) =>
            sendLogs(clientEvent.connectionId, events)
          case SubscriberMessageEnvelope(subscriberMessage) =>
            handleSubscriberMessage(clientEvent.connectionId, subscriberMessage)
          case unexpectedMessage => ()
          //logUnusualActivity(s"Connection ${clientEvent.connectionId} without a name received unexpected message: $unexpectedMessage")
        }
      case Some(player) =>
        clientEvent.message match {
          case JoinGame(requestedPlayerName) =>
            logUnusualActivity(s"${player.name} tries to join the game as $requestedPlayerName")
          case PlayerDeclarationMessage(playerDeclaration) =>
            handlePlayerDeclaration(player, playerDeclaration)
          case Log(message) =>
            log(s"Player ${player.name} logs: $message")
          case GetLogs(events) =>
            sendLogs(clientEvent.connectionId, events)
          case SubscriberMessageEnvelope(subscriberMessage) =>
            handleSubscriberMessage(clientEvent.connectionId, subscriberMessage)
        }
    }
  }

  private def playerNameOf(connectionId: Long): Option[String] =
    connectionIdToPlayerName.get(connectionId)

  def dispatch(message: ServerToClientMessage): Unit = {
    val binaryMessage = WSPicklers.pickle(message)
    connectionIdToActorRef.foreach(t => sendBinaryTo(t._2, binaryMessage))
  }

  private def sendTo(connectionIds: Set[Long], message: ServerToClientMessage): Unit = {
    val binaryMessage = WSPicklers.pickle(message)
    connectionIds.map(connectionIdToActorRef).foreach(sendBinaryTo(_, binaryMessage))
  }

  private def sendTo(connectionId: Long, message: ServerToClientMessage): Unit = {
    connectionIdToActorRef
      .get(connectionId)
      .foreach(actorRef => sendTo(actorRef, message))
  }

  private def sendTo(actor: ActorRef, message: ServerToClientMessage): Unit = {
    val binaryMessage = WSPicklers.pickle(message)
    sendBinaryTo(actor, binaryMessage)
  }

  private def sendBinaryTo(actor: ActorRef, message: ByteString): Unit = {
    actor ! message
  }

  private def validatePlayerName(playerName: String): Either[String, String] = {
    if (connectionIdToPlayerName.values.toSeq.contains(playerName)) {
      Left(Text.playerNameIsTaken)
    } else {
      if (playerName.nonEmpty) {
        Right(playerName.take(24))
      } else {
        val players = connectionIdToPlayerName.values.toSet
        val generatedPlayerName = (1 to 100)
          .find(n => !players.contains(Text.unnamedShip(n)))
          .map(Text.unnamedShip)
          .getOrElse(Random.nextString(12))
        Right(generatedPlayerName)
      }
    }
  }

  private def logUnusualActivity(message: String): Unit = {
    log(s"UNUSUAL ACTIVITY: $message")
  }

  override def removePlayer(playerName: String): Unit = {
    super.removePlayer(playerName)
    log(s"Removing player: $playerName")
    connectionIdToPlayerName.find(_._2 == playerName).foreach {
      case (playerConnectionId, _) =>
        connectionIdToPlayerName -= playerConnectionId
        sendTo(playerConnectionId, YouHaveLost("You have lost!"))
    }
  }

  def handleSubscriberMessage(connectionId: Long, subscriberMessage: SubscriberMessage): Unit = {
    subscriberMessage match {
      case SubscribeTo(sector) =>
        if (sectorMapper.sectorExists(sector)) {
          subscribe(connectionId, sector)
          val directives = initialSectorDirectives(sector)
          sendTo(connectionId, PropagateHostDirectives(Vector(ConfirmSubscription(sector))))
          sendTo(connectionId, PropagateHostDirectives(directives))
        }
      case UnsubscribeFrom(sector) =>
        unsubscribe(connectionId, sector)
        sendTo(connectionId, PropagateHostDirectives(Vector(ConfirmUnsubscription(sector))))
    }
  }

  def sendLogs(connectionId: Long, events: Int): Unit = {
    val tailEvents = logs.takeRight(events).toVector
    sendTo(connectionId, Logs(tailEvents))
  }

}
