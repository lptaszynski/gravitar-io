package io.gravitar

object Text {
  val playerNameIsTaken   = "Sorry, chosen name is already taken!"
  val playerIsStillInGame = "Player is still in game."
  def unnamedShip(n: Int) = s"Unnamed ship #$n"
}
