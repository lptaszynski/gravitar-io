package io.gravitar.engine

import io.gravitar._
import io.gravitar.s2d._
import org.denigma.threejs._

import scala.scalajs.js.Dynamic.literal

trait VClientBody extends MClientBody {

  override def gameEngine: ViewGameEngine

  val sprite: Sprite = createSprite

  updateRadius()

  override def mass_=(value: Double): Unit = {
    super.mass_=(value)
    updateRadius()
  }

  def updateRadius(): Unit = sprite.scale.set(2 * radius, 2 * radius, 0)

  override def tickVisible(dFraction: Double): Unit = {
    super.tickVisible(dFraction)
    sprite.position.x = visiblePosition.x
    sprite.position.y = visiblePosition.y
  }

  protected def createSprite: Sprite = {
    val spriteMaterial = new SpriteMaterial(
      literal(map = texture, color = color, transparent = true, alphaTest = 0.5)
        .asInstanceOf[SpriteMaterialParameters]
    )
    val sprite = new Sprite(spriteMaterial)
    sprite.position.set(position.x, position.y, 0)
    sprite
  }

  def updateTexture(): Unit = {
    sprite.material.map = texture
    sprite.material.needsUpdate = true
  }

  protected def texture: Texture

  protected def color: Int = 0xffffff

  protected def antimatterColor: Int = if (antimatter) 0xffcccc else 0xcce6ff

}

class VClientAsteroid(
    override val gameEngine: ViewGameEngine,
    @specialized override val id: Double,
    @specialized initialMass: Double,
    @specialized initialHealth: Double,
    @specialized initialGCoefficient: Double,
    override val position: MPoint,
    override val velocity: MVec,
    @specialized override val kind: Byte,
    override val antimatter: Boolean
) extends MClientAsteroid(
      gameEngine,
      id,
      initialMass,
      initialHealth,
      initialGCoefficient,
      antimatter,
      position,
      velocity,
      kind
    )
    with VClientBody {

  private var currentLevelOfDestruction: Int = levelOfDestruction(initialHealth)

  override def health_=(value: Double): Unit = {
    super.health_=(value)
    if (levelOfDestruction(value) != currentLevelOfDestruction) {
      currentLevelOfDestruction = levelOfDestruction(value)
      updateTexture()
    }
  }

  def levelOfDestruction(health: Double): Int = {
    if (health >= 0.75) 0 else if (health >= 0.5) 1 else if (health >= 0.25) 2 else 3
  }

  def texture: Texture = gameEngine.gameAssets.asteroids(kind)(currentLevelOfDestruction)

  override def color: Int = antimatterColor

}

class VClientShip(
    override val gameEngine: ViewGameEngine,
    @specialized override val id: Double,
    @specialized initialMass: Double,
    @specialized initialHealth: Double,
    @specialized initialGCoefficient: Double,
    initialAntimatter: Boolean,
    override val position: MPoint,
    override val velocity: MVec,
    override val controlledByPlayer: String
) extends MClientShip(
      gameEngine,
      id,
      initialMass,
      initialHealth,
      initialGCoefficient,
      initialAntimatter,
      position,
      velocity,
      controlledByPlayer
    )
    with VClientBody {

  override def health_=(value: Double): Unit = {
    super.health_=(value)
    labels.redraw()
  }

  override def mass_=(value: Double): Unit = {
    super.mass_=(value)
    labels.redraw()
  }

  val labels: ShipLabels = new ShipLabels(sprite, this)

  override def texture: Texture = gameEngine.gameAssets.ship

}

class VClientBomb(
    override val gameEngine: ViewGameEngine,
    @specialized override val id: Double,
    @specialized initialMass: Double,
    @specialized initialHealth: Double,
    @specialized initialGCoefficient: Double,
    initialAntimatter: Boolean,
    override val position: MPoint,
    override val velocity: MVec,
    override val firedByBodyId: Double
) extends MClientBomb(
      gameEngine,
      id,
      initialMass,
      initialHealth,
      initialGCoefficient,
      initialAntimatter,
      position,
      velocity,
      firedByBodyId
    )
    with VClientBody {

  override def texture: Texture = gameEngine.mode match {
    case PlayerMode(player) if player.controlledShipId == firedByBodyId =>
      gameEngine.gameAssets.bombBlue
    case _ => gameEngine.gameAssets.bombRed
  }

  override def color: Int = antimatterColor

}

class VClientDust(
    override val gameEngine: ViewGameEngine,
    @specialized override val id: Double,
    @specialized initialMass: Double,
    @specialized initialHealth: Double,
    @specialized initialGCoefficient: Double,
    initialAntimatter: Boolean,
    override val position: MPoint,
    override val velocity: MVec,
    override val antimatter: Boolean
) extends MClientDust(
      gameEngine,
      id,
      initialMass,
      initialHealth,
      initialGCoefficient,
      initialAntimatter,
      position,
      velocity,
      antimatter
    )
    with VClientBody {

  override def texture: Texture = gameEngine.gameAssets.dust

  override def color: Int = antimatterColor
}

object VClientBody {

  def apply(imBody: ImClientBody)(implicit gameEngine: ViewGameEngine): VClientBody = {
    imBody match {
      case imAsteroid: ImClientAsteroid =>
        new VClientAsteroid(
          gameEngine,
          imAsteroid.id,
          imAsteroid.mass,
          imAsteroid.health,
          imAsteroid.gCoefficient,
          imAsteroid.position.toMutable,
          imAsteroid.velocity.toMutable,
          imAsteroid.kind,
          imAsteroid.antimatter
        )

      case imShip: ImClientShip =>
        new VClientShip(
          gameEngine,
          imShip.id,
          imShip.mass,
          imShip.health,
          imShip.gCoefficient,
          imShip.antimatter,
          imShip.position.toMutable,
          imShip.velocity.toMutable,
          imShip.controlledByPlayer
        )

      case imBomb: ImClientBomb =>
        new VClientBomb(
          gameEngine,
          imBomb.id,
          imBomb.mass,
          imBomb.health,
          imBomb.gCoefficient,
          imBomb.antimatter,
          imBomb.position.toMutable,
          imBomb.velocity.toMutable,
          imBomb.firedByBodyId
        )

      case imDust: ImClientDust =>
        new VClientDust(
          gameEngine,
          imDust.id,
          imDust.mass,
          imDust.health,
          imDust.gCoefficient,
          imDust.antimatter,
          imDust.position.toMutable,
          imDust.velocity.toMutable,
          imDust.antimatter
        )
    }
  }

}
