package io.gravitar.engine

import io.gravitar.s2d._
import io.gravitar.scene.CanvasSprite
import io.gravitar.utils._
import org.denigma.threejs._

class ShipLabels(val shipObject3D: Object3D, val ship: VClientShip) extends CanvasSprite(256, 256) {

  ctx.textAlign = "center"
  ctx.textBaseline = "middle"
  ctx.fillStyle = "white"
  ctx.strokeStyle = "black"

  sprite.position.set(0, 0, 1)
  sprite.scale.set(2, 2, 1)

  redraw()
  addLabelsToShip()

  def draw(): Unit = {
    ctx.beginPath()
    drawPlayerName()
    drawMass()
    if (ship.health < 1 - MinPositiveValue) drawHealthBar()
  }

  def addLabelsToShip(): Unit = shipObject3D.add(sprite)

  private def drawPlayerName(): Unit = {
    ctx.font = "16pt neuropolx"
    ctx.fillStyle = "white"
    ctx.fillText(ship.controlledByPlayer, canvas.width / 2, canvas.height / 8)
  }

  private def drawMass(): Unit = {
    ctx.font = "12pt neuropolx"
    ctx.fillStyle = "white"
    ctx.fillText(formatRoundedInt(ship.mass.toInt), canvas.width / 2, canvas.height / 8 + 22)
  }

  private def drawHealthBar(): Unit = {
    ctx.fillStyle = "green"
    ctx.fillRect(canvas.width * 0.25, canvas.height - 60, canvas.width * 0.5 * ship.health, 16)
    ctx.strokeStyle = "black"
    ctx.strokeRect(canvas.width * 0.25, canvas.height - 60, canvas.width * 0.5, 16)
  }

}
