package io.gravitar.engine

import io.gravitar.engine.bhtree.{BHTree, DivRemPartition, MassCenter}
import io.gravitar.s2d._

import scala.collection.mutable

class WorkerGameEngine extends ClientGameEngine with MeshConfiguration {

  type EMBody = MClientBody
  type EMShip = MClientShip

  override def imBodyToBodyType(imBody: ImClientBody): EMBody = MClientBody(imBody)

  protected val auxPoint                   = new MPoint()
  protected var lastMovementUpdate: Double = 0

  private val auxMeshBuffer: mutable.ArrayBuffer[Double] = mutable.ArrayBuffer()

  def computePartialGridUpdate(
      gridCenterPosition: Point,
      now: Double,
      partition: DivRemPartition
  ): Vector[Double] = {

    val dFraction = (now - lastMovementUpdate) / gameParameters.tickMillis
    lastMovementUpdate = now

    tickBodiesSmoothly(dFraction)

    val attractiveBHTree = new BHTree(gameParameters, sectorMapper)
    val repulsiveBHTree  = new BHTree(gameParameters, sectorMapper)

    outOfSightAttractiveMassCenters.foreach(attractiveBHTree.insert)
    outOfSightRepulsiveMassCenters.foreach(repulsiveBHTree.insert)

    val (attractiveMassCenters, repulsiveMassCenters) = {
      val (attractiveBodies, repulsiveBodies) =
        bodyIdToBody.values.partition(body => body.isAttractive)
      attractiveBodies.map(mBodyToMassCenter) -> repulsiveBodies.map(mBodyToMassCenter)
    }

    attractiveMassCenters.foreach(attractiveBHTree.insert)
    repulsiveMassCenters.foreach(repulsiveBHTree.insert)

    auxMeshBuffer.clear()

    for {
      index <- partition.range(numberOfGridPoints)
      xCoordinate = indexToXCoordinate(index)
      yCoordinate = indexToYCoordinate(index)
    } {

      auxVec1.toZero()
      auxVec2.toZero()

      auxPoint.setTo(
        gridCenterPosition.x + (pointsInMeshSegment * xCoordinate),
        gridCenterPosition.y + (pointsInMeshSegment * yCoordinate)
      )

      attractiveBHTree.applyNBodyForce(auxPoint, auxVec1)
      repulsiveBHTree.applyNBodyForce(auxPoint, auxVec2)
      applyOutOfEdgeForce(auxPoint, auxVec2)

      val p = if (auxVec2.lengthSquared > auxVec1.lengthSquared) 1 else -1
      auxVec2 -= auxVec1
      val z = p * gameParameters.gMeshAmplifier * auxVec2.length / gameParameters.tickMillis
      auxMeshBuffer += z
    }

    auxMeshBuffer.toVector
  }

  def mBodyToMassCenter(body: EMBody): MassCenter = {
    new MassCenter(
      body.visiblePosition,
      body.mass * math.abs(body.visibleGCoefficient),
      body.radius
    )
  }

}
