package io.gravitar.engine

import collection.mutable

//TODO move to options and should be broadcast by the main thread
trait MeshConfiguration {
  protected val pointsInMeshSegment: Int = 50

  protected val (xLeftSpan, xRightSpan): (Int, Int) = (26, 26)
  protected val (yBottomSpan, yTopSpan): (Int, Int) = (16, 16)
  protected val (xCenter, yCenter): (Int, Int)      = (xRightSpan - xLeftSpan, yTopSpan - yBottomSpan)
  protected val xRelativeCoordinates: Range         = -xLeftSpan to xRightSpan
  protected val yRelativeCoordinates: Range         = -yBottomSpan to yTopSpan
  protected val xSpan: Int                          = xRelativeCoordinates.length
  protected val ySpan: Int                          = yRelativeCoordinates.length

  protected val numberOfGridPoints: Int = xSpan * ySpan

  private val indexToXCoordinateBuffer = mutable.ArrayBuffer.fill(numberOfGridPoints)(0)
  private val indexToYCoordinateBuffer = mutable.ArrayBuffer.fill(numberOfGridPoints)(0)
  for (x <- xRelativeCoordinates; y <- yRelativeCoordinates) {
    val index = getGridPointIndex(x, y)
    indexToXCoordinateBuffer(index) = x
    indexToYCoordinateBuffer(index) = y
  }

  protected def indexToXCoordinate(index: Int): Int = indexToXCoordinateBuffer(index)
  protected def indexToYCoordinate(index: Int): Int = indexToYCoordinateBuffer(index)

  protected def getGridPointIndex(xCoordinate: Int, yCoordinate: Int): Int = {
    val j = xCoordinate + xLeftSpan
    val i = -yCoordinate + yTopSpan
    i * xSpan + j
  }

}
