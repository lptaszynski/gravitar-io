package io.gravitar.engine

import io.gravitar.engine.GameAssets.Ready
import io.gravitar.s2d._
import io.gravitar.scene.{GamingScene, HUDScene}
import io.gravitar.utils.Benchmark
import io.gravitar.utils.RichVectorsTranslations._
import io.gravitar.{BenchmarkLoggingConfig, Mode, PlayerMode, SpecMode}
import org.denigma.threejs._

import collection.immutable
import scala.scalajs.js.Dynamic.literal

class ViewGameEngine(val gamingScene: GamingScene, val hUDScene: HUDScene, var mode: Mode)
    extends ClientGameEngine
    with MeshConfiguration {

  implicit val gameEngine: ViewGameEngine = this

  val gameAssets: GameAssets[Ready.type] = gamingScene.gameAssets

  override type EMBody = VClientBody
  override type EMShip = VClientShip

  override def imBodyToBodyType(imBody: ImClientBody): VClientBody = VClientBody(imBody)

  val specPosition = new Vector3(0, 0, 0)

  var playerBodyPosition = new Vector3(0, 0, 0)

  val cameraLookingAt = new Vector3(0, 0, 0)

  val visibleRange = 1200

  private val cameraVector: Vector3 = new Vector3(0, -250, 800)

  private val meshUpdateBenchmark: Benchmark = new Benchmark(
    logging = BenchmarkLoggingConfig.Main.meshUpdate,
    message = "mesh update benchmark"
  )

  private var gravityMesh: Mesh         = new Mesh()
  private val gravityMeshCenter: MPoint = new MPoint(gravityMesh.position.x, gravityMesh.position.y)
  addGravityPotentialsMesh()

  def setModeTo(newMode: Mode): Unit = {
    mode = newMode
    refreshTextures()
  }

  def getCenterPoint: ImPoint =
    ImPoint(
      gamingScene.camera.position.x - cameraVector.x,
      gamingScene.camera.position.y - cameraVector.y
    )

  def getGridCenterPoint: Point = gravityMeshCenter

  def updateCamera(): Unit = {
    mode match {
      case SpecMode =>
        gamingScene.camera.lookAt(specPosition)
        gamingScene.camera.position.set(specPosition.x, specPosition.y, specPosition.z)
        gamingScene.camera.position.add(cameraVector)
        cameraLookingAt.set(specPosition.x, specPosition.y, specPosition.z)
      case PlayerMode(player) =>
        playerBodyPosition = getPlayerShipOpt(player.name)
          .map(_.sprite.position)
          .getOrElse(playerBodyPosition)
        gamingScene.camera.lookAt(playerBodyPosition)
        gamingScene.camera.position
          .set(playerBodyPosition.x, playerBodyPosition.y, playerBodyPosition.z)
        gamingScene.camera.position.add(cameraVector)
        cameraLookingAt.set(playerBodyPosition.x, playerBodyPosition.y, playerBodyPosition.z)
    }
  }

  def setNewGravityPotentials(vertexPotentials: immutable.Vector[Double]): Unit = {
    var index = 0
    vertexPotentials.foreach { potential =>
      gravityMesh.geometry.vertices(index).z = potential
      index += 1
    }
    moveGravityPotentialsGridIfNeeded()
    refreshPotentialsGrid()
  }

  def getSectorsUpdate: SectorsUpdate = {
    val sectorsWithinRange =
      sectorMapper.getSectorsWithinRangeOf(cameraLookingAt.x, cameraLookingAt.y, visibleRange)
    val sectorsToSubscribe   = sectorsWithinRange.diff(subscriptions)
    val sectorsToUnsubscribe = subscriptions.toSet.diff(sectorsWithinRange)
    SectorsUpdate(sectorsToSubscribe, sectorsToUnsubscribe)
  }

  private def moveGravityPotentialsGridIfNeeded(): Unit = {
    val xDiff      = (cameraLookingAt.x - gravityMesh.position.x).toInt / pointsInMeshSegment
    val yDiff      = (cameraLookingAt.y - gravityMesh.position.y).toInt / pointsInMeshSegment
    val vertices   = gravityMesh.geometry.vertices
    val potentials = vertices.map(_.z)
    if (xDiff != 0 || yDiff != 0) {
      for {
        x <- xRelativeCoordinates
        y <- yRelativeCoordinates
      } {
        val fromCoordinateX = x + xDiff
        val fromCoordinateY = y + yDiff
        if (fromCoordinateX <= xRightSpan && fromCoordinateX >= -xLeftSpan &&
            fromCoordinateY <= yTopSpan && fromCoordinateY >= -yBottomSpan) {
          val index     = getGridPointIndex(x, y)
          val fromIndex = getGridPointIndex(fromCoordinateX, fromCoordinateY)
          vertices(index).z = potentials(fromIndex)
        }
      }
      gravityMesh.position.x += xDiff * pointsInMeshSegment
      gravityMesh.position.y += yDiff * pointsInMeshSegment
      gravityMeshCenter.setTo(gravityMesh.position.x, gravityMesh.position.y)
    }
  }

  def refreshPotentialsGrid(): Unit = {
    gravityMesh.geometry.verticesNeedUpdate = true
  }

  override def setGameParameters(newGameParameters: GameParameters): Unit = {
    super.setGameParameters(newGameParameters)
  }

  override def setBodyPosition(bodyId: Double, newPosition: ImPoint): Unit = {
    super.setBodyPosition(bodyId, newPosition)
  }

  override def addBody(body: ImClientBody): Unit = {
    super.addBody(body)
    addExistingBodyToScene(body.id)
  }

  private def addExistingBodyToScene(bodyId: Double): Unit = {
    val vBody = getBody(bodyId)
    gamingScene.scene.add(vBody.sprite)
  }

  override def removeBody(bodyId: Double): Unit = {
    val maybeVBody = bodyIdToBody.get(bodyId)
    maybeVBody.foreach { vBody =>
      gamingScene.scene.remove(vBody.sprite)
    }
    super.removeBody(bodyId)
  }

  override def removePlayer(playerName: String): Unit = {
    super.removePlayer(playerName)
    mode match {
      case PlayerMode(thisPlayer) if thisPlayer.name == playerName =>
        specPosition.set(playerBodyPosition.x, playerBodyPosition.y, playerBodyPosition.z)
        setModeTo(SpecMode)
      case _ => ()
    }
  }

  private def addGravityPotentialsMesh(): Unit = {
    gamingScene.scene.remove(gravityMesh)
    val mesh = getGravityPotentialsMesh
    gravityMesh = mesh
    gamingScene.scene.add(mesh)
  }

  private def getGravityPotentialsMesh: Mesh = {
    val gravityPlaneGeometry =
      new PlaneGeometry(
        (xSpan - 1) * pointsInMeshSegment,
        (ySpan - 1) * pointsInMeshSegment,
        xSpan - 1,
        ySpan - 1
      )
    val gravityPlaneMaterial = new MeshBasicMaterial(
      literal(
        color = 0x00aaff,
        wireframe = true,
        transparent = true,
        depthWrite = false,
        depthTest = false
      ).asInstanceOf[MeshBasicMaterialParameters]
    )
    val gravityPotentials = new Mesh(gravityPlaneGeometry, gravityPlaneMaterial)
    gravityPotentials.position.set(0, 0, 1)
    gravityPotentials
  }

  private def refreshTextures(): Unit = bodyIdToBody.valuesIterator.foreach(_.updateTexture())

}
