package io.gravitar.engine

import io.gravitar.engine.GameAssets._
import org.denigma.threejs._
import org.scalajs.dom

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Promise}

class GameAssets[S <: State]() {

  private val loadingManager = new LoadingManager()
  private val textureLoader  = new TextureLoader(loadingManager)

  val readyPromise: Promise[Unit] = Promise()

  loadingManager.onLoad = () => {
    dom.window.setTimeout(() => readyPromise.success(), 200)
  }

  val numberOfAsteroid: Int = ImClientAsteroid.numberOfKinds
  val levelsOfDestruction   = 4

  var asteroids: mutable.ArrayBuffer[mutable.ArrayBuffer[Texture]] = mutable.ArrayBuffer.empty
  (0 until numberOfAsteroid).foreach { asteroid =>
    val asteroidLevels = mutable.ArrayBuffer.empty[Texture]
    asteroids += asteroidLevels
    (0 until levelsOfDestruction).foreach { level =>
      asteroidLevels += null
    }
  }

  (0 until numberOfAsteroid).foreach { asteroid =>
    (0 until levelsOfDestruction).foreach { level =>
      textureLoader.load(s"resources/textures/asteroid_${asteroid}_$level.png", { texture =>
        asteroids(asteroid).update(level, texture)
      })
    }
  }

  var background: Texture = null

  var ship: Texture = null

  var dust: Texture = null

  var bombRed: Texture = null

  var bombBlue: Texture = null

  textureLoader.load("resources/textures/background.jpg", { texture =>
    background = texture
  })

  textureLoader.load("resources/textures/ship.png", { texture =>
    ship = texture
  })

  textureLoader.load("resources/textures/dust.png", { texture =>
    dust = texture
  })

  textureLoader.load("resources/textures/bomb_red.png", { texture =>
    bombRed = texture
  })

  textureLoader.load("resources/textures/bomb_blue.png", { texture =>
    bombBlue = texture
  })

  val ready: Future[GameAssets[Ready.type]] =
    readyPromise.future.map(_ => this.asInstanceOf[GameAssets[Ready.type]])

}

object GameAssets {
  def apply(): GameAssets[Loading.type] = new GameAssets()

  sealed trait State

  case object Loading extends State

  case object Ready extends State

}
