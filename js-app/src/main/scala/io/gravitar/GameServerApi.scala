package io.gravitar

import io.gravitar.engine._
import io.gravitar.engine.effect.gravitymanipulation.{Attract, Repulse, StopGravityManipulation}
import io.gravitar.engine.effect.shooting.{StartShooting, StopShooting}
import io.gravitar.pubsub._
import io.gravitar.s2d.ImVec
import io.gravitar.wsprotocol._
import org.scalajs.dom.WebSocket

class GameServerApi(val webSocket: WebSocket) {

  def subscribeToSector(sector: Int): Unit     = sendSubscriberMessage(SubscribeTo(sector))
  def unsubscribeFromSector(sector: Int): Unit = sendSubscriberMessage(UnsubscribeFrom(sector))

  def updateEngineAcc(newEngineAcc: ImVec): Unit =
    sendDeclaration(DeclareShipEngineAcc(newEngineAcc))
  def updateRotation(newRotation: Double): Unit =
    sendDeclaration(DeclareShipRotation(newRotation.toFloat))

  def startShooting(): Unit = sendDeclaration(DeclareShootingEvent(StartShooting))
  def stopShooting(): Unit  = sendDeclaration(DeclareShootingEvent(StopShooting))

  def attract(): Unit = sendDeclaration(DeclareGravityManipulationEvent(Attract))
  def repulse(): Unit = sendDeclaration(DeclareGravityManipulationEvent(Repulse))

  def stopGravityManipulation(): Unit =
    sendDeclaration(DeclareGravityManipulationEvent(StopGravityManipulation))

  def log(message: String): Unit = send(Log(message))

  def sendDeclaration(playerDeclaration: PlayerDeclaration): Unit =
    send(PlayerDeclarationMessage(playerDeclaration))
  def sendSubscriberMessage(subscriberMessage: SubscriberMessage): Unit =
    send(SubscriberMessageEnvelope(subscriberMessage))

  def send(clientToServerMessage: ClientToServerMessage): Unit =
    webSocket.send(WSPicklers.pickle(clientToServerMessage))

}
