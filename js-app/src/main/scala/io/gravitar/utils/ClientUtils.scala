package io.gravitar.utils

import org.scalajs.dom

object ClientUtils {

  def getWebsocketUri: String = {
    val wsProtocol = if (dom.document.location.protocol == "https:") "wss" else "ws"
    s"$wsProtocol://${dom.document.location.host}/play"
  }

}
