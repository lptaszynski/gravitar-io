package io.gravitar.utils

import io.gravitar.s2d._
import org.denigma.threejs.Vector3

trait RichVector3Translations {

  implicit class RichVector3(val vector3: Vector3) {

    def toImVec: ImVec = ImVec(vector3.x, vector3.y)
    def toMVec: MVec   = new MVec(vector3.x, vector3.y)

    def toImPoint: ImPoint = ImPoint(vector3.x, vector3.y)
    def toMPoint: MPoint   = new MPoint(vector3.x, vector3.y)

  }

}

trait RichPointTranslations {

  implicit class RichPoint(val point: ImPoint) {
    def toVector3(z: Double = 0): Vector3 = new Vector3(point.x, point.y, z)
  }

}

trait RichVecTranslations {

  implicit class RichVec(val vec: ImVec) {
    def toVector3(z: Double = 0): Vector3 = new Vector3(vec.x, vec.y, z)
  }

}

object RichVectorsTranslations
    extends RichVector3Translations
    with RichPointTranslations
    with RichVecTranslations
