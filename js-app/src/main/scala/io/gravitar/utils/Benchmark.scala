package io.gravitar.utils

import scala.collection.mutable

class Benchmark(
    val n: Int = 20,
    private val initialAverage: Int = 0,
    var logging: Boolean = true,
    val message: String = "benchmark"
) {

  private var average: Int       = initialAverage
  val buffer: mutable.Queue[Int] = mutable.Queue.empty

  private var lastStartTimestamp: Int = 0

  def getAverage: Int = average

  def startTimer(): Unit = lastStartTimestamp = System.currentTimeMillis().toInt

  def stopTimer(): Unit = put(System.currentTimeMillis().toInt - lastStartTimestamp)

  def insert(diff: Int): Unit = {
    put(diff)
    if (logging) log(diff)
  }

  def benchmarkAndReturn[R](block: => R): R = {
    val t0     = System.nanoTime()
    val result = block
    val t1     = System.nanoTime()
    val diff   = ((t1 - t0) / 1000000).toInt
    put(diff)
    if (logging) log(diff)
    result
  }

  private def log(diff: Int): Unit =
    println(s"$message diff=$diff on average=$getAverage")

  private def put(diff: Int): Unit = {
    if (buffer.length < n) {
      buffer += diff
      average = buffer.sum / buffer.length
    } else {
      val toDrop = buffer.dequeue()
      buffer += diff
      average -= toDrop / n
      average += diff / n
    }
  }

  def clear(): Unit = {
    buffer.clear()
    average = initialAverage
  }
}
