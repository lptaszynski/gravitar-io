package io.gravitar.utils

import java.nio.ByteBuffer

import scala.scalajs.js.typedarray.TypedArrayBufferOps._
import scala.scalajs.js.typedarray.{ArrayBuffer, TypedArrayBuffer}

trait SerializationOps {

  implicit class RichByteBuffer(byteBuffer: ByteBuffer) {
    def toArrayBuffer: ArrayBuffer = {
      if (byteBuffer.hasTypedArray()) {
        // get relevant part of the underlying typed array
        byteBuffer.typedArray().subarray(byteBuffer.position, byteBuffer.limit).buffer
      } else {
        // fall back to copying the data
        val tempBuffer        = ByteBuffer.allocateDirect(byteBuffer.remaining)
        val origPosition: Int = byteBuffer.position
        tempBuffer.put(byteBuffer)
        byteBuffer.position(origPosition)
        tempBuffer.typedArray().buffer
      }
    }
  }

  implicit class RichArrayBuffer(arrayBuffer: ArrayBuffer) {
    def toByteBuffer: ByteBuffer =
      TypedArrayBuffer.wrap(arrayBuffer.asInstanceOf[ArrayBuffer])
  }

}

object SerializationOps extends SerializationOps
