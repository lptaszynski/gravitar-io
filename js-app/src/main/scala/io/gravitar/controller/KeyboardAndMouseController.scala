package io.gravitar.controller

import io.gravitar.GameServerApi
import io.gravitar.s2d._
import org.scalajs.dom
import org.scalajs.dom.ext.KeyCode
import org.scalajs.dom.raw.MouseEvent

import scala.collection.mutable

class KeyboardAndMouseController(override val gameServerApi: GameServerApi)
    extends Controller(gameServerApi) {

  private val playerEnginePower: MVec = new MVec()

  private val playerRotation: MVec = new MVec()

  private val keysDown: mutable.Set[Int] = mutable.Set()

  private val navigationalKeys: Set[Int] = Set(KeyCode.W, KeyCode.S, KeyCode.A, KeyCode.D)

  private val buttonsDown: mutable.Set[Int] = mutable.Set()

  override def setup(): Unit = {
    dom.window.oncontextmenu = (e: MouseEvent) => false

    dom.window.addEventListener("mousemove", setShipRotationByMouse, false)
    dom.window.addEventListener("mouseout", setEngineAccToNone, false)
    dom.window.addEventListener("mousedown", handleMouseDown, false)
    dom.window.addEventListener("mouseup", handleMouseUp, false)
    dom.window.addEventListener("keydown", handleKeyDown, false)
    dom.window.addEventListener("keyup", handleKeyUp, false)
  }

  private def updateEngineVector(): Unit = {
    playerEnginePower.setTo(0, 0)
    if (keysDown.contains(KeyCode.W)) playerEnginePower += (0, 1)
    if (keysDown.contains(KeyCode.S)) playerEnginePower += (0, -1)
    if (keysDown.contains(KeyCode.A)) playerEnginePower += (-1, 0)
    if (keysDown.contains(KeyCode.D)) playerEnginePower += (1, 0)
    if (playerEnginePower.lengthSquared > 0) playerEnginePower.normalize()
    gameServerApi.updateEngineAcc(playerEnginePower.toImmutable)
  }

  private def setShipRotationByMouse(mouseEvent: MouseEvent) = {
    val halfWidth  = dom.window.innerWidth / 2
    val halfHeight = dom.window.innerHeight / 2
    playerRotation.x = (mouseEvent.clientX - halfWidth) / (halfWidth / 2)
    playerRotation.y = -(mouseEvent.clientY - halfHeight) / (halfHeight / 2)
    gameServerApi.updateRotation(playerRotation.theta)
  }

  private def setEngineAccToNone(mouseEvent: MouseEvent) = {
    gameServerApi.updateEngineAcc(ImVec())
  }

  private def handleKeyDown(e: dom.KeyboardEvent): Unit = {
    e.keyCode match {
      case KeyCode.Space if !keysDown.contains(KeyCode.Space) =>
        gameServerApi.startShooting()
      case navigationalKey
          if navigationalKeys.contains(navigationalKey) && !keysDown.contains(navigationalKey) =>
        keysDown += e.keyCode
        updateEngineVector()
      case _ => ()
    }
    keysDown += e.keyCode
  }

  private def handleKeyUp(e: dom.KeyboardEvent): Unit = {
    e.keyCode match {
      case KeyCode.Space =>
        gameServerApi.stopShooting()
      case navigationalKey if navigationalKeys.contains(navigationalKey) =>
        keysDown -= e.keyCode
        updateEngineVector()
      case _ => ()
    }
    keysDown -= e.keyCode
  }

  private def handleMouseDown(e: dom.MouseEvent): Unit = {
    e.button match {
      case 0 if !buttonsDown.contains(0) =>
        gameServerApi.attract()
      case 2 if !buttonsDown.contains(2) =>
        gameServerApi.repulse()
      case _ => ()
    }
    buttonsDown += e.button
  }

  private def handleMouseUp(e: dom.MouseEvent): Unit = {
    e.button match {
      case 0 =>
        if (!buttonsDown.contains(2))
          gameServerApi.stopGravityManipulation()
      case 2 =>
        if (!keysDown.contains(0))
          gameServerApi.stopGravityManipulation()
      case _ => ()
    }
    buttonsDown -= e.button
  }

}
