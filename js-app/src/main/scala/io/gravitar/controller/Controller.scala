package io.gravitar.controller

import io.gravitar.GameServerApi

abstract class Controller(val gameServerApi: GameServerApi) {

  def setup(): Unit

}
