package io.gravitar

import io.gravitar.wsprotocol._
import org.scalajs.dom
import org.scalajs.dom.raw.{HTMLDivElement, HTMLElement, HTMLInputElement}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Promise

class NewGameHandler(gameServerApi: GameServerApi) {

  private val killedMessage: HTMLElement =
    dom.document.getElementById("killedMessage").asInstanceOf[HTMLElement]
  private val startMenuWrapper: HTMLDivElement =
    dom.document.getElementById("startMenuWrapper").asInstanceOf[HTMLDivElement]
  private val playerNameInput: HTMLInputElement =
    dom.document.getElementById("playerNameInput").asInstanceOf[HTMLInputElement]
  private val startButton: HTMLInputElement =
    dom.document.getElementById("startButton").asInstanceOf[HTMLInputElement]
  private val playerNameError: HTMLElement =
    dom.document.getElementById("playerNameError").asInstanceOf[HTMLElement]
  private val browserError: HTMLElement =
    dom.document.getElementById("browserError").asInstanceOf[HTMLElement]

  checkBrowserCompatibility()

  var responseFromServer: Promise[Either[String, String]] = Promise()

  var startPlayingAs: Promise[String] = Promise()

  var logsResponseFromServer: Promise[Vector[String]] = Promise()

  def showNewGameProposition(message: String = ""): Unit = {
    killedMessage.innerHTML = message
    startMenuWrapper.removeAttribute("hidden")
    startButton.focus()
    responseFromServer = Promise()
    startPlayingAs = Promise()
  }

  def askLogs(events: Int): Unit = {
    gameServerApi.send(GetLogs(events))
    logsResponseFromServer = Promise()
    logsResponseFromServer.future.foreach(_.foreach(println))
  }

  def tryToJoinGame(): Unit = {
    val typedPlayerName = playerNameInput.value
    gameServerApi.send(JoinGame(typedPlayerName))
    responseFromServer = Promise()
    responseFromServer.future.foreach {
      case Left(errorMessage)        => showPlayerNameError(errorMessage)
      case Right(receivedPlayerName) => playAs(receivedPlayerName)
    }
  }

  private def showPlayerNameError(error: String): Unit = {
    showNewGameProposition()
    playerNameError.innerHTML = error
  }

  private def checkBrowserCompatibility(): Unit = {
    val userAgent    = dom.window.navigator.userAgent.toLowerCase
    val isCompatible = userAgent.contains("webkit") || userAgent.contains("chrome")
    if (!isCompatible) {
      browserError.innerHTML =
        """WARNING: Game works best on <a href="https://www.google.com/chrome/">Chrome</a> browser."""
    }
  }

  private def playAs(playerName: String): Unit = {
    startPlayingAs.success(playerName)
    startMenuWrapper.setAttribute("hidden", "true")
    playerNameError.innerHTML = ""
  }

}
