package io.gravitar

import org.scalajs.dom.webworkers.DedicatedWorkerGlobalScope
import org.scalajs.dom.WindowTimers
import org.scalajs.dom.raw.Performance

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobalScope

@js.native
trait PerformanceMeasurer extends js.Object {
  def performance: Performance = js.native
}

@js.native
@JSGlobalScope
object WorkerGlobalScope extends js.Object {

  def self: DedicatedWorkerGlobalScope with WindowTimers with PerformanceMeasurer = js.native
}
