package io.gravitar.wsprotocol

import boopickle.Default._
import io.gravitar.utils.SerializationOps._
import io.gravitar.wsprotocol.ClientToServerMessage._
import io.gravitar.wsprotocol.ServerToClientMessage._

import scala.scalajs.js.typedarray.ArrayBuffer

object WSPicklers {

  def pickle(clientToServerMessage: ClientToServerMessage): ArrayBuffer =
    Pickle.intoBytes[ClientToServerMessage](clientToServerMessage).toArrayBuffer

  def unpickle(data: ArrayBuffer): ServerToClientMessage =
    Unpickle[ServerToClientMessage].fromBytes(data.toByteBuffer)

}
