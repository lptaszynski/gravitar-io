package io.gravitar.worker

import org.scalajs.dom.MessageEvent
import org.scalajs.dom.webworkers.Worker

import scala.concurrent.{Future, Promise}
import scala.scalajs.js.typedarray.ArrayBuffer

class SimpleWorkerProxy(val url: String, val n: Int) {

  private val worker: Worker = new Worker(url)

  private val workerReady: Promise[Unit] = Promise()
  val workerReadyFuture: Future[Unit]    = workerReady.future

  private var requestPromise: Promise[WorkerMessage] = Promise()

  worker.addEventListener("message", onMessage _)

  // API
  def request[Response <: WorkerMessage](request: WorkerMessage): Future[Response] = {
    send(request)
    requestPromise = Promise[WorkerMessage]()
    requestPromise.future.asInstanceOf[Future[Response]]
  }

  def request[Response <: WorkerMessage](serializedRequest: ArrayBuffer): Future[Response] = {
    send(serializedRequest)
    requestPromise = Promise[WorkerMessage]()
    requestPromise.future.asInstanceOf[Future[Response]]
  }

  def send(message: WorkerMessage): Unit = {
    val data = WorkerMessage.pickle(message)
    worker.postMessage(data)
  }

  def send(serialized: ArrayBuffer): Unit = {
    worker.postMessage(serialized)
  }

  private def onMessage(messageEvent: MessageEvent): Unit = {
    val data       = messageEvent.data.asInstanceOf[ArrayBuffer]
    val fromWorker = WorkerMessage.unpickle(data)
    fromWorker match {
      case Ready => workerReady.success()
      case response: PartialGridUpdate =>
        requestPromise.success(response)
      case message => throw new Exception(s"invalid message for server: $message")
    }
  }

  def terminate(): Unit = worker.terminate()

}
