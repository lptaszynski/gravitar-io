package io.gravitar.worker

import io.gravitar.WorkerGlobalScope
import org.scalajs.dom.MessageEvent

import scala.scalajs.js.annotation.JSExport
import scala.scalajs.js.typedarray.ArrayBuffer

trait SimpleWorkerThread {

  def handleMessage(message: WorkerMessage): Option[WorkerMessage]

  def sendMessage(message: WorkerMessage): Unit = {
    val data = WorkerMessage.pickle(message)
    WorkerGlobalScope.self.postMessage(data)
  }

  @JSExport
  def main(): Unit = {
    WorkerGlobalScope.self.addEventListener("message", onMessage _)
    val readyData = WorkerMessage.pickle(Ready)
    WorkerGlobalScope.self.postMessage(readyData)
  }

  private def onMessage(messageEvent: MessageEvent): Unit = {
    val data     = messageEvent.data.asInstanceOf[ArrayBuffer]
    val envelope = WorkerMessage.unpickle(data)
    envelope match {
      case workerMessage: WorkerMessage =>
        val maybeResponseFuture = handleMessage(workerMessage)
        maybeResponseFuture.foreach { response =>
          val responseData = WorkerMessage.pickle(response)
          WorkerGlobalScope.self.postMessage(responseData)
        }
      case message => throw new Exception(s"invalid message for worker: $message")

    }
  }
}
