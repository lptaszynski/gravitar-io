package io.gravitar.worker

import io.gravitar.engine.bhtree.DivRemPartition
import io.gravitar.s2d._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.scalajs.js.typedarray.ArrayBuffer

class GameEngineWorkersApi(val workersPool: WorkersPool) {

  private val currentGridCenter: MPoint = new MPoint()

  def broadcast(serializedMessage: ArrayBuffer): Unit =
    workersPool.broadcast(serializedMessage)

  def configureGridUpdates(now: Double, gridCenterPosition: Point): Unit = {
    currentGridCenter.setTo(gridCenterPosition)
    workersPool.workersPool.foreach { worker =>
      worker.send(ConfigurePartition(DivRemPartition(workersPool.n, worker.n)))
    }
  }

  def getGridUpdate(gridCenterPosition: Point, now: Double): Future[GridUpdate] = {
    workersPool
      .broadcastRequest[PartialGridUpdate](
        GetGridUpdate(gridCenterPosition.x, gridCenterPosition.y, now)
      )
      .map(results => GridUpdate.fromPartialMeshUpdates(results: _*))
  }
}
