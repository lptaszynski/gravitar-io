package io.gravitar.worker

import io.gravitar.engine._
import io.gravitar.engine.bhtree.DivRemPartition
import io.gravitar.s2d._
import io.gravitar.wsprotocol.{
  ServerToClientMessage,
  PropagateHostDirectives => ServerPropagateHostDirectives
}

import scala.scalajs.js.annotation.JSExportTopLevel

@JSExportTopLevel("WorkerGameEngineThread")
object WorkerGameEngineThread extends SimpleWorkerThread {

  val engine: WorkerGameEngine = new WorkerGameEngine()

  private val currentGridCenter: MPoint         = new MPoint()
  private var currentPartition: DivRemPartition = null

  override def handleMessage(message: WorkerMessage): Option[WorkerMessage] = {
    message match {
      case PropagateHostDirectives(encodedHostDirectives) =>
        val hostDirectives = ServerToClientMessage.serverToClientMessagePickler
          .unpickle(encodedHostDirectives)
          .asInstanceOf[ServerPropagateHostDirectives]
        engine.handleHostDirectives(hostDirectives.hostDirectives)
        None
      case ConfigurePartition(partition) =>
        currentPartition = partition
        None
      case GetGridUpdate(x, y, now) =>
        currentGridCenter.setTo(x, y)
        val result = engine.computePartialGridUpdate(currentGridCenter, now, currentPartition)
        Some(PartialGridUpdate(result))
      case invalidMessage =>
        throw new Exception(s"Invalid worker message $invalidMessage")
    }
  }

}
