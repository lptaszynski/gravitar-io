package io.gravitar.worker

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.scalajs.js.typedarray.ArrayBuffer

class WorkersPool(val url: String, val n: Int = 8) {

  val workersPool: Seq[SimpleWorkerProxy] =
    (0 until n).map(n => new SimpleWorkerProxy(url, n))

  val poolReady: Future[Unit] =
    Future
      .sequence(workersPool.map(_.workerReadyFuture))
      .map(_ => println(s"Pool of ${workersPool.length} worker(s) is ready"))

  def broadcast(serializedMessage: ArrayBuffer): Unit = {
    workersPool.foreach(_.send(serializedMessage))
  }

  def broadcastRequest[Response <: WorkerMessage](
      workerMessage: WorkerMessage
  ): Future[Seq[Response]] = {
    val serializedRequest = WorkerMessage.pickle(workerMessage)
    Future.sequence(workersPool.map(_.request(serializedRequest)))
  }

  def terminate(): Unit = workersPool.foreach(_.terminate())

}
