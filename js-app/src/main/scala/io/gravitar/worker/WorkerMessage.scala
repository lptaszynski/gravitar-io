package io.gravitar.worker

import java.nio.ByteBuffer

import boopickle.Default._
import boopickle.Pickler
import io.gravitar.engine._
import io.gravitar.utils.SerializationOps._
import io.gravitar.engine.HostDirective._
import io.gravitar.engine.bhtree.DivRemPartition
import io.gravitar.engine.bhtree.DivRemPartition._
import io.gravitar.s2d.ImPoint
import io.gravitar.s2d.ImPoint._

import scala.collection.mutable
import scala.scalajs.js.typedarray.ArrayBuffer

sealed trait WorkerMessage

// messages to a worker
case class PropagateHostDirectives(encodedHostDirectives: ByteBuffer) extends WorkerMessage

case class ConfigurePartition(meshIndicesPartition: DivRemPartition) extends WorkerMessage

case class GetGridUpdate(@specialized x: Double, @specialized y: Double, @specialized now: Double)
    extends WorkerMessage

// messages from a worker
case object Ready extends WorkerMessage

case class PartialGridUpdate(partialVertexPotentials: Vector[Double]) extends WorkerMessage

// grouped response
case class GridUpdate(vertexPotential: Vector[Double]) extends WorkerMessage

object WorkerMessage {

  implicit val workerMessagePickler: Pickler[WorkerMessage] = generatePickler[WorkerMessage]

  def pickle(workerMessage: WorkerMessage): ArrayBuffer = {
    Pickle.intoBytes[WorkerMessage](workerMessage).toArrayBuffer
  }

  def unpickle(arrayBuffer: ArrayBuffer): WorkerMessage = {
    Unpickle[WorkerMessage].fromBytes(arrayBuffer.toByteBuffer)
  }
}

object GridUpdate {

  def fromPartialMeshUpdates(computedMeshUpdates: PartialGridUpdate*): GridUpdate = {
    val div           = computedMeshUpdates.length
    var rem           = 0
    val indices       = computedMeshUpdates.map(_.partialVertexPotentials.size).sum
    val mergingBuffer = mutable.ArrayBuffer.fill(indices)(0.0)
    computedMeshUpdates.foreach { meshUpdate =>
      var idx = 0
      meshUpdate.partialVertexPotentials.foreach { value =>
        val translatedIndex = DivRemPartition.toFullIndexTranslation(div, rem, idx)
        mergingBuffer(translatedIndex) = value
        idx += 1
      }
      rem += 1
    }
    GridUpdate(mergingBuffer.toVector)
  }

}
