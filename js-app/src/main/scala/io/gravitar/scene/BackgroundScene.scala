package io.gravitar.scene

import io.gravitar.engine.GameAssets
import io.gravitar.engine.GameAssets.Ready
import org.denigma.threejs._

import scala.scalajs.js.Dynamic

class BackgroundScene(
    override var width: Double,
    override var height: Double,
    val gameAssets: GameAssets[Ready.type]
) extends SceneWrapper {

  override val scene: Scene = new Scene()

  override val camera: OrthographicCamera =
    new OrthographicCamera(width / -2, width / 2, height / 2, height / -2, 0.1, 1500)
  camera.position.z = 100

  scene.add(camera)

  private val backgroundMesh = createBackgroundMesh(gameAssets.background)
  scene.add(backgroundMesh)

  override def setSize(newWidth: Double, newHeight: Double): Unit = {
    super.setSize(newWidth, newHeight)
    camera.left = width / -2
    camera.right = width / 2
    camera.top = height / 2
    camera.bottom = height / -2
    camera.near = 0.1
    camera.far = 1500

    camera.updateProjectionMatrix()
  }

  private def createBackgroundMesh(texture: Texture): Mesh = {
    val mesh = new Mesh(
      new PlaneGeometry(2, 2, 0),
      new MeshBasicMaterial(
        Dynamic
          .literal(map = texture, depthWrite = false, depthTest = false)
          .asInstanceOf[MeshBasicMaterialParameters]
      )
    )
    mesh.scale.set(1000, 1000, 1)
    mesh
  }

}
