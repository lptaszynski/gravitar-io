package io.gravitar.scene

import io.gravitar.engine.{GameAssets, Leaderboard, Summary}
import io.gravitar.engine.GameAssets.Ready
import org.denigma.threejs._

class HUDScene(
    override var width: Double,
    override var height: Double,
    val gameAssets: GameAssets[Ready.type]
) extends SceneWrapper {

  private val offset: Int = 16

  override val scene: Scene = new Scene()

  override val camera: OrthographicCamera =
    new OrthographicCamera(-halfHeight, halfWidth, halfHeight, -halfHeight, 0.1, 1500)
  camera.position.z = 10

  scene.add(camera)

  val leaderBoardView: LeaderboardView = new LeaderboardView()
  setLeaderboardPosition()
  showLeaderboard()

  val summaryView: SummaryView = new SummaryView()
  setSummaryPosition()
  showSummary()

  override def setSize(newWidth: Double, newHeight: Double): Unit = {
    super.setSize(newWidth, newHeight)
    camera.left = -halfWidth
    camera.right = halfWidth
    camera.top = halfHeight
    camera.bottom = -halfHeight
    camera.near = 0.1
    camera.far = 1500

    camera.updateProjectionMatrix()
    setLeaderboardPosition()
  }

  def showLeaderboard(): Unit                           = scene.add(leaderBoardView.sprite)
  def hideLeaderboard(): Unit                           = scene.remove(leaderBoardView.sprite)
  def updateLeaderboard(leaderBoard: Leaderboard): Unit = leaderBoardView.update(leaderBoard)

  private def setLeaderboardPosition(): Unit = {
    val leaderBoardHalfWidth  = leaderBoardView.widthScaled / 2
    val leaderBoardHalfHeight = leaderBoardView.heightScaled / 2
    leaderBoardView.sprite.position.set(
      halfWidth - leaderBoardHalfWidth - offset,
      halfHeight - leaderBoardHalfHeight - offset,
      1
    )
  }

  def showSummary(): Unit                   = scene.add(summaryView.sprite)
  def hideSummary(): Unit                   = scene.remove(summaryView.sprite)
  def updateSummary(summary: Summary): Unit = summaryView.update(summary)

  private def setSummaryPosition(): Unit = {
    val summaryHalfWidth  = summaryView.widthScaled / 2
    val summaryHalfHeight = summaryView.heightScaled / 2
    summaryView.sprite.position.set(
      -(halfWidth - summaryHalfWidth - offset),
      -(halfHeight - summaryHalfHeight - offset),
      1
    )
  }

}
