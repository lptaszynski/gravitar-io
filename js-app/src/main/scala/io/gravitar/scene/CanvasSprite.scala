package io.gravitar.scene

import org.denigma.threejs.{Sprite, SpriteMaterial, SpriteMaterialParameters, Texture}
import org.scalajs.dom
import org.scalajs.dom.CanvasRenderingContext2D
import org.scalajs.dom.raw.HTMLCanvasElement

import scala.scalajs.js.Dynamic.literal

abstract class CanvasSprite(
    val width: Int,
    val height: Int,
    val widthScale: Double = 1,
    val heightScale: Double = 1
) {

  val widthScaled  = width * widthScale
  val heightScaled = height * heightScale

  protected val canvas: HTMLCanvasElement =
    dom.document.createElement("canvas").asInstanceOf[HTMLCanvasElement]
  canvas.width = width
  canvas.height = height

  protected val ctx              = canvas.getContext("2d").asInstanceOf[CanvasRenderingContext2D]
  protected val texture: Texture = new Texture(canvas)

  protected val material = new SpriteMaterial(
    literal(
      map = texture,
      color = 0xffffff,
      transparent = true,
      alphaTest = 0.01,
      depthWrite = true
    ).asInstanceOf[SpriteMaterialParameters]
  )

  val sprite = new Sprite(material)

  sprite.scale.set(widthScaled, heightScaled, 1)

  protected def draw(): Unit

  protected def needsUpdate(): Unit = texture.needsUpdate = true

  def redraw(): Unit = {
    clear()
    draw()
    needsUpdate()
  }

  protected def clear(): Unit = ctx.clearRect(0, 0, width, height)

  protected def fillStrokedText(text: String, x: Double, y: Double): Unit = {
    ctx.fillText(text, x, y)
    ctx.strokeText(text, x, y)
  }
}
