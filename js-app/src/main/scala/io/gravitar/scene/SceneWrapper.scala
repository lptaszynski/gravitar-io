package io.gravitar.scene

import org.denigma.threejs.{Camera, Renderer, Scene}

trait SceneWrapper {

  protected var width: Double

  protected var height: Double

  protected var halfWidth: Double = width / 2

  protected var halfHeight: Double = height / 2

  def camera: Camera

  def scene: Scene

  def setSize(newWidth: Double, newHeight: Double): Unit = {
    width = newWidth
    height = newHeight

    halfWidth = width / 2
    halfHeight = height / 2
  }

  def renderTo(renderer: Renderer, time: Double = 0): Unit = {
    renderer.render(scene, camera)
  }
}
