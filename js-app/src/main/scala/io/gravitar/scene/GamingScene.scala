package io.gravitar.scene

import io.gravitar.engine.GameAssets
import io.gravitar.engine.GameAssets.Ready
import org.denigma.threejs._

class GamingScene(
    override var width: Double,
    override var height: Double,
    val gameAssets: GameAssets[Ready.type]
) extends SceneWrapper {

  override val scene: Scene = new Scene()

  override val camera: PerspectiveCamera = new PerspectiveCamera(60, width / height, -100, 100)

  scene.add(camera)

  val mainLight = new DirectionalLight(0xffffff)
  mainLight.target.position.set(0, 0, 0)
  mainLight.position.set(0, 0, 1000)

  scene.add(mainLight)

  val axes = new AxisHelper(50)
  scene.add(axes)

  override def setSize(newWidth: Double, newHeight: Double): Unit = {
    super.setSize(newWidth, newHeight)
    camera.aspect = width / height
    camera.updateProjectionMatrix()
  }

}
