package io.gravitar.scene

import io.gravitar.engine.Summary
import io.gravitar.utils._

class SummaryView extends CanvasSprite(256, 128, 0.75, 0.75) {

  var currentSummary: Summary = Summary()

  ctx.textBaseline = "middle"
  ctx.fillStyle = "white"
  ctx.strokeStyle = "black"

  redraw()

  def update(summary: Summary): Unit = {
    currentSummary = summary
    redraw()
  }

  protected override def draw(): Unit = {
    ctx.font = "16pt courier new"
    ctx.fillStyle = "white"
    ctx.textAlign = "left"
    ctx.fillText("galaxy summary:", 0, 8)
    drawRecord(1, "tick", formatToSpacedInt(currentSummary.tick))
    drawRecord(2, "ships", formatToSpacedInt(currentSummary.shipsCount))
    drawRecord(3, "asteroids", formatToSpacedInt(currentSummary.asteroidsCount))
    drawRecord(4, "dusts", formatToSpacedInt(currentSummary.dustCount))
    drawRecord(5, "matter mass", formatRoundedInt(currentSummary.matter.toInt))
    drawRecord(6, "antimatter mass", formatRoundedInt(currentSummary.antimatter.toInt))
  }

  protected def drawRecord(position: Int, label: String, value: String) = {
    ctx.font = "12pt courier new"
    ctx.textAlign = "left"
    ctx.fillText(s"$label:", 0, 16 + position * 16)

    ctx.font = "12pt courier new"
    ctx.textAlign = "right"
    ctx.fillText(s"$value", width, 16 + position * 16)
  }

}
