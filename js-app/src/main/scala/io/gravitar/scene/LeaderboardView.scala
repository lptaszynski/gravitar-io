package io.gravitar.scene

import io.gravitar.engine.Leaderboard
import io.gravitar.utils._

class LeaderboardView extends CanvasSprite(512, 512, 0.75, 0.75) {

  var currentLeaderboard: Leaderboard = Leaderboard()

  ctx.textAlign = "center"
  ctx.textBaseline = "middle"
  ctx.fillStyle = "white"
  ctx.strokeStyle = "black"

  redraw()

  def update(leaderboard: Leaderboard): Unit = {
    currentLeaderboard = leaderboard
    redraw()
  }

  protected override def draw(): Unit = {
    ctx.font = "26pt neuropolx"
    ctx.fillStyle = "white"
    ctx.fillText("Leaderboard", width / 2, 16)
    ctx.font = "16pt neuropolx"
    currentLeaderboard.bestScores.zipWithIndex.foreach {
      case (score, position) =>
        ctx.fillText(
          s"${position + 1}. ${score.playerName} - ${formatRoundedInt(score.score.toInt)}",
          width / 2,
          64 + position * 32
        )
    }
  }

}
