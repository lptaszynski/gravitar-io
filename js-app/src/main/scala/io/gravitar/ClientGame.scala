package io.gravitar

import io.gravitar.Frontend.newGameHandler
import io.gravitar.engine._
import io.gravitar.pubsub._
import io.gravitar.utils.Benchmark
import io.gravitar.utils.SerializationOps._
import io.gravitar.worker.{
  GameEngineWorkersApi,
  WorkerMessage,
  PropagateHostDirectives => WorkerPropagateHostDirectives
}

import io.gravitar.wsprotocol._
import org.scalajs.dom
import org.scalajs.dom.WebSocket
import org.scalajs.dom.raw.{ErrorEvent, Event, MessageEvent}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Promise}
import scala.scalajs.js.typedarray.ArrayBuffer

class ClientGame(
    override val webSocket: WebSocket,
    val workersPoolApi: GameEngineWorkersApi,
    val viewGameEngine: ViewGameEngine
) extends GameServerApi(webSocket) {

  private var tickMarker: Double = 0

  configureWebSocket()

  protected val promiseReady: Promise[Unit] = Promise()
  val ready: Future[ClientGame] = promiseReady.future.map { _ =>
    configureSectorsRefresher()
    this
  }

  def getTickFraction(deltaTime: Double): Double =
    deltaTime / viewGameEngine.gameParameters.tickMillis

  private def configureWebSocket(): Unit = {
    webSocket.binaryType = "arraybuffer"
    webSocket.onopen = { event: Event =>
      log(s"hello!")
    }
    webSocket.onerror = { event: ErrorEvent =>
      ()
    }
    webSocket.onmessage = { event: MessageEvent =>
      val wsMsg = WSPicklers.unpickle(event.data.asInstanceOf[ArrayBuffer])
      wsMsg match {
        case PropagateHostDirectives(hostDirectives) =>
          val workerMessage = WorkerPropagateHostDirectives(
            event.data.asInstanceOf[ArrayBuffer].toByteBuffer
          )
          val encodedWorkerMessage = WorkerMessage.pickle(workerMessage)
          workersPoolApi.broadcast(encodedWorkerMessage)
          viewGameEngine.handleHostDirectives(hostDirectives)
          if (hostDirectives == Vector(EndOfFrame)) {
            val now = dom.window.performance.now()
            tickMarker = now
            if (!promiseReady.isCompleted) promiseReady.success()
          }
        case JoinedGameAs(player, body) =>
          newGameHandler.responseFromServer.success(Right(player.name))
          viewGameEngine.setModeTo(PlayerMode(player))
          viewGameEngine.playerBodyPosition.set(body.position.x, body.position.y, 0)
          refreshSectors()
        case CannotJoinGame(reason) =>
          newGameHandler.responseFromServer.success(Left(reason))
        case YouHaveLost(message) =>
          newGameHandler.showNewGameProposition(message)
        case UpdateLeaderboard(leaderboard) =>
          viewGameEngine.hUDScene.updateLeaderboard(leaderboard)
        case UpdateSummary(summary) =>
          viewGameEngine.hUDScene.updateSummary(summary)
        case Logs(events) =>
          newGameHandler.logsResponseFromServer.success(events)
      }
      webSocket.onclose = { event: Event =>
        ()
      }
    }
  }

  private def configureSectorsRefresher(): Unit = {
    refreshSectors()
    dom.window.setInterval(() => refreshSectors(), 200)
  }

  private def refreshSectors(): Unit = {
    val sectorsUpdate = viewGameEngine.getSectorsUpdate
    applySectorsUpdate(sectorsUpdate)
  }

  private def applySectorsUpdate(sectorsUpdate: SectorsUpdate): Unit = {
    sectorsUpdate.subscibeTo.foreach(subscribeToSector)
    sectorsUpdate.unsubscribeFrom.foreach(unsubscribeFromSector)
  }

}
