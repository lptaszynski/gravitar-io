package io.gravitar

object BenchmarkLoggingConfig {

  object Gravity {
    val responseAwait: Boolean = false
  }

  object Collisions {
    val responseAwait: Boolean = false
  }

  object Main {
    val tick: Boolean       = false
    val render: Boolean     = false
    val meshUpdate: Boolean = false
  }
}
