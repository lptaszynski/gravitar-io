package io.gravitar

import io.gravitar.controller.KeyboardAndMouseController
import io.gravitar.engine._
import io.gravitar.scene.{BackgroundScene, GamingScene, HUDScene}
import io.gravitar.utils._
import io.gravitar.worker.{GameEngineWorkersApi, WorkersPool}
import org.denigma.threejs._
import org.scalajs.dom
import org.scalajs.dom.raw._

import scala.scalajs.js
import scala.concurrent.ExecutionContext.Implicits.global
import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel}

@JSExportTopLevel("Frontend")
object Frontend {

  var newGameHandler: NewGameHandler = null

  @JSExport
  def logs(events: Int = 100): Unit = newGameHandler.askLogs(events)

  @JSExport
  def play(): Unit = newGameHandler.tryToJoinGame()

  @JSExport
  def main(): Unit = {

    val workersPool    = new WorkersPool("/resources/worker.js", 3)
    val workersPoolApi = new GameEngineWorkersApi(workersPool)

    val renderer = new WebGLRenderer(
      js.Dynamic
        .literal(alpha = true, antialias = false)
        .asInstanceOf[WebGLRendererParameters]
    )
    renderer.setClearColor(new Color(0x000000), 0xffffff)
    renderer.setSize(dom.window.innerWidth, dom.window.innerHeight)
    renderer.autoClear = false

    dom.document.getElementById("WebGL-output").appendChild(renderer.domElement)

    val gameAssets = GameAssets()

    gameAssets.ready.zip(workersPool.poolReady).map {
      case (readyGameAssets, _) =>
        val (width, height) = (dom.window.innerWidth, dom.window.innerHeight)

        val backgroundScene = new BackgroundScene(width, height, readyGameAssets)
        val gamingScene     = new GamingScene(width, height, readyGameAssets)
        val hudScene        = new HUDScene(width, height, readyGameAssets)

        def resize = {
          val (width, height) = (dom.window.innerWidth, dom.window.innerHeight)
          backgroundScene.setSize(width, height)
          gamingScene.setSize(width, height)
          hudScene.setSize(width, height)
          renderer.setSize(width, height)
        }

        def onResize(e: Event) = {
          // Calling it without a delay causes invalid proportions on firefox
          dom.window.setTimeout(() => resize, 100)
        }

        dom.window.setTimeout(() => resize, 100)

        dom.window.addEventListener("resize", onResize, false)

        val gameViewEngine: ViewGameEngine = new ViewGameEngine(gamingScene, hudScene, SpecMode)

        val serverConnection = new WebSocket(ClientUtils.getWebsocketUri)

        val futureClientGame = workersPool.poolReady
          .flatMap(_ => new ClientGame(serverConnection, workersPoolApi, gameViewEngine).ready)

        futureClientGame.foreach { clientGame =>
          workersPoolApi
            .configureGridUpdates(dom.window.performance.now(), gameViewEngine.getGridCenterPoint)

          val controller = new KeyboardAndMouseController(clientGame)
          controller.setup()

          newGameHandler = new NewGameHandler(clientGame)
          newGameHandler.showNewGameProposition()

          var lastRenderTime                  = 0.0
          var receivedLastGridUpdate: Boolean = true
          def renderScene(renderTime: Double): Unit = {
            val diff = renderTime - lastRenderTime
            lastRenderTime = renderTime

            val dFraction = clientGame.getTickFraction(diff)

            if (receivedLastGridUpdate) {
              receivedLastGridUpdate = false
              workersPoolApi.getGridUpdate(gameViewEngine.getGridCenterPoint, renderTime).foreach {
                meshUpdate =>
                  gameViewEngine.setNewGravityPotentials(meshUpdate.vertexPotential)
                  gameViewEngine.refreshPotentialsGrid()
                  receivedLastGridUpdate = true
              }
            }

            gameViewEngine.tickBodiesSmoothly(dFraction)

            gameViewEngine.updateCamera()

            renderer.clear()
            backgroundScene.renderTo(renderer)
            gamingScene.renderTo(renderer)
            hudScene.renderTo(renderer)

            dom.window.requestAnimationFrame(renderScene)
          }

          lastRenderTime = dom.window.performance.now()
          renderScene(lastRenderTime)

        }
    }
  }
}
