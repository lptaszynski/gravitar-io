package io.gravitar

import io.gravitar.engine.ImPlayer

sealed trait Mode
case object SpecMode                      extends Mode
case class PlayerMode(ImPlayer: ImPlayer) extends Mode
