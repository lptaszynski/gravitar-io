## gravitar-io

My game prototype inspired by agar.io, please be aware that it's still a work in progress...

To run a local server:

```
sbt
backend/reStart

[info] Application backend not yet started
backend Starting io.gravitar.Boot.main()
[info] Starting application backend in the background ...
[success] Total time: 3 s, completed Dec 16, 2019, 5:10:25 PM
sbt:gravitar-io> backend Server is listening on 0:0:0:0:0:0:0:0:8080

backend/reStop
```

Visit `localhost:8080` to play it, enjoy!

To create a runnable jar file use `backend/assembly`. The output file is generated to `backend/target/scala-2.12/backend-assembly-x.y.z.jar`.

The application and the game configuration are defined respectively in `backend/src/main/resources/application.conf` and `backend/src/main/resources/game.conf`.

Have fun!
