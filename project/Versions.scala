object Versions
    extends SharedVersions
    with AkkaVersions
    with WebJarsVersions
    with ScalaJSVersions {
  val scala = "2.12.8"
}

// versions for libs that are shared between client and server
trait SharedVersions {
  val scalaTest = "3.0.1"
  val boopickle = "1.3.1"
}

trait AkkaVersions {
  val akka = "2.5.6"
  val akkaHttp = "10.0.10"
}

trait ScalaJSVersions {
  val threejsFacade = "0.0.77-0.1.8"
  val dom = "0.9.3"
  val scalaCSS = "0.5.1"
}

trait WebJarsVersions {
  val threeJs = "r77"
}
