addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.0.0")

addSbtPlugin("io.spray" % "sbt-revolver" % "0.9.0")

addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.28")

addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.5")

addSbtPlugin("com.typesafe.sbt" % "sbt-web" % "1.4.3") // advanced assets handling

addSbtPlugin("com.vmunier" % "sbt-web-scalajs" % "1.0.6")
