import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._
import org.portablescala.sbtplatformdeps.PlatformDepsPlugin.autoImport._

import sbt._

object Dependencies {

  //libs for testing
  lazy val testing: Def.Initialize[Seq[ModuleID]] = Def.setting(
    Seq(
      "org.scalatest" %%% "scalatest" % Versions.scalaTest % Test,
      "org.scalactic" %%% "scalactic" % Versions.scalaTest % Test))

  //common purpose libs
  lazy val commonShared: Def.Initialize[Seq[ModuleID]] = Def.setting(
    Seq("io.suzaku" %%% "boopickle" % Versions.boopickle))

  lazy val templates = Def.setting(
    Seq(
      "com.github.japgolly.scalacss" %%% "core" % Versions.scalaCSS,
      "com.github.japgolly.scalacss" %%% "ext-scalatags" % Versions.scalaCSS
    ))

  //akka-related libs
  lazy val akka = Def.setting(
    Seq(
      "com.typesafe.akka" %% "akka-testkit" % Versions.akka % Test,
      "com.typesafe.akka" %% "akka-stream-testkit" % Versions.akka % Test,
      "com.typesafe.akka" %% "akka-http" % Versions.akkaHttp,
      "com.typesafe.akka" %% "akka-http-testkit" % Versions.akkaHttp % Test
    ))

  // scalajs libs
  lazy val sjsLibs = Def.setting(
    Seq(
      "org.scala-js" %%% "scalajs-dom" % Versions.dom
    ))

  lazy val facadeDependencies = Def.setting(
    Seq(
      "org.denigma" %%% "threejs-facade" % Versions.threejsFacade
    ))

  // dependencies on javascript libs
  lazy val webjars = Def.setting(
    Seq(
      "org.webjars" % "three.js" % Versions.threeJs
    ))

}
