import com.typesafe.sbt.web.SbtWeb.autoImport._
import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._

name := "gravitar-io"

version in ThisBuild := "0.7.10"

def commonSettings = Seq(
  scalaVersion := Versions.scala,
  resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases",
  resolvers += sbt.Resolver.bintrayRepo("denigma", "denigma-releases"),
  resolvers += Resolver.jcenterRepo,
  libraryDependencies ++= Dependencies.commonShared.value,
  libraryDependencies ++= Dependencies.testing.value
)

lazy val root = project
  .in(file("."))
  .aggregate(jsApp, backend, sharedJVM, sharedJS)

// Scala-Js frontend
lazy val jsApp = project
  .in(file("js-app"))
  .enablePlugins(ScalaJSPlugin, ScalaJSWeb)
  .settings(commonSettings: _*)
  .settings(
    libraryDependencies ++= Dependencies.templates.value,
    libraryDependencies ++= Dependencies.sjsLibs.value,
    libraryDependencies ++= Dependencies.facadeDependencies.value
  )
  .dependsOn(sharedJS)


// Akka Http based backend
lazy val backend = project
  .in(file("backend"))
  .settings(commonSettings: _*)
  .settings(
    unmanagedResourceDirectories in Compile += { baseDirectory.value / "src/main/resources" },
    libraryDependencies ++= Dependencies.templates.value,
    libraryDependencies ++= Dependencies.akka.value,
    scalaJSProjects := Seq(jsApp),
    resourceGenerators in Compile += Def.task {
      val f1 = (fullOptJS in Compile in jsApp).value
      Seq(f1.data)
    }.taskValue,
    (fullClasspath in Runtime) += (packageBin in Assets).value, //to package production deps
    mainClass in Compile := Some("io.gravitar.Boot"),
    watchSources ++= (watchSources in jsApp).value)
  .dependsOn(sharedJVM)
  .enablePlugins(SbtWeb)

lazy val shared =
  (crossProject.crossType(CrossType.Pure) in file("shared"))
    .settings(commonSettings)
    .settings(
      name := "shared"
    )
    .jsConfigure(p => p.enablePlugins(ScalaJSPlugin, ScalaJSWeb))

lazy val sharedJVM = shared.jvm
lazy val sharedJS = shared.js
